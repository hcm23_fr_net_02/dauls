﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Common.Helpers;
using QuanLyThuVien.Models;
using QuanLyThuVien.Service.Contracts;

namespace QuanLyThuVien.Views
{
    public class CustomerManager
    {
        private readonly ICustomerService _customerService;

        public CustomerManager(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        //User can navigation through pages and open submenu at any pages
        public void SetMainFunctions()
        {
            var customers = _customerService.GetAll().ToList();
            int currentPage = 1;
            double pages;
            ConsoleKeyInfo pagingChoice = new ConsoleKeyInfo();

            while (pagingChoice.Key != ConsoleKey.B)
            {
                Console.Clear();
                var paging = PaginationHelper.SetPagination(customers, ref currentPage, out pages);

                try
                {
                    PaginationHelper.PrintPagination(paging);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Menu.PrintPaginationMenu(currentPage, pages);
                pagingChoice = Console.ReadKey();

                switch (pagingChoice.Key)
                {
                    case ConsoleKey.N:

                        if (currentPage < pages)
                        {
                            currentPage++;
                        }

                        break;

                    case ConsoleKey.P:

                        if (currentPage > 1)
                        {
                            currentPage--;
                        }

                        break;

                    case ConsoleKey.I:

                        SetSubFunctions(paging, ref pagingChoice);

                        break;

                    default:
                        break;
                }
            }
        }

        //User can see more features to interact
        public void SetSubFunctions(ICollection<Customer> pagingCustomers, ref ConsoleKeyInfo pagingChoice)
        {
            Console.WriteLine();
            ConsoleKeyInfo subChoice = new ConsoleKeyInfo();

            while (subChoice.Key != ConsoleKey.D5 && subChoice.Key != ConsoleKey.NumPad5)
            {
                Console.Clear();

                try
                {
                    PaginationHelper.PrintPagination(pagingCustomers);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Menu.PrintCustomersSubMenu();
                subChoice = Console.ReadKey();

                switch (subChoice.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.AddMenu);
                            AddNewCustomer();
                            Refresh(ref subChoice, ref pagingChoice);
                            Console.WriteLine(NotificationConstants.Success);
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.GetDetailMenu);
                            GetDetailCustomer();
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.DeleteMenu);
                            DeleteCustomer();
                            Refresh(ref subChoice, ref pagingChoice);
                            Console.WriteLine(NotificationConstants.Success);
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.UpdateMenu);
                            UpdateCustomer();
                            Refresh(ref subChoice, ref pagingChoice);
                            Console.WriteLine(NotificationConstants.Success);
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    default:
                        break;
                }
            }
        }

        public void AddNewCustomer()
        {
            Console.WriteLine("Input information of customer to add new (Name, Address, Phone number):");
            string[] customerInfo = Console.ReadLine()!.Split(',', StringSplitOptions.TrimEntries);
            long phoneNumber = 0;

            if (customerInfo.Length < 3)
            {
                throw new Exception(NotificationConstants.AllInfoMustBeFilled);
            }

            if (!long.TryParse(customerInfo[2], out phoneNumber))
            {
                throw new Exception(NotificationConstants.FieldMustBeANumber);
            }

            _customerService.AddCustomer(customerInfo[0], customerInfo[1], phoneNumber);
        }

        public int GetDetailCustomer()
        {
            Console.Write(MenuConstants.InputCustomerId);
            string inputId = Console.ReadLine()!;
            int id = 0;

            if (!int.TryParse(inputId, out id))
            {
                throw new Exception(NotificationConstants.FieldMustBeANumber);
            }

            Customer customer = _customerService.GetById(id);
            Console.WriteLine($"Id: {customer.Id} Customer name: {customer.Name} Address: {customer.Address} Phone number: {customer.PhoneNumber}");

            return id;
        }

        public void UpdateCustomer()
        {
            int id = GetDetailCustomer();

            Console.WriteLine("Input information of customer to update (Name, Address, Phone number):");
            string[] customerInfo = Console.ReadLine()!.Split(',', StringSplitOptions.TrimEntries);
            long phoneNumber = 0;

            if (customerInfo.Length < 3)
            {
                throw new Exception(NotificationConstants.AllInfoMustBeFilled);
            }

            if (!long.TryParse(customerInfo[2], out phoneNumber))
            {
                throw new Exception(NotificationConstants.FieldMustBeANumber);
            }

            _customerService.UpdateCustomer(id, customerInfo[0], customerInfo[1], phoneNumber);
        }

        public void DeleteCustomer()
        {
            int id = GetDetailCustomer();

            _customerService.DeleteCustomer(id);
        }

        public void Refresh(ref ConsoleKeyInfo subChoice, ref ConsoleKeyInfo pagingChoice)
        {
            subChoice = new ConsoleKeyInfo((char)ConsoleKey.D5, ConsoleKey.D5, false, false, false);
            pagingChoice = new ConsoleKeyInfo((char)ConsoleKey.B, ConsoleKey.B, false, false, false);
        }
    }
}
