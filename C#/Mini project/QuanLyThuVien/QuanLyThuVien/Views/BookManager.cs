﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Common.Enums;
using QuanLyThuVien.Common.Helpers;
using QuanLyThuVien.Models;
using QuanLyThuVien.Service.Contracts;

namespace QuanLyThuVien.Views
{
    public class BookManager
    {
        private readonly IBookService _bookService;
        private readonly ICommentService _commentService;

        public BookManager(IBookService bookService, ICommentService commentService)
        {
            _bookService = bookService;
            _commentService = commentService;
        }

        //User can navigation through pages and open submenu at any pages
        public void SetMainFunctions()
        {
            var books = _bookService.GetAll().ToList();
            int currentPage = 1;
            double pages;
            
            ConsoleKeyInfo pagingChoice = new ConsoleKeyInfo();

            while (pagingChoice.Key != ConsoleKey.B)
            {
                Console.Clear();
                var paging = PaginationHelper.SetPagination(books, ref currentPage, out pages);

                try
                {
                    PaginationHelper.PrintPagination(paging);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                Menu.PrintPaginationMenu(currentPage, pages);
                pagingChoice = Console.ReadKey();

                switch (pagingChoice.Key)
                {
                    case ConsoleKey.N:

                        if (currentPage < pages)
                        {
                            currentPage++;
                        }

                        break;

                    case ConsoleKey.P:

                        if (currentPage > 1)
                        {
                            currentPage--;
                        }

                        break;

                    case ConsoleKey.I:

                        SetSubFunctions(paging, ref pagingChoice);

                        break;

                    default:
                        break;
                }
            }
        }

        //User can see more features to interact
        public void SetSubFunctions(ICollection<Book> pagingBooks, ref ConsoleKeyInfo pagingChoice)
        {
            Console.WriteLine();
            ConsoleKeyInfo subChoice = new ConsoleKeyInfo();

            while (subChoice.Key != ConsoleKey.D7 && subChoice.Key != ConsoleKey.NumPad7)
            {
                Console.Clear();

                try
                {
                    PaginationHelper.PrintPagination(pagingBooks);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                Menu.PrintBooksSubMenu();
                subChoice = Console.ReadKey();

                switch (subChoice.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        
                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.AddMenu);
                            AddNewBook();
                            Refresh(ref subChoice, ref pagingChoice);
                            Console.WriteLine(NotificationConstants.Success);
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.GetDetailMenu);
                            PrintDetailBook();
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.DeleteMenu);
                            DeleteBook();
                            Refresh(ref subChoice, ref pagingChoice);
                            Console.WriteLine(NotificationConstants.Success);
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.UpdateMenu);
                            UpdateBook();
                            Refresh(ref subChoice, ref pagingChoice);
                            Console.WriteLine(NotificationConstants.Success);
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D5:
                    case ConsoleKey.NumPad5:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.AddMenu);
                            AddComment();
                            Refresh(ref subChoice, ref pagingChoice);
                            Console.WriteLine(NotificationConstants.Success);
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D6:
                    case ConsoleKey.NumPad6:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.GetDetailMenu);
                            PrintCommentsByBookId();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    default:
                        break;
                }
            }
        }

        public void AddNewBook()
        {
            Console.WriteLine("Input information of book to add new (Title, Author, Publication Year, Quantity, Genre):");
            string[] bookInfo = Console.ReadLine()!.Split(',', StringSplitOptions.TrimEntries);
            int publicationYear = 0;
            int quantity = 0;

            if (bookInfo.Length < 5)
            {
                throw new Exception(NotificationConstants.AllInfoMustBeFilled);
            }

            if (!int.TryParse(bookInfo[2], out publicationYear) || !int.TryParse(bookInfo[3], out quantity))
            {
                throw new Exception(NotificationConstants.FieldMustBeANumber);
            }

            _bookService.AddBook(bookInfo[0], bookInfo[1], publicationYear, quantity, bookInfo[4]);
        }

        public int PrintDetailBook()
        {
            Console.Write(MenuConstants.InputBookId);
            string inputId = Console.ReadLine()!;
            int id = 0;

            if (!int.TryParse(inputId, out id))
            {
                throw new Exception(NotificationConstants.FieldMustBeANumber);
            }

            Book book = _bookService.GetById(id);
            Console.WriteLine($"Id: {book.Id} Title: {book.Title} Author: {book.Author} Publication Year: {book.PublicationYear}");
            Console.WriteLine($"Quantity: {book.Quantity} Available: {book.IsAvailable} Genre: {book.Genre}");

            return id;
        }

        public void UpdateBook()
        {
            int id = PrintDetailBook();

            Console.WriteLine("Input information of book to update (Title, Author, Publication Year, Quantity, Genre):");
            string[] bookInfo = Console.ReadLine()!.Split(',', StringSplitOptions.TrimEntries);
            int publicationYear = 0;
            int quantity = 0;

            if (bookInfo.Length < 5)
            {
                throw new Exception(NotificationConstants.AllInfoMustBeFilled);
            }

            if (!int.TryParse(bookInfo[2], out publicationYear) || !int.TryParse(bookInfo[3], out quantity))
            {
                throw new Exception(NotificationConstants.FieldMustBeANumber);
            }

            _bookService.UpdateBook(id, bookInfo[0], bookInfo[1], publicationYear, quantity, bookInfo[4]);
        }

        public void DeleteBook()
        {
            int id = PrintDetailBook();

            _bookService.DeleteBook(id);
        }

        public void AddComment()
        {
            int bookId = PrintDetailBook();

            Console.WriteLine("Input header of this comment: ");
            string header = Console.ReadLine()!;

            Console.WriteLine("Input content of this comment: ");
            string content = Console.ReadLine()!;

            Console.Write("Input rate scale 1-3 (Boring, Normal, Good): ");
            string inputRate = Console.ReadLine()!;
            int rateNum = 0;

            if (!int.TryParse(inputRate, out rateNum))
            {
                throw new ArgumentException(NotificationConstants.FieldMustBeANumber);
            }

            if (rateNum < 1 || rateNum > 3)
            {
                throw new ArgumentOutOfRangeException(NotificationConstants.InvalidRateNumber);
            }

            BookRate rate = (BookRate)rateNum - 1;

            _commentService.AddComment(bookId, header, content, rate);
        }

        public void PrintCommentsByBookId()
        {
            int bookId = PrintDetailBook();
            var comments = _commentService.GetCommentsByBookId(bookId).ToList();
            
            PaginationHelper.NavigatePagination(comments);
        }

        public void FilterBooks()
        { 
            ConsoleKeyInfo choice = new ConsoleKeyInfo();

            while (choice.Key != ConsoleKey.D5 && choice.Key != ConsoleKey.NumPad5)
            {
                Menu.PrintFilterBooksMenu();
                choice = Console.ReadKey();

                switch (choice.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:

                        Console.WriteLine();
                        Console.Write("Input author name: ");
                        string authorName = Console.ReadLine()!;

                        try
                        {
                            var booksByAuthor = _bookService.GetBooksByAuthor(authorName);
                            PaginationHelper.NavigatePagination(booksByAuthor);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:

                        Console.WriteLine();
                        Console.Write("Input genre: ");
                        string genre = Console.ReadLine()!;

                        try
                        {
                            var booksByGenre = _bookService.GetBooksByGenre(genre);
                            PaginationHelper.NavigatePagination(booksByGenre);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:

                        Console.WriteLine();
                        Console.Write("Input publication year: ");
                        string inputPublicationYear = Console.ReadLine()!;
                        int publicationYear = 0;

                        if (!int.TryParse(inputPublicationYear, out publicationYear))
                        {
                            throw new ArgumentException(NotificationConstants.InvalidNumber);
                        }

                        try
                        {
                            var booksByPublicationYear = _bookService.GetBooksByPublicationYear(publicationYear);
                            PaginationHelper.NavigatePagination(booksByPublicationYear);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:

                        Console.WriteLine();
                        Console.Write("Input title: ");
                        string title = Console.ReadLine()!;

                        try
                        {
                            var booksByTitle = _bookService.GetBooksByTitle(title);
                            PaginationHelper.NavigatePagination(booksByTitle);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    default:
                        break;
                }
            }

        }

        public void Refresh(ref ConsoleKeyInfo subChoice, ref ConsoleKeyInfo pagingChoice)
        {
            subChoice = new ConsoleKeyInfo((char)ConsoleKey.D7, ConsoleKey.D7, false, false, false);
            pagingChoice = new ConsoleKeyInfo((char)ConsoleKey.B, ConsoleKey.B, false, false, false);
        }
    }
}
