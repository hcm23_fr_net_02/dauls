﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Common.Helpers;
using QuanLyThuVien.Models;
using QuanLyThuVien.Service.Contracts;
using QuanLyThuVien.ViewModels;

namespace QuanLyThuVien.Views
{
    public class LoanManager
    {
        private readonly ILoanService _loanService;
        private readonly ICustomerService _customerService;
        private readonly IBookService _bookService;

        public LoanManager(ILoanService loanService, ICustomerService customerService, IBookService bookService)
        {
            _loanService = loanService;
            _customerService = customerService;
            _bookService = bookService;
        }

        //User can navigation through pages and open submenu at any pages
        public void SetMainFunctions()
        {
            var loans = _loanService.GetAll().ToList();
            int currentPage = 1;
            double pages;
            ConsoleKeyInfo pagingChoice = new ConsoleKeyInfo();

            while (pagingChoice.Key != ConsoleKey.B)
            {
                Console.Clear();
                var paging = PaginationHelper.SetPagination(loans, ref currentPage, out pages);

                try
                {
                    PaginationHelper.PrintPagination(paging);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Menu.PrintPaginationMenu(currentPage, pages);
                pagingChoice = Console.ReadKey();

                switch (pagingChoice.Key)
                {
                    case ConsoleKey.N:

                        if (currentPage < pages)
                        {
                            currentPage++;
                        }

                        break;

                    case ConsoleKey.P:

                        if (currentPage > 1)
                        {
                            currentPage--;
                        }

                        break;

                    case ConsoleKey.I:

                        SetSubFunctions(paging, ref pagingChoice);

                        break;

                    default:
                        break;
                }
            }
        }

        //User can see more features to interact
        public void SetSubFunctions(ICollection<LoanVM> pagingLoans, ref ConsoleKeyInfo pagingChoice)
        {
            Console.WriteLine();
            ConsoleKeyInfo subChoice = new ConsoleKeyInfo();

            while (subChoice.Key != ConsoleKey.D6 && subChoice.Key != ConsoleKey.NumPad6)
            {
                Console.Clear();

                try
                {
                    PaginationHelper.PrintPagination(pagingLoans);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Menu.PrintLoansSubMenu();
                subChoice = Console.ReadKey();

                switch (subChoice.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:

                        try
                        {
                            Console.WriteLine();
                            PrintAvailableBooks();
                            Refresh(ref subChoice, ref pagingChoice);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.ReturnBooksMenu);
                            ReturnBooks();
                            Refresh(ref subChoice, ref pagingChoice);
                            Console.WriteLine(NotificationConstants.Success);
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:

                        try
                        {
                            Console.WriteLine();
                            PrintLoansByCustomerName();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:

                        try
                        {
                            Console.WriteLine();
                            PrintLoansByBookTitle();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    case ConsoleKey.D5:
                    case ConsoleKey.NumPad5:

                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine(MenuConstants.GetDetailMenu);
                            PrintDetailLoan();
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                        break;

                    default:
                        break;
                }
            }
        }

        public void PrintAvailableBooks()
        {
            var books = _bookService.GetAvailableBooks().ToList();
            int currentPage = 1;
            double pages;
            ConsoleKeyInfo pagingChoice = new ConsoleKeyInfo();

            while (pagingChoice.Key != ConsoleKey.B)
            {
                Console.Clear();
                var paging = PaginationHelper.SetPagination(books, ref currentPage, out pages);

                try
                {
                    PaginationHelper.PrintPagination(paging);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Menu.PrintPaginationMenu(currentPage, pages);
                pagingChoice = Console.ReadKey();

                switch (pagingChoice.Key)
                {
                    case ConsoleKey.N:

                        if (currentPage < pages)
                        {
                            currentPage++;
                        }

                        break;

                    case ConsoleKey.P:

                        if (currentPage > 1)
                        {
                            currentPage--;
                        }

                        break;

                    case ConsoleKey.I:

                        BorrowBooks(paging);
                        Console.WriteLine(NotificationConstants.Success);
                        pagingChoice = new ConsoleKeyInfo((char)ConsoleKey.B, ConsoleKey.B, false, false, false);
                        Console.ReadKey();

                        break;

                    default:
                        break;
                }
            }
        }

        public void PrintLoansByCustomerName()
        {
            Console.WriteLine();
            Console.Write("Input customer name: ");
            string customerName = Console.ReadLine()!;

            var loansByCustomerName = _loanService.GetLoansByCustomerName(customerName);
            PaginationHelper.NavigatePagination(loansByCustomerName);
        }

        public void PrintLoansByBookTitle()
        {
            Console.WriteLine();
            Console.Write("Input book title: ");
            string bookTitle = Console.ReadLine()!;

            var loansByBookTitle = _loanService.GetLoansByBookTitle(bookTitle);
            PaginationHelper.NavigatePagination(loansByBookTitle);
        }

        public void PrintDetailLoan()
        {
            Console.Write("Input loan id: ");
            string inputId = Console.ReadLine()!;
            int id = 0;

            if (!int.TryParse(inputId, out id))
            {
                throw new Exception(NotificationConstants.FieldMustBeANumber);
            }

            LoanVM loanVM = _loanService.GetLoanById(id);
            ICollection<DetailLoanVM> detailLoansVM = _loanService.GetDetailLoansByLoanId(loanVM.Id);
            Console.WriteLine(loanVM);

            foreach (DetailLoanVM detailLoanVM in detailLoansVM)
            {
                Console.WriteLine(detailLoanVM);
            }
        }

        public void BorrowBooks(ICollection<Book> pagingAvailableBooks)
        {
            Console.Clear();

            //--------------------------Print Available Books--------------------------

            try
            {
                PaginationHelper.PrintPagination(pagingAvailableBooks);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            //--------------------------Input many ids of books to borrow--------------------------

            Console.WriteLine(MenuConstants.BorrowBooksMenu);

            Console.WriteLine("Input ids of books (1, 2, 3,...): ");
            string[] inputBooksId = Console.ReadLine()!.Split(',', StringSplitOptions.TrimEntries);
            HashSet<int> booksId = new HashSet<int>();
            Dictionary<int, int> bookQuantity = new Dictionary<int, int>();

            if (inputBooksId.Length < 1)
            {
                throw new ArgumentException(NotificationConstants.AllInfoMustBeFilled);
            }

            for (int i = 0; i < inputBooksId.Length; i++)
            {
                int id = 0;

                if (!int.TryParse(inputBooksId[i], out id))
                {
                    throw new ArgumentException(NotificationConstants.FieldMustBeANumber);
                }

                booksId.Add(id);
            }

            //--------------------------Input quantity for each book--------------------------

            foreach (int id in booksId)
            {
                Console.Write("Input quantity of book id {0}: ", id);
                string inputQuantity = Console.ReadLine()!;
                int quantity = 0;

                if (!int.TryParse(inputQuantity, out quantity))
                {
                    throw new ArgumentException(NotificationConstants.FieldMustBeANumber);
                }

                bookQuantity.Add(id, quantity);
            }

            //--------------------------Input return date--------------------------

            Console.WriteLine("Input return date (dd/mm/yyyy): ");
            string[] inputDate = Console.ReadLine()!.Split('/', StringSplitOptions.TrimEntries);
            int month = 0;
            int day = 0;
            int year = 0;

            if (inputDate.Length < 3)
            {
                throw new Exception(NotificationConstants.AllInfoMustBeFilled);
            }

            if (!int.TryParse(inputDate[0], out day) || !int.TryParse(inputDate[1], out month) || !int.TryParse(inputDate[2], out year))
            {
                throw new Exception(NotificationConstants.FieldMustBeANumber);
            }

            DateTime dateEnd = new DateTime(year, month, day);

            if (dateEnd < DateTime.Now)
            {
                throw new Exception("Return date cannot be earlier current time");
            }

            //--------------------------Check customer exists or not--------------------------

            Console.WriteLine("Input customer phone number: ");
            string inputPhoneNumber = Console.ReadLine()!.Trim();
            long phoneNumber = 0;

            if (!long.TryParse(inputPhoneNumber, out phoneNumber))
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            Customer customer = _customerService.GetByPhone(phoneNumber);

            if (customer == null)
            {
                Console.WriteLine("This is a new customer, please add new customer");
                int newCustomerId = AddNewCustomer(phoneNumber);
                _loanService.BorrowBook(newCustomerId, dateEnd, bookQuantity);
            }
            else
            {
                _loanService.BorrowBook(customer.Id, dateEnd, bookQuantity);
            }
        }

        public int AddNewCustomer(long phoneNumber)
        {
            Console.WriteLine("Input information of customer to add new (Name, Address):");
            string[] customerInfo = Console.ReadLine()!.Split(',', StringSplitOptions.TrimEntries);

            if (customerInfo.Length < 2)
            {
                throw new Exception(NotificationConstants.AllInfoMustBeFilled);
            }

            return _customerService.AddCustomer(customerInfo[0], customerInfo[1], phoneNumber);
        }

        public void ReturnBooks()
        {
            Console.Write("Input loan id: ");
            string inputId = Console.ReadLine()!;
            int id = 0;

            if (!int.TryParse(inputId, out id))
            {
                throw new Exception(NotificationConstants.FieldMustBeANumber);
            }

            _loanService.ReturnBook(id);
        }

        public void Refresh(ref ConsoleKeyInfo subChoice, ref ConsoleKeyInfo pagingChoice)
        {
            subChoice = new ConsoleKeyInfo((char)ConsoleKey.D6, ConsoleKey.D6, false, false, false);
            pagingChoice = new ConsoleKeyInfo((char)ConsoleKey.B, ConsoleKey.B, false, false, false);
        }
    }
}
