﻿using QuanLyThuVien.Common.Constants;

namespace QuanLyThuVien.Views
{
    public static class Menu
    {
        public static void PrintMainMenu()
        {
            Console.Clear();
            Console.WriteLine(MenuConstants.MainMenu);
            Console.WriteLine(MenuConstants.ManageBooks, 1);
            Console.WriteLine(MenuConstants.FilterBooks, 2);
            Console.WriteLine(MenuConstants.ManageCustomers, 3);
            Console.WriteLine(MenuConstants.ManageLoans, 4);
            Console.WriteLine(MenuConstants.Exit, 5);
            Console.Write(MenuConstants.InputChoice);
        }

        public static void PrintBooksSubMenu()
        {
            Console.WriteLine();
            Console.WriteLine(MenuConstants.BookSubMenu);
            Console.WriteLine(MenuConstants.AddBook, 1);
            Console.WriteLine(MenuConstants.GetDetailBook, 2);
            Console.WriteLine(MenuConstants.DeleteBook, 3);
            Console.WriteLine(MenuConstants.UpdateBook, 4);
            Console.WriteLine(MenuConstants.AddComment, 5);
            Console.WriteLine(MenuConstants.PrintAllComments, 6);
            Console.WriteLine(MenuConstants.Exit, 7);
            Console.Write(MenuConstants.InputChoice);
        }

        public static void PrintFilterBooksMenu()
        {
            Console.Clear();
            Console.WriteLine(MenuConstants.FilterBooksMenu);
            Console.WriteLine(MenuConstants.FilterBooksByAuthor, 1);
            Console.WriteLine(MenuConstants.FilterBooksByGenre, 2);
            Console.WriteLine(MenuConstants.FilterBooksByPublicationYear, 3);
            Console.WriteLine(MenuConstants.FilterBooksByTitle, 4);
            Console.WriteLine(MenuConstants.Exit, 5);
            Console.Write(MenuConstants.InputChoice);

        }

        public static void PrintCustomersSubMenu()
        {
            Console.WriteLine();
            Console.WriteLine(MenuConstants.CustomerSubMenu);
            Console.WriteLine(MenuConstants.AddCustomer, 1);
            Console.WriteLine(MenuConstants.GetDetailCustomer, 2);
            Console.WriteLine(MenuConstants.DeleteCustomer, 3);
            Console.WriteLine(MenuConstants.UpdateCustomer, 4);
            Console.WriteLine(MenuConstants.Exit, 5);
            Console.Write(MenuConstants.InputChoice);
        }

        public static void PrintLoansSubMenu()
        {
            Console.WriteLine();
            Console.WriteLine(MenuConstants.LoanSubMenu);
            Console.WriteLine(MenuConstants.BorrowBooks, 1);
            Console.WriteLine(MenuConstants.ReturnBooks, 2);
            Console.WriteLine(MenuConstants.FilterLoansByCustomerName, 3);
            Console.WriteLine(MenuConstants.FilterLoansByBookTitle, 4);
            Console.WriteLine(MenuConstants.PrintDetailLoan, 5);
            Console.WriteLine(MenuConstants.Exit, 6);
            Console.Write(MenuConstants.InputChoice);
        }

        public static void PrintPaginationMenu(int currentPage, double pages)
        {
            Console.WriteLine();
            Console.WriteLine(MenuConstants.PaginationMenu);

            if (currentPage < pages)
            {
                Console.WriteLine(MenuConstants.NextPage);
            }

            if (currentPage > 1)
            {
                Console.WriteLine(MenuConstants.PreviousPage);
            }

            Console.WriteLine(MenuConstants.BackMenu);
            Console.WriteLine(MenuConstants.SubChoice);
            Console.Write(MenuConstants.InputPaginationChoice);
        }

        public static void PrintSubPaginationMenu(int currentPage, double pages)
        {
            Console.WriteLine();
            Console.WriteLine(MenuConstants.PaginationMenu);

            if (currentPage < pages)
            {
                Console.WriteLine(MenuConstants.NextPage);
            }

            if (currentPage > 1)
            {
                Console.WriteLine(MenuConstants.PreviousPage);
            }

            Console.WriteLine(MenuConstants.BackMenu);
            Console.WriteLine(MenuConstants.SubChoice);
            Console.Write(MenuConstants.InputSubPaginationChoice);
        }
    }
}