﻿namespace QuanLyThuVien.ViewModels
{
    public class LoanVM
    {
        private int _id;
        private string _customerName = "";
        private DateTime _dateStart;
        private DateTime _dateEnd;
        private bool _isReturn;

        public int Id { get => _id; init => _id = value; }
        public string CustomerName { get => _customerName; init => _customerName = value; }
        public DateTime DateStart { get => _dateStart; init => _dateStart = value; }
        public DateTime DateEnd { get => _dateEnd; init => _dateEnd = value; }
        public bool IsReturn { get => _isReturn; init => _isReturn = value; }

        public override string ToString()
        {
            return $"ID: {_id} Customer: {_customerName} DateStart: {_dateStart} DateEnd: {_dateEnd} Return: {_isReturn}";
        }
    }
}
