﻿namespace QuanLyThuVien.ViewModels
{
    public class DetailLoanVM
    {
        private int _id;
        private int _loanId;
        private string _bookTitle = "";
        private int _quantity;

        public int Id { get => _id; init => _id = value; }
        public int LoanId { get => _loanId; init => _loanId = value; }
        public string BookTitle { get => _bookTitle; init => _bookTitle = value; }
        public int Quantity { get => _quantity; init => _quantity = value; }

        public override string ToString()
        {
            return $"Book: {_bookTitle} Quantity: {_quantity}";
        }
    }
}
