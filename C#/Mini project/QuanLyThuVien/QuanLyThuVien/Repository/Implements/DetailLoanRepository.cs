﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Common.Helpers;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;

namespace QuanLyThuVien.Repository.Implements
{
    public class DetailLoanRepository : GenericRepository<DetailLoan>, IDetailLoanRepository
    {
        public DetailLoanRepository()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.DetailLoansFileName);
            _dbSet = JsonHelper.LoadJsonFile<DetailLoan>(path);
        }

        public DetailLoan GetById(int id)
        {
            return _dbSet.SingleOrDefault(x => x.Id == id)!;
        }

        public int GetLatestId()
        {
            DetailLoan detailLoan = _dbSet.LastOrDefault()!;

            return detailLoan.Id;
        }

        public ICollection<DetailLoan> GetByBookId(int bookId)
        {
            return _dbSet.Where(x => x.BookId == bookId).ToList();
        }

        public ICollection<DetailLoan> GetByLoanId(int loanId)
        {
            return _dbSet.Where(x => x.LoanId == loanId).ToList();
        }

        public void SaveChanges()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.DetailLoansFileName);
            JsonHelper.SaveJsonFile(path, _dbSet);
        }

        public void FetchData()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.DetailLoansFileName);
            _dbSet = JsonHelper.LoadJsonFile<DetailLoan>(path);
        }
    }
}
