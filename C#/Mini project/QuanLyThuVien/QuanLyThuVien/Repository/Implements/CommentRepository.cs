﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Common.Helpers;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;

namespace QuanLyThuVien.Repository.Implements
{
    public class CommentRepository : GenericRepository<Comment>, ICommentRepository
    {
        public CommentRepository()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.CommentsFileName);
            _dbSet = JsonHelper.LoadJsonFile<Comment>(path);
        }

        public Comment GetById(int id)
        {
            return _dbSet.SingleOrDefault(x => x.Id == id)!;
        }

        public int GetLatestId()
        {
            Comment comment = _dbSet.LastOrDefault()!;

            return comment.Id;
        }

        public ICollection<Comment> GetByBookId(int bookId)
        {
            return _dbSet.Where(x => x.BookId == bookId).ToList();
        }

        public void SaveChanges()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.CommentsFileName);
            JsonHelper.SaveJsonFile(path, _dbSet);
        }

        public void FetchData()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.CommentsFileName);
            _dbSet = JsonHelper.LoadJsonFile<Comment>(path);
        }
    }
}
