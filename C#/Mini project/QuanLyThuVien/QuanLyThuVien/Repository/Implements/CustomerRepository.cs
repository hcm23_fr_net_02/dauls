﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Common.Helpers;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;

namespace QuanLyThuVien.Repository.Implements
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.CustomersFileName);
            _dbSet = JsonHelper.LoadJsonFile<Customer>(path);
        }

        public Customer GetById(int id)
        {
            return _dbSet.SingleOrDefault(x => x.Id == id)!;
        }

        public Customer GetByPhoneNumber(long phoneNumber)
        {
            return _dbSet.SingleOrDefault(x => x.PhoneNumber == phoneNumber)!;
        }

        public ICollection<Customer> GetCustomersByName(string name)
        {
            return _dbSet.Where(x => x.Name.ToLower().Contains(name.ToLower().Trim())).ToList();
        }

        public int GetLatestId()
        {
            Customer customer = _dbSet.LastOrDefault()!;

            return customer.Id;
        }

        public void Update(int id, Customer newCustomer)
        {
            Customer customer = _dbSet.SingleOrDefault(x => x.Id == id)!;

            customer.Name = newCustomer.Name;
            customer.Address = newCustomer.Address;
            customer.PhoneNumber = newCustomer.PhoneNumber;
        }

        public void Delete(Customer customer)
        {
            _dbSet.Remove(customer);
        }

        public void SaveChanges()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.CustomersFileName);
            JsonHelper.SaveJsonFile(path, _dbSet);
        }

        public void FetchData()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.CustomersFileName);
            _dbSet = JsonHelper.LoadJsonFile<Customer>(path);
        }
    }
}
