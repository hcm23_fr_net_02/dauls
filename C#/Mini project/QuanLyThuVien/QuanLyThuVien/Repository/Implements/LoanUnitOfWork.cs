﻿using QuanLyThuVien.Repository.Contracts;

namespace QuanLyThuVien.Repository.Implements
{
    public class LoanUnitOfWork : ILoanUnitOfWork
    {
        private readonly IDetailLoanRepository _detailLoanRepository;
        private readonly ILoanRepository _loanRepository;
        private readonly IBookRepository _bookRepository;

        public LoanUnitOfWork(IDetailLoanRepository detailLoanRepository, ILoanRepository loanRepository, IBookRepository bookRepository)
        {
            _detailLoanRepository = detailLoanRepository;
            _loanRepository = loanRepository;
            _bookRepository = bookRepository;
        }

        public IDetailLoanRepository DetailLoanRepository => _detailLoanRepository;

        public ILoanRepository LoanRepository => _loanRepository;

        public IBookRepository BookRepository => _bookRepository;

        public void SaveChanges()
        {
            _detailLoanRepository.SaveChanges();
            _loanRepository.SaveChanges();
            _bookRepository.SaveChanges();
        }

        public void FetchData()
        {
            _detailLoanRepository.FetchData();
            _loanRepository.FetchData();
            _bookRepository.FetchData();
        }
    }
}
