﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Common.Helpers;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;

namespace QuanLyThuVien.Repository.Implements
{
    public class LoanRepository : GenericRepository<Loan>, ILoanRepository
    {
        public LoanRepository()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.LoansFileName);
            _dbSet = JsonHelper.LoadJsonFile<Loan>(path);
        }

        public Loan GetById(int id)
        {
            return _dbSet.SingleOrDefault(x => x.Id == id)!;
        }

        public int GetLatestId()
        {
            Loan loan = _dbSet.LastOrDefault()!;

            return loan.Id;
        }

        public void Update(int id, Loan newLoan)
        {
            Loan loan = _dbSet.SingleOrDefault(x => x.Id == id)!;

            loan.CustomerId = newLoan.CustomerId;
            loan.DateStart = newLoan.DateStart;
            loan.DateEnd = newLoan.DateEnd;
            loan.IsReturn = newLoan.IsReturn;
        }

        public void SaveChanges()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.LoansFileName);
            JsonHelper.SaveJsonFile(path, _dbSet);
        }

        public void FetchData()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.LoansFileName);
            _dbSet = JsonHelper.LoadJsonFile<Loan>(path);
        }
    }
}
