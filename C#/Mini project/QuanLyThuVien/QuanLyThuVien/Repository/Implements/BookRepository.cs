﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Common.Helpers;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;

namespace QuanLyThuVien.Repository.Implements
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        public BookRepository()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.BooksFileName);
            _dbSet = JsonHelper.LoadJsonFile<Book>(path);
        }

        public Book GetById(int id)
        {
            return _dbSet.SingleOrDefault(x => x.Id == id)!;
        }

        public int GetLatestId()
        {
            Book book = _dbSet.LastOrDefault()!;

            return book.Id;
        }

        public ICollection<Book> GetByAuthor(string author)
        {
            return _dbSet.Where(x => x.Author!.ToLower().Contains(author.ToLower().Trim())).ToList();
        }

        public ICollection<Book> GetByGenre(string genre)
        {
            return _dbSet.Where(x => x.Genre.ToLower().Contains(genre.ToLower().Trim())).ToList();
        }

        public ICollection<Book> GetByPublicationYear(int publicationYear)
        {
            return _dbSet.Where(x => x.PublicationYear == publicationYear).ToList();
        }

        public ICollection<Book> GetByTitle(string title)
        {
            return _dbSet.Where(x => x.Title!.ToLower().Contains(title.ToLower().Trim())).ToList();
        }

        public ICollection<Book> GetAvailableBooks()
        {
            return _dbSet.Where(x => x.IsAvailable == true).ToList();
        }

        public void Update(int id, Book newBook)
        {
            Book book = _dbSet.SingleOrDefault(x => x.Id == id)!;

            book.Title = newBook.Title;
            book.Author = newBook.Author;
            book.PublicationYear = newBook.PublicationYear;
            book.Quantity = newBook.Quantity;
            book.IsAvailable = newBook.IsAvailable;
            book.Genre = newBook.Genre;
        }

        public void Delete(Book book)
        {
            _dbSet.Remove(book);
        }

        public void SaveChanges()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.BooksFileName);
            JsonHelper.SaveJsonFile(path, _dbSet);
        }

        public void FetchData()
        {
            string path = Path.Combine(FileNameConstants.Folder, FileNameConstants.BooksFileName);
            _dbSet = JsonHelper.LoadJsonFile<Book>(path);
        }
    }
}
