﻿using QuanLyThuVien.Repository.Contracts;

namespace QuanLyThuVien.Repository.Implements
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected ICollection<T> _dbSet = new List<T>();

        public void Insert(T entity)
        {
            _dbSet.Add(entity);
        }

        public ICollection<T> GetAll()
        {
            return _dbSet;
        }
    }
}
