﻿using QuanLyThuVien.Models;

namespace QuanLyThuVien.Repository.Contracts
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        public Customer GetById(int id);
        public int GetLatestId();
        public Customer GetByPhoneNumber(long phoneNumber);
        public ICollection<Customer> GetCustomersByName(string name);
        public void Update(int id, Customer newCustomer);
        public void Delete(Customer customer);
        public void SaveChanges();
        public void FetchData();
    }
}
