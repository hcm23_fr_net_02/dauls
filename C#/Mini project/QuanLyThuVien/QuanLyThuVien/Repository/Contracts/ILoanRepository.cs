﻿using QuanLyThuVien.Models;

namespace QuanLyThuVien.Repository.Contracts
{
    public interface ILoanRepository : IGenericRepository<Loan>
    {
        public Loan GetById(int id);
        public int GetLatestId();
        public void Update(int id, Loan newLoan);
        public void SaveChanges();
        public void FetchData();
    }
}
