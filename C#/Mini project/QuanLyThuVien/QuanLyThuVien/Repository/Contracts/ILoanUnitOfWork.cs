﻿namespace QuanLyThuVien.Repository.Contracts
{
    public interface ILoanUnitOfWork
    {
        public IDetailLoanRepository DetailLoanRepository { get; }
        public ILoanRepository LoanRepository { get; }
        public IBookRepository BookRepository { get; }
        public void SaveChanges();
        public void FetchData();
    }
}
