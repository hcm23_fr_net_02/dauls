﻿using QuanLyThuVien.Models;

namespace QuanLyThuVien.Repository.Contracts
{
    public interface IDetailLoanRepository : IGenericRepository<DetailLoan>
    {
        public DetailLoan GetById(int id);
        public int GetLatestId();
        public ICollection<DetailLoan> GetByLoanId(int loanId);
        public ICollection<DetailLoan> GetByBookId(int bookId);
        public void SaveChanges();
        public void FetchData();
    }
}
