﻿using QuanLyThuVien.Models;

namespace QuanLyThuVien.Repository.Contracts
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        public Book GetById(int id);
        public int GetLatestId();
        public ICollection<Book> GetByTitle(string title);
        public ICollection<Book> GetByAuthor(string author);
        public ICollection<Book> GetByPublicationYear(int publicationYear);
        public ICollection<Book> GetByGenre(string genre);
        public ICollection<Book> GetAvailableBooks();
        public void Update(int id, Book newBook);
        public void Delete(Book book);
        public void SaveChanges();
        public void FetchData();
    }
}
