﻿namespace QuanLyThuVien.Repository.Contracts
{
    public interface IGenericRepository<T> where T : class
    {
        public void Insert(T entity);
        public ICollection<T> GetAll();
    }
}