﻿using QuanLyThuVien.Models;

namespace QuanLyThuVien.Repository.Contracts
{
    public interface ICommentRepository : IGenericRepository<Comment>
    {
        public Comment GetById(int id);
        public int GetLatestId();
        public ICollection<Comment> GetByBookId(int bookId);
        public void SaveChanges();
        public void FetchData();
    }
}
