﻿namespace QuanLyThuVien.Common.Constants
{
    public static class MenuConstants
    {
        public const string AddMenu = "-------------------------Add-------------------------";
        public const string GetDetailMenu = "-------------------------Detail-------------------------";
        public const string UpdateMenu = "-------------------------Update-------------------------";
        public const string DeleteMenu = "-------------------------Delete-------------------------";

        public const string BorrowBooksMenu = "-------------------------Borrow-------------------------";
        public const string ReturnBooksMenu = "-------------------------Return-------------------------";

        public const string MainMenu = "-------------------------Main Menu-------------------------";
        public const string ManageBooks = "{0}. Manage books in library";
        public const string FilterBooks = "{0}. Filter books in library by Title, Author, Publication Year, Genre";
        public const string ManageLoans = "{0}. Manage loans in library";
        public const string ManageCustomers = "{0}. Manage customers in library";
        public const string InputChoice = "Input your choice: ";
        public const string Exit = "{0}. Exit";

        public const string PaginationMenu = "-------------------------Pagination Menu-------------------------";
        public const string NextPage = "Press 'N' to move to next page";
        public const string PreviousPage = "Press 'P' to move to previous page";
        public const string BackMenu = "Press 'B' to back to the main menu";
        public const string SubChoice = "Press 'I' to see more features";
        public const string ElementsOnCurrentPage = "Elements on page number {0}";
        public const string InputPaginationChoice = "Please input your choice (N, P, B, I): ";
        public const string InputSubPaginationChoice = "Please input your choice (N, P, B): ";        

        public const string BookSubMenu = "-------------------------Book Sub Menu-------------------------";
        public const string AddBook = "{0}. Add new book";
        public const string InputBookId = "Input id of book: ";
        public const string GetDetailBook = "{0}. Get detail book information";
        public const string UpdateBook = "{0}. Update book information";
        public const string DeleteBook = "{0}. Delete book information";
        public const string AddComment = "{0}. Add a comment for a book";
        public const string PrintAllComments = "{0}. Print all comments of a book";

        public const string FilterBooksMenu = "-------------------------Filter Books Menu-------------------------";
        public const string FilterBooksByAuthor = "{0}. Filter books by name of author";
        public const string FilterBooksByGenre = "{0}. Filter books by genre";
        public const string FilterBooksByPublicationYear = "{0}. Filter books by publication year";
        public const string FilterBooksByTitle = "{0}. Filter books by title";

        public const string CustomerSubMenu = "-------------------------Customer Sub Menu-------------------------";
        public const string AddCustomer = "{0}. Add new customer";
        public const string InputCustomerId = "Input id of customer: ";
        public const string GetDetailCustomer = "{0}. Get detail customer information";
        public const string UpdateCustomer = "{0}. Update customer information";
        public const string DeleteCustomer = "{0}. Delete customer information";

        public const string LoanSubMenu = "-------------------------Loan Sub Menu-------------------------";
        public const string BorrowBooks = "{0}. Borrow books";
        public const string ReturnBooks = "{0}. Return books";
        public const string FilterLoansByCustomerName = "{0}. Filter loans by customer name";
        public const string FilterLoansByBookTitle = "{0}. Filter loans by book title";
        public const string PrintDetailLoan = "{0}. Print detail loan";
    }
}
