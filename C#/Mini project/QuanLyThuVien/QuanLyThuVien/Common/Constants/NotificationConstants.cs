﻿namespace QuanLyThuVien.Common.Constants
{
    public static class NotificationConstants
    {
        public const string FilePathNotFound = "File path not found, path: ";
        public const string AllInfoMustBeFilled = "All information fields must be filled";
        public const string InvalidNumber = "Number must be greater than or equal zero";
        public const string StringNotAbleToBeEmpty = "String not able to be empty";
        public const string FieldMustBeANumber = "This field must be a number";
        public const string IdNotExist = "This id does not exist";
        public const string ObjectDoesNotExist = "This object does not exist";
        public const string Empty = "List have no elements";
        public const string Success = "Success";
        public const string DateStartMustEarlierThanDateEnd = "DateStart must be earlier than DateEnd";
        public const string DateEndMustBeLaterThanDateStart = "DateEnd must be later than DateStart";
        public const string InvalidPublicationYear = "Publication year cannot be later than current year";
        public const string InvalidPhoneNumber = "Phone number must contain 10 digits";
        public const string InvalidRateNumber = "Rate number must between 1-3";
        public const string BookNotAvailable = "This book is not available";
        public const string BookHasBeenBorrowed = "This book has been borrowed";
        public const string CustomerHasNotReturned = "This customer has not returned books";
        public const string LoanHasBeenReturned = "All of books of this loan have been returned";
        public const string PhoneNumberExists = "Phone number exists";
    }
}
