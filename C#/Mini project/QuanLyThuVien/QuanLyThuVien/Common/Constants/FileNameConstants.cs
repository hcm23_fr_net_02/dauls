﻿namespace QuanLyThuVien.Common.Constants
{
    public static class FileNameConstants
    {
        public const string Folder = "JsonFiles";
        public const string BooksFileName = "books.json";
        public const string CustomersFileName = "customers.json";
        public const string CommentsFileName = "comments.json";
        public const string DetailLoansFileName = "detail_loans.json";
        public const string GenresFileName = "genres.json";
        public const string LoansFileName = "loans.json";
    }
}
