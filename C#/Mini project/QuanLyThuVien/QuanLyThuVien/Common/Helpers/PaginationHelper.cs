﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Models;
using QuanLyThuVien.Views;

namespace QuanLyThuVien.Common.Helpers
{
    public static class PaginationHelper
    {
        private static readonly int _elementsOnPage = 5;

        public static ICollection<T> SetPagination<T>(ICollection<T> collection, ref int currentPage, out double pages)
        {
            pages = Math.Ceiling(collection.Count * 1.0 / _elementsOnPage);

            var paging = collection.Skip((currentPage - 1) * _elementsOnPage).Take(_elementsOnPage).ToList();

            return paging;
        }

        public static void PrintPagination<T>(ICollection<T> collection)
        {
            if (collection.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
        }

        public static void NavigatePagination<T>(ICollection<T> collection)
        {
            int currentPage = 1;
            double pages;
            ConsoleKeyInfo pagingChoice = new ConsoleKeyInfo();

            while (pagingChoice.Key != ConsoleKey.B)
            {
                Console.Clear();
                var paging = SetPagination(collection, ref currentPage, out pages);

                try
                {
                    PrintPagination(paging);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Menu.PrintSubPaginationMenu(currentPage, pages);
                pagingChoice = Console.ReadKey();

                switch (pagingChoice.Key)
                {
                    case ConsoleKey.N:

                        if (currentPage < pages)
                        {
                            currentPage++;
                        }

                        break;

                    case ConsoleKey.P:

                        if (currentPage > 1)
                        {
                            currentPage--;
                        }

                        break;

                    default:
                        break;
                }
            }
        }
    }
}
