﻿using Newtonsoft.Json;
using QuanLyThuVien.Common.Constants;
using System.Text;

namespace QuanLyThuVien.Common.Helpers
{
    public static class JsonHelper
    {
        public static ICollection<T> LoadJsonFile<T>(string fileName)
        {
            if (!File.Exists(fileName))
            {
                StringBuilder message = new StringBuilder(NotificationConstants.FilePathNotFound);
                message.Append(fileName);
                throw new FileNotFoundException(message.ToString());
            }

            using (var fileStream = File.OpenRead(fileName))
            using (var streamReader = new StreamReader(fileStream))
            using (var jsonReader = new JsonTextReader(streamReader))
            {
                var serializer = new JsonSerializer();
                return serializer.Deserialize<ICollection<T>>(jsonReader)!;
            }
        }

        public static void SaveJsonFile<T>(string fileName, ICollection<T> enumerable)
        {
            if (!File.Exists(fileName))
            {
                StringBuilder message = new StringBuilder(NotificationConstants.FilePathNotFound);
                message.Append(fileName);
                throw new FileNotFoundException(message.ToString());
            }

            using (var fileStream = File.Create(fileName))
            using (var streamWriter = new StreamWriter(fileStream))
            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(jsonWriter, enumerable);
            }
        }
    }
}
