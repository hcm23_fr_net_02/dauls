﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;
using QuanLyThuVien.Service.Contracts;

namespace QuanLyThuVien.Service.Implements
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly ILoanRepository _loanRepository;

        public CustomerService(ICustomerRepository customerRepository, ILoanRepository loanRepository)
        {
            _customerRepository = customerRepository;
            _loanRepository = loanRepository;
        }

        public ICollection<Customer> GetAll()
        {
            ICollection<Customer> customers = _customerRepository.GetAll();

            if (customers.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return customers;
        }

        public Customer GetById(int id)
        {
            if (id < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            Customer customer = _customerRepository.GetById(id);

            if (customer == null)
            {
                throw new ArgumentException(NotificationConstants.IdNotExist);
            }

            return customer;
        }

        public Customer GetByPhone(long phoneNumber)
        {
            if (phoneNumber.ToString().Length != 10)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            Customer customer = _customerRepository.GetByPhoneNumber(phoneNumber);

            if (customer == null)
            {
                throw new ArgumentException(NotificationConstants.ObjectDoesNotExist);
            }

            return customer;
        }

        public ICollection<Customer> GetCustomersByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(NotificationConstants.AllInfoMustBeFilled);
            }

            ICollection<Customer> customers = _customerRepository.GetCustomersByName(name);

            if (customers.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return customers;
        }

        public int AddCustomer(string name, string address, long phoneNumber)
        {
            Customer customer = _customerRepository.GetByPhoneNumber(phoneNumber);

            if (customer != null)
            {
                throw new ArgumentException(NotificationConstants.PhoneNumberExists);
            }

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(address))
            {
                throw new ArgumentNullException(NotificationConstants.AllInfoMustBeFilled);
            }

            if (phoneNumber.ToString().Length != 10)
            {
                throw new ArgumentException(NotificationConstants.InvalidPhoneNumber);
            }

            int id = _customerRepository.GetLatestId() + 1;

            Customer newCustomer = new Customer()
            {
                Id = id,
                Name = name,
                Address = address,
                PhoneNumber = phoneNumber
            };

            try
            {
                _customerRepository.Insert(newCustomer);
                _customerRepository.SaveChanges();
            }
            catch (Exception)
            {
                _customerRepository.FetchData();
                throw;
            }

            return newCustomer.Id;
        }

        public void UpdateCustomer(int id, string name, string address, long phoneNumber)
        {
            if (id < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(address))
            {
                throw new ArgumentNullException(NotificationConstants.AllInfoMustBeFilled);
            }

            if (phoneNumber.ToString().Length != 10)
            {
                throw new ArgumentException(NotificationConstants.InvalidPhoneNumber);
            }

            Customer newCustomer = new Customer()
            {
                Name = name,
                Address = address,
                PhoneNumber = phoneNumber
            };

            try
            {
                _customerRepository.Update(id, newCustomer);
                _customerRepository.SaveChanges();
            }
            catch (Exception)
            {
                _customerRepository.FetchData();
                throw;
            }
        }

        public void DeleteCustomer(int id)
        {
            if (id < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            Customer customer = _customerRepository.GetById(id);

            if (customer == null)
            {
                throw new ArgumentException(NotificationConstants.IdNotExist);
            }

            bool isNotReturn = _loanRepository.GetAll().Any(x => x.CustomerId == customer.Id && x.IsReturn == false);

            if (isNotReturn)
            {
                throw new Exception(NotificationConstants.CustomerHasNotReturned);
            }

            try
            {
                _customerRepository.Delete(customer);
                _customerRepository.SaveChanges();
            }
            catch (Exception)
            {
                _customerRepository.FetchData();
                throw;
            }
        }
    }
}
