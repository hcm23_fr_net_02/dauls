﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;
using QuanLyThuVien.Service.Contracts;

namespace QuanLyThuVien.Service.Implements
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;
        private readonly IDetailLoanRepository _detailLoanRepository;

        public BookService(IBookRepository bookRepository, IDetailLoanRepository detailLoanRepository)
        {
            _bookRepository = bookRepository;
            _detailLoanRepository = detailLoanRepository;
        }

        public ICollection<Book> GetAll()
        {
            ICollection<Book> books = _bookRepository.GetAll();

            if (books.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return books;
        }

        public Book GetById(int id)
        {
            if (id < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            Book book = _bookRepository.GetById(id);

            if (book == null)
            {
                throw new ArgumentException(NotificationConstants.IdNotExist);
            }

            return book;
        }

        public ICollection<Book> GetAvailableBooks()
        {
            ICollection<Book> books = _bookRepository.GetAvailableBooks();

            if (books.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return books;
        }

        public ICollection<Book> GetBooksByAuthor(string author)
        {
            if (string.IsNullOrEmpty(author))
            {
                throw new ArgumentNullException(NotificationConstants.AllInfoMustBeFilled);
            }

            ICollection<Book> books = _bookRepository.GetByAuthor(author);

            if (books.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return books;
        }

        public ICollection<Book> GetBooksByGenre(string genre)
        {
            if (string.IsNullOrEmpty(genre))
            {
                throw new ArgumentNullException(NotificationConstants.AllInfoMustBeFilled);
            }

            ICollection<Book> books = _bookRepository.GetByGenre(genre);

            if (books.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return books;
        }

        public ICollection<Book> GetBooksByPublicationYear(int publicationYear)
        {
            if (publicationYear < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            if (publicationYear > DateTime.Now.Year)
            {
                throw new ArgumentException(NotificationConstants.InvalidPublicationYear);
            }

            ICollection<Book> books = _bookRepository.GetByPublicationYear(publicationYear);

            if (books.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return books;
        }

        public ICollection<Book> GetBooksByTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException(NotificationConstants.AllInfoMustBeFilled);
            }

            ICollection<Book> books = _bookRepository.GetByTitle(title);

            if (books.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return books;
        }

        public void UpdateBook(int id, string title, string author, int publicationYear, int quantity, string genre)
        {
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(author))
            {
                throw new ArgumentNullException(NotificationConstants.AllInfoMustBeFilled);
            }

            if (id < 0 || publicationYear < 0 || quantity < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            if (publicationYear > DateTime.Now.Year)
            {
                throw new ArgumentException(NotificationConstants.InvalidPublicationYear);
            }

            Book newBook = new Book()
            {
                Title = title,
                Author = author,
                PublicationYear = publicationYear,
                Quantity = quantity,
                Genre = genre
            };

            try
            {
                _bookRepository.Update(id, newBook);
                _bookRepository.SaveChanges();
            }
            catch (Exception)
            {
                _bookRepository.FetchData();
                throw;
            }
        }

        public void AddBook(string title, string author, int publicationYear, int quantity, string genre)
        {
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(author))
            {
                throw new ArgumentNullException(NotificationConstants.AllInfoMustBeFilled);
            }

            if (publicationYear < 0 || quantity < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            if (publicationYear > DateTime.Now.Year)
            {
                throw new ArgumentException(NotificationConstants.InvalidPublicationYear);
            }

            int id = _bookRepository.GetLatestId() + 1;

            Book newBook = new Book()
            {
                Id = id,
                Title = title,
                Author = author,
                PublicationYear = publicationYear,
                Quantity = quantity,
                Genre = genre
            };

            try
            {
                _bookRepository.Insert(newBook);
                _bookRepository.SaveChanges();
            }
            catch (Exception)
            {
                _bookRepository.FetchData();
                throw;
            }
        }

        public void DeleteBook(int id)
        {
            if (id < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            Book book = _bookRepository.GetById(id);

            if (book == null)
            {
                throw new ArgumentException(NotificationConstants.IdNotExist);
            }

            bool isBorrowed = _detailLoanRepository.GetAll().Any(x => x.BookId == book.Id);

            if (isBorrowed)
            {
                throw new ArgumentException(NotificationConstants.BookHasBeenBorrowed);
            }

            try
            {
                _bookRepository.Delete(book);
                _bookRepository.SaveChanges();
            }
            catch (Exception)
            {
                _bookRepository.FetchData();
                throw;
            }  
        }
    }
}