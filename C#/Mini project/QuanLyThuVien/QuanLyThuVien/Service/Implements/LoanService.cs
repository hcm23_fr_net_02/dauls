﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;
using QuanLyThuVien.Service.Contracts;
using QuanLyThuVien.ViewModels;

namespace QuanLyThuVien.Service.Implements
{
    public class LoanService : ILoanService
    {
        private readonly ILoanUnitOfWork _loanUnitOfWork;
        private readonly ICustomerRepository _customerRepository;

        public LoanService(ILoanUnitOfWork loanUnitOfWork, ICustomerRepository customerRepository)
        {
            _loanUnitOfWork = loanUnitOfWork;
            _customerRepository = customerRepository;
        }

        public ICollection<LoanVM> GetAll()
        {
            ICollection<Loan> loans = _loanUnitOfWork.LoanRepository.GetAll();
            ICollection<Customer> customers = _customerRepository.GetAll();

            var loansVM = loans.Join(customers, l => l.CustomerId, c => c.Id, (l, c) => new LoanVM()
            {
                Id = l.Id,
                CustomerName = c.Name,
                DateStart = l.DateStart,
                DateEnd = l.DateEnd,
                IsReturn = l.IsReturn
            }).ToList();

            if (loansVM.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return loansVM;
        }

        public LoanVM GetLoanById(int id)
        {
            if (id < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            Loan loan = _loanUnitOfWork.LoanRepository.GetById(id);

            if (loan == null)
            {
                throw new ArgumentException(NotificationConstants.IdNotExist);
            }

            Customer customer = _customerRepository.GetById(loan.CustomerId);

            if (customer == null)
            {
                throw new ArgumentException(NotificationConstants.IdNotExist);
            }

            LoanVM loanVM = new LoanVM()
            {
                Id = loan.Id,
                CustomerName = customer.Name,
                DateStart = loan.DateStart,
                DateEnd = loan.DateEnd,
                IsReturn = loan.IsReturn
            };

            return loanVM;
        }

        public ICollection<DetailLoanVM> GetDetailLoansByLoanId(int loanId)
        {
            ICollection<DetailLoan> detailLoans = _loanUnitOfWork.DetailLoanRepository.GetByLoanId(loanId);
            ICollection<Book> books = _loanUnitOfWork.BookRepository.GetAll();

            var detailLoansVM = detailLoans.Join(books, dl => dl.BookId, b => b.Id, (dl, b) => new DetailLoanVM()
            {
                Id = dl.Id,
                LoanId = dl.LoanId,
                BookTitle = b.Title!,
                Quantity = dl.Quantity
            }).ToList();

            if (detailLoansVM.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return detailLoansVM;
        }

        public ICollection<LoanVM> GetLoansByCustomerName(string customerName)
        {
            ICollection<Customer> customers = _customerRepository.GetCustomersByName(customerName);
            ICollection<Loan> loans = _loanUnitOfWork.LoanRepository.GetAll();

            var loansVM = loans.Join(customers, l => l.CustomerId, c => c.Id, (l, c) => new LoanVM()
            {
                Id = l.Id,
                CustomerName = c.Name,
                DateStart = l.DateStart,
                DateEnd = l.DateEnd,
                IsReturn = l.IsReturn
            }).ToList();

            if (loansVM.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return loansVM;
        }

        public ICollection<LoanVM> GetLoansByBookTitle(string bookTitle)
        {
            ICollection<Book> books = _loanUnitOfWork.BookRepository.GetByTitle(bookTitle);
            ICollection<DetailLoan> detailLoans = _loanUnitOfWork.DetailLoanRepository.GetAll();
            ICollection<Loan> loans = _loanUnitOfWork.LoanRepository.GetAll();
            ICollection<Customer> customers = _customerRepository.GetAll();

            var detailLoansWithBook = detailLoans.Join(books, dl => dl.BookId, b => b.Id, (dl, b) => dl);
            var loansVM = loans.Join(detailLoansWithBook, l => l.Id, dl => dl.LoanId, (l, dl) => l)
                                .Distinct()
                                .Join(customers, l => l.CustomerId, c => c.Id, (l, c) => new LoanVM()
                                {
                                    Id = l.Id,
                                    CustomerName = c.Name,
                                    DateStart = l.DateStart,
                                    DateEnd = l.DateEnd,
                                    IsReturn = l.IsReturn
                                })
                                .ToList();

            if (loansVM.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return loansVM;
        }

        #region Borrow book functions

        public void BorrowBook(int customerId, DateTime dateEnd, Dictionary<int, int> bookQuantity)
        {
            try
            {
                int loanId = AddLoan(customerId, dateEnd);
                AddDetailLoan(loanId, bookQuantity);
                _loanUnitOfWork.SaveChanges();
            }
            catch (Exception)
            {
                _loanUnitOfWork.FetchData();
                throw;
            }
        }

        public int AddLoan(int customerId, DateTime dateEnd)
        {
            if (customerId < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            if (dateEnd < DateTime.Now)
            {
                throw new ArgumentException(NotificationConstants.DateEndMustBeLaterThanDateStart);
            }

            int id = _loanUnitOfWork.LoanRepository.GetLatestId() + 1;

            Loan newLoan = new Loan()
            {
                Id = id,
                CustomerId = customerId,
                DateStart = DateTime.Now,
                DateEnd = dateEnd,
                IsReturn = false,
            };

            _loanUnitOfWork.LoanRepository.Insert(newLoan);

            return newLoan.Id;
        }

        public void AddDetailLoan(int loanId, Dictionary<int, int> bookQuantity)
        {
            foreach (KeyValuePair<int, int> item in bookQuantity)
            {
                Book book = _loanUnitOfWork.BookRepository.GetById(item.Key);

                if (book.Quantity < item.Value)
                {
                    throw new Exception(NotificationConstants.BookNotAvailable);
                }

                int id = _loanUnitOfWork.DetailLoanRepository.GetLatestId() + 1;

                DetailLoan newDetailLoan = new DetailLoan()
                {
                    Id = id,
                    LoanId = loanId,
                    BookId = item.Key,
                    Quantity = item.Value
                };

                _loanUnitOfWork.DetailLoanRepository.Insert(newDetailLoan);

                book.Quantity -= newDetailLoan.Quantity;

                if (book.Quantity < 1)
                {
                    book.IsAvailable = false;
                }

                _loanUnitOfWork.BookRepository.Update(book.Id, book);
            }
        }

        #endregion

        #region Return book functions

        public void ReturnBook(int loanId)
        {
            try
            {
                ICollection<DetailLoan> detailLoans = GetDetailLoansByLoanIdToReturn(loanId);
                UpdateBookWhenReturn(detailLoans);
                _loanUnitOfWork.SaveChanges();
            }
            catch (Exception)
            {
                _loanUnitOfWork.FetchData();
                throw;
            }

        }

        public ICollection<DetailLoan> GetDetailLoansByLoanIdToReturn(int loanId)
        {
            Loan loan = _loanUnitOfWork.LoanRepository.GetById(loanId);

            if (loan == null)
            {
                throw new ArgumentException(NotificationConstants.IdNotExist);
            }

            if (loan.IsReturn)
            {
                throw new Exception(NotificationConstants.LoanHasBeenReturned);
            }

            ICollection<DetailLoan> detailLoans = _loanUnitOfWork.DetailLoanRepository.GetByLoanId(loan.Id);

            if (detailLoans.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            loan.IsReturn = true;
            _loanUnitOfWork.LoanRepository.Update(loan.Id, loan);

            return detailLoans;
        }

        public void UpdateBookWhenReturn(ICollection<DetailLoan> detailLoans)
        {
            foreach (DetailLoan detailLoan in detailLoans)
            {
                Book book = _loanUnitOfWork.BookRepository.GetById(detailLoan.BookId);

                book.Quantity += detailLoan.Quantity;

                if (book.Quantity > 0)
                {
                    book.IsAvailable = true;
                }

                _loanUnitOfWork.BookRepository.Update(book.Id, book);
            }
        }

        #endregion
    }
}