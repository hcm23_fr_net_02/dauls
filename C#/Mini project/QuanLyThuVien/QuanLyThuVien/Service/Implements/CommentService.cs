﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Common.Enums;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;
using QuanLyThuVien.Service.Contracts;

namespace QuanLyThuVien.Service.Implements
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public void AddComment(int bookId, string header, string content, BookRate rate)
        {
            if (bookId < 0)
            {
                throw new ArgumentException(NotificationConstants.InvalidNumber);
            }

            int id = _commentRepository.GetLatestId() + 1;

            Comment newComment = new Comment()
            {
                Id = id,
                BookId = bookId,
                Header = header,
                Content = content,
                Rate = rate,
                CommentDate = DateTime.Now
            };

            try
            {
                _commentRepository.Insert(newComment);
                _commentRepository.SaveChanges();
            }
            catch (Exception)
            {
                _commentRepository.FetchData();
                throw;
            }
        }

        public ICollection<Comment> GetCommentsByBookId(int bookId)
        {
            ICollection<Comment> comments = _commentRepository.GetByBookId(bookId);

            if (comments.Count < 1)
            {
                throw new Exception(NotificationConstants.Empty);
            }

            return comments;
        }
    }
}
