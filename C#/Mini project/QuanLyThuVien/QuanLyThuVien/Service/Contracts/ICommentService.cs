﻿using QuanLyThuVien.Common.Enums;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;

namespace QuanLyThuVien.Service.Contracts
{
    public interface ICommentService
    {
        public void AddComment(int bookId, string header, string content, BookRate rate);
        public ICollection<Comment> GetCommentsByBookId(int bookId);
    }
}
