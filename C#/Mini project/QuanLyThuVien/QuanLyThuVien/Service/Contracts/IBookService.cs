﻿using QuanLyThuVien.Models;

namespace QuanLyThuVien.Service.Contracts
{
    public interface IBookService
    {
        public ICollection<Book> GetAll();
        public Book GetById(int id);
        public ICollection<Book> GetAvailableBooks();
        public ICollection<Book> GetBooksByAuthor(string author);
        public ICollection<Book> GetBooksByGenre(string genre);
        public ICollection<Book> GetBooksByPublicationYear(int publicationYear);
        public ICollection<Book> GetBooksByTitle(string title);
        public void AddBook(string title, string author, int publicationYear, int quantity, string genre);
        public void UpdateBook(int id, string title, string author, int publicationYear, int quantity, string genre);
        public void DeleteBook(int id);
    }
}
