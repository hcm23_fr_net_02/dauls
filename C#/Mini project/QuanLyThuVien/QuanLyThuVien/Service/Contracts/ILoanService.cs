﻿using QuanLyThuVien.Models;
using QuanLyThuVien.ViewModels;

namespace QuanLyThuVien.Service.Contracts
{
    public interface ILoanService
    {
        public ICollection<LoanVM> GetAll();
        public LoanVM GetLoanById(int id);
        public ICollection<DetailLoanVM> GetDetailLoansByLoanId(int loanId);
        public ICollection<LoanVM> GetLoansByCustomerName(string customerName);
        public ICollection<LoanVM> GetLoansByBookTitle(string bookTitle);
        public void BorrowBook(int customerId, DateTime dateEnd, Dictionary<int, int> bookQuantity);
        public int AddLoan(int customerId, DateTime dateEnd);
        public void AddDetailLoan(int loanId, Dictionary<int, int> bookQuantity);
        public void ReturnBook(int loanId);
        public ICollection<DetailLoan> GetDetailLoansByLoanIdToReturn(int loanId);
        public void UpdateBookWhenReturn(ICollection<DetailLoan> detailLoans);
    }
}
