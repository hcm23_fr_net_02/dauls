﻿using QuanLyThuVien.Models;

namespace QuanLyThuVien.Service.Contracts
{
    public interface ICustomerService
    {
        public ICollection<Customer> GetAll();
        public Customer GetById(int id);
        public Customer GetByPhone(long phoneNumber);
        public ICollection<Customer> GetCustomersByName(string name);
        public int AddCustomer(string name, string address, long phoneNumber);
        public void UpdateCustomer(int id, string name, string address, long phoneNumber);
        public void DeleteCustomer(int id);
    }
}
