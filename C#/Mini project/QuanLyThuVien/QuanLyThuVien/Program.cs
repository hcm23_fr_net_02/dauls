﻿using QuanLyThuVien.Repository.Implements;
using QuanLyThuVien.Service.Implements;
using QuanLyThuVien.Views;

try
{
    BookRepository bookRepository = new BookRepository();
    CommentRepository commentRepository = new CommentRepository();
    CustomerRepository customerRepository = new CustomerRepository();
    DetailLoanRepository detailLoanRepository = new DetailLoanRepository();
    LoanRepository loanRepository = new LoanRepository();
    LoanUnitOfWork loanUnitOfWork = new LoanUnitOfWork(detailLoanRepository, loanRepository, bookRepository);

    BookService bookService = new BookService(bookRepository, detailLoanRepository);
    CommentService commentService = new CommentService(commentRepository);
    CustomerService customerService = new CustomerService(customerRepository, loanRepository);
    LoanService loanService = new LoanService(loanUnitOfWork, customerRepository);

    ConsoleKeyInfo masterChoice = new ConsoleKeyInfo();

    while (masterChoice.Key != ConsoleKey.D5 && masterChoice.Key != ConsoleKey.NumPad5)
    {
        Menu.PrintMainMenu();
        masterChoice = Console.ReadKey();

        switch (masterChoice.Key)
        {
            case ConsoleKey.D1:
            case ConsoleKey.NumPad1:
                {
                    BookManager bookManager = new BookManager(bookService, commentService);
                    bookManager.SetMainFunctions();

                    break;
                }

            case ConsoleKey.D2:
            case ConsoleKey.NumPad2:
                {
                    BookManager bookManager = new BookManager(bookService, commentService);
                    bookManager.FilterBooks();

                    break;
                }

            case ConsoleKey.D3:
            case ConsoleKey.NumPad3:
                {
                    CustomerManager customerManager = new CustomerManager(customerService);
                    customerManager.SetMainFunctions();

                    break;
                }

            case ConsoleKey.D4:
            case ConsoleKey.NumPad4:
                {
                    LoanManager loanManager = new LoanManager(loanService, customerService, bookService);
                    loanManager.SetMainFunctions();

                    break;
                }

            default:
                break;
        }
    }
}
catch (Exception e)
{
    Console.WriteLine(e.Message);
}
