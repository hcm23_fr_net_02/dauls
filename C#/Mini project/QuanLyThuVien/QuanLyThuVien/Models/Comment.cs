﻿using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Common.Enums;

namespace QuanLyThuVien.Models
{
    public class Comment
    {
        private int _id;
        private int _bookId;
        private string _header = "";
        private string _content = "";
        private BookRate _rate;
        private DateTime _commentDate;

        public int Id
        {
            get { return _id; }
            init
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _id = value;
            }
        }

        public int BookId
        {
            get { return _bookId; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _bookId = value;
            }
        }

        public string? Header
        {
            get { return _header; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException(NotificationConstants.StringNotAbleToBeEmpty);
                }
                _header = value;
            }
        }

        public string? Content
        {
            get { return _content; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException(NotificationConstants.StringNotAbleToBeEmpty);
                }
                _content = value;
            }
        }

        public BookRate Rate
        {
            get { return _rate; }
            set { _rate = value; }
        }

        public DateTime CommentDate
        {
            get { return _commentDate; }
            set { _commentDate = value; }
        }

        public override string ToString()
        {
            return $"ID: {_id} Rate: {_rate} Date: {CommentDate}\nHeader: {_header}\nContent: {_content}";
        }
    }
}
