﻿using QuanLyThuVien.Common.Constants;

namespace QuanLyThuVien.Models
{
    public class DetailLoan
    {
        private int _id;
        private int _loanId;
        private int _bookId;
        private int _quantity;

        public int Id
        {
            get { return _id; }
            init
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _id = value;
            }
        }

        public int LoanId
        {
            get { return _loanId; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _loanId = value;
            }
        }

        public int BookId
        {
            get { return _bookId; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _bookId = value;
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _quantity = value;
            }
        }
    }
}
