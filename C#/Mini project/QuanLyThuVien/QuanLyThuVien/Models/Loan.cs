﻿using QuanLyThuVien.Common.Constants;

namespace QuanLyThuVien.Models
{
    public class Loan
    {
        private int _id;
        private int _customerId;
        private DateTime _dateStart;
        private DateTime _dateEnd;
        private bool _isReturn;

        public int Id
        {
            get { return _id; }
            init
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _id = value;
            }
        }

        public int CustomerId
        {
            get { return _customerId; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _customerId = value;
            }
        }

        public DateTime DateStart
        {
            get { return _dateStart; }
            set => _dateStart = value;
        }

        public DateTime DateEnd
        {
            get { return _dateEnd; }
            set
            {
                if (value <= _dateStart)
                {
                    throw new ArgumentException(NotificationConstants.DateEndMustBeLaterThanDateStart);
                }
                _dateEnd = value;
            }
        }

        public bool IsReturn
        {
            get { return _isReturn; }
            set { _isReturn = value; }
        }
    }
}
