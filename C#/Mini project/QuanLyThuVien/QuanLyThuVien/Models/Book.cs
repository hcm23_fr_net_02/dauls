﻿using QuanLyThuVien.Common.Constants;

namespace QuanLyThuVien.Models
{
    public class Book
    {
        private int _id;
        private string _title = "";
        private string _author = "";
        private int _publicationYear;
        private int _quantity;
        private bool _isAvailable;
        private string _genre = "";

        public int Id
        {
            get { return _id; }
            init
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _id = value;
            }
        }

        public string? Title
        {
            get { return _title; }
            set
            {
                if (value != null)
                    _title = value;
                else
                    throw new ArgumentNullException(NotificationConstants.StringNotAbleToBeEmpty);
            }
        }

        public string? Author
        {
            get { return _author; }
            set
            {
                if (value != null)
                    _author = value;
                else
                    throw new ArgumentNullException(NotificationConstants.StringNotAbleToBeEmpty);
            }
        }

        public int PublicationYear
        {
            get { return _publicationYear; }
            set
            {
                if (value >= 0)
                    _publicationYear = value;
                else
                    throw new ArgumentOutOfRangeException(NotificationConstants.InvalidNumber);
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                if (value >= 0)
                {
                    _quantity = value;

                    if (value > 0)
                        _isAvailable = true;
                    else
                        _isAvailable = false;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(NotificationConstants.InvalidNumber);
                }
            }
        }

        public bool IsAvailable
        {
            get { return _isAvailable; }
            set { _isAvailable = value; }
        }

        public string Genre
        {
            get { return _genre; }
            set
            {
                if (value != null)
                    _genre = value;
                else
                    throw new ArgumentNullException(NotificationConstants.StringNotAbleToBeEmpty);
            }
        }

        public override string ToString()
        {
            return $"ID: {_id} Title: {_title} Author: {_author} Publication Year: {_publicationYear} Genre: {_genre}";
        }
    }
}
