﻿using QuanLyThuVien.Common.Constants;

namespace QuanLyThuVien.Models
{
    public class Customer
    {
        private int _id;
        private string _name = "";
        private string _address = "";
        private long _phoneNumber;

        public int Id
        {
            get { return _id; }
            init
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _id = value;
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException(NotificationConstants.StringNotAbleToBeEmpty);
                }
                _name = value;
            }
        }

        public string Address
        {
            get { return _address; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException(NotificationConstants.StringNotAbleToBeEmpty);
                }
                _address = value;
            }
        }

        public long PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException(NotificationConstants.InvalidNumber);
                }
                _phoneNumber = value;
            }
        }

        public override string ToString()
        {
            return $"ID: {_id} Customer name: {_name}";
        }
    }
}
