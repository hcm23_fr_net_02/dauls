﻿using Moq;
using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;
using QuanLyThuVien.Service.Implements;

namespace TestQuanLyThuVien
{
    public class UnitTestCustomerService
    {
        [Fact]
        public void GetAll_ShouldReturnList()
        {
            //Arrange
            var mockCustomerRepository = new Mock<ICustomerRepository>();

            ICollection<Customer> fakeCustomers = new List<Customer>()
            {
                new Customer()
                {
                    Id = 0,
                    Name = "Customer A",
                    Address = "Address",
                    PhoneNumber = 1234567899
                },
                new Customer()
                {
                    Id = 1,
                    Name = "Customer B",
                    Address = "Address",
                    PhoneNumber = 1234567898
                }
            };

            mockCustomerRepository.Setup(b => b.GetAll()).Returns(fakeCustomers);
            var customerService = new CustomerService(mockCustomerRepository.Object, null!);

            //Act
            var customers = customerService.GetAll();

            //Assert
            Assert.NotNull(customers);
            Assert.Equal(2, customers.Count);
            Assert.Equal(fakeCustomers, customers);
        }

        [Fact]
        public void GetAll_ShouldThrowEmptyException()
        {
            //Arrange
            var customerRepository = new Mock<ICustomerRepository>();
            customerRepository.Setup(b => b.GetAll()).Returns(new List<Customer>());
            var customerService = new CustomerService(customerRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<Exception>(() => customerService.GetAll());
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void GetById_ShouldReturnObject()
        {
            //Arrange
            var mockCustomerRepository = new Mock<ICustomerRepository>();

            var fakeCustomer = new Customer()
            {
                Id = 0,
                Name = "Customer A",
                Address = "Address",
                PhoneNumber = 1234567899
            };

            mockCustomerRepository.Setup(b => b.GetById(0)).Returns(fakeCustomer);
            var customerService = new CustomerService(mockCustomerRepository.Object, null!);

            //Act
            var book = customerService.GetById(0);

            //Assert
            Assert.NotNull(book);
            Assert.Equal(fakeCustomer, book);
        }

        [Fact]
        public void GetById_ShouldThrowInvalidNumberException()
        {
            //Arrange
            var mockCustomerRepository = new Mock<ICustomerRepository>();
            var customerService = new CustomerService(mockCustomerRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentException>(() => customerService.GetById(-1));
        }

        [Fact]
        public void GetById_ShouldThrowIdNotExistException()
        {
            //Arrange
            var mockCustomerRepository = new Mock<ICustomerRepository>();
            var customerService = new CustomerService(mockCustomerRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentException>(() => customerService.GetById(1));
        }

        [Fact]
        public void GetByPhone_ShouldReturnObject()
        {
            //Arrange
            var mockCustomerRepository = new Mock<ICustomerRepository>();

            var fakeCustomer = new Customer()
            {
                Id = 0,
                Name = "Customer A",
                Address = "Address",
                PhoneNumber = 1234567899
            };

            mockCustomerRepository.Setup(b => b.GetByPhoneNumber(1234567899)).Returns(fakeCustomer);
            var customerService = new CustomerService(mockCustomerRepository.Object, null!);

            //Act
            var book = customerService.GetByPhone(1234567899);

            //Assert
            Assert.NotNull(book);
            Assert.Equal(fakeCustomer, book);
        }

        [Fact]
        public void GetByPhone_ShouldThrowInvalidPhoneNumber()
        {
            //Arrange
            var mockCustomerRepository = new Mock<ICustomerRepository>();
            var customerService = new CustomerService(mockCustomerRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentException>(() => customerService.GetByPhone(123456789));
        }

        [Fact]
        public void GetByPhone_ShouldThrowObjectDoesNotExist()
        {
            //Arrange
            var mockCustomerRepository = new Mock<ICustomerRepository>();
            var customerService = new CustomerService(mockCustomerRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentException>(() => customerService.GetByPhone(123456789));
        }

        [Fact]
        public void GetCustomersByName_ShouldReturnList()
        {
            //Arrange
            var mockCustomerRepository = new Mock<ICustomerRepository>();

            ICollection<Customer> fakeCustomers = new List<Customer>()
            {
                new Customer()
                {
                    Id = 0,
                    Name = "Customer A",
                    Address = "Address",
                    PhoneNumber = 1234567899
                },
                new Customer()
                {
                    Id = 1,
                    Name = "Customer B",
                    Address = "Address",
                    PhoneNumber = 1234567898
                }
            };

            mockCustomerRepository.Setup(b => b.GetCustomersByName("Customer")).Returns(fakeCustomers);
            var customerService = new CustomerService(mockCustomerRepository.Object, null!);

            //Act
            var customers = customerService.GetCustomersByName("Customer");

            //Assert
            Assert.NotNull(customers);
            Assert.Equal(2, customers.Count);
            Assert.Equal(fakeCustomers, customers);
        }

        [Fact]
        public void GetCustomersByName_ShouldThrowArgumentNullException()
        {
            //Arrange
            var customerRepository = new Mock<ICustomerRepository>();
            customerRepository.Setup(b => b.GetCustomersByName(It.IsAny<string>())).Returns(new List<Customer>());
            var customerService = new CustomerService(customerRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentNullException>(() => customerService.GetCustomersByName(null!));
        }

        [Fact]
        public void GetCustomersByName_ShouldThrowEmptyException()
        {
            //Arrange
            var customerRepository = new Mock<ICustomerRepository>();
            customerRepository.Setup(b => b.GetCustomersByName("Customer")).Returns(new List<Customer>());
            var customerService = new CustomerService(customerRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<Exception>(() => customerService.GetCustomersByName("Customer"));
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void AddCustomer_ShouldCallInsertAndSaveChanges()
        {
            // Arrange
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            customerRepositoryMock.Setup(repo => repo.GetByPhoneNumber(It.IsAny<long>())).Returns((Customer)null!);
            customerRepositoryMock.Setup(repo => repo.GetLatestId()).Returns(0);
            var customerService = new CustomerService(customerRepositoryMock.Object, null!);

            string name = "Sample name";
            string address = "Sample address";
            long phoneNumber = 1234567890;

            // Act
            int customerId = customerService.AddCustomer(name, address, phoneNumber);

            // Assert
            // Verify that the customer was added and ID was returned
            customerRepositoryMock.Verify(repo => repo.Insert(It.IsAny<Customer>()), Times.Once);
            customerRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
            Assert.Equal(1, customerId); // The expected ID should be the latest ID + 1
        }

        [Theory]
        [InlineData("", "Sample address", 1234567890)]
        public void AddCustomer_ShouldThrowArgumentNullException(string name, string address, long phoneNumber)
        {
            // Arrange
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            customerRepositoryMock.Setup(repo => repo.GetByPhoneNumber(phoneNumber)).Returns((Customer)null!);
            var customerService = new CustomerService(customerRepositoryMock.Object, null!);

            // Act and Assert
            var exception = Assert.Throws<ArgumentNullException>(() => customerService.AddCustomer(name, address, phoneNumber));
        }

        [Theory]
        [InlineData("Sample name", "Sample address", 123)]
        public void AddCustomer_ShouldThrowArgumentException(string name, string address, long phoneNumber)
        {
            // Arrange
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            customerRepositoryMock.Setup(repo => repo.GetByPhoneNumber(phoneNumber)).Returns((Customer)null!);
            var customerService = new CustomerService(customerRepositoryMock.Object, null!);

            // Act and Assert
            var exception = Assert.Throws<ArgumentException>(() => customerService.AddCustomer(name, address, phoneNumber));
        }

        [Theory]
        [InlineData("Sample name", "Sample address", 1234567899)]
        public void AddCustomer_ShouldThrowPhoneNumberExistsException(string name, string address, long phoneNumber)
        {
            // Arrange
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            customerRepositoryMock.Setup(repo => repo.GetByPhoneNumber(phoneNumber)).Returns(new Customer()); // Existing customer
            var customerService = new CustomerService(customerRepositoryMock.Object, null!);

            // Act and Assert
            var exception = Assert.Throws<ArgumentException>(() => customerService.AddCustomer(name, address, phoneNumber));
        }

        [Fact]
        public void UpdateCustomer_ShouldCallUpdateAndSaveChanges()
        {
            // Arrange
            int id = 1;
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            customerRepositoryMock.Setup(repo => repo.GetById(id)).Returns(new Customer()); // Simulate an existing customer
            var customerService = new CustomerService(customerRepositoryMock.Object, null!);

            string name = "Updated Name";
            string address = "Updated Address";
            long phoneNumber = 1234567890;

            // Act
            customerService.UpdateCustomer(id, name, address, phoneNumber);

            // Assert
            // Verify that the customer was updated
            customerRepositoryMock.Verify(repo => repo.Update(id, It.IsAny<Customer>()), Times.Once);
            customerRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
        }

        [Theory]
        [InlineData(-1, "Updated Name", "Updated Address", 1234567890, NotificationConstants.InvalidNumber)]
        [InlineData(1, "Updated Name", "Updated Address", 123, NotificationConstants.InvalidPhoneNumber)]
        public void UpdateCustomer_ShouldThrowArgumentException(int id, string name, string address, long phoneNumber, string expectedErrorMessage)
        {
            // Arrange
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            customerRepositoryMock.Setup(repo => repo.GetById(id)).Returns(id == 1 ? new Customer() : null!);
            var customerService = new CustomerService(customerRepositoryMock.Object, null!);

            // Act and Assert
            var exception = Assert.Throws<ArgumentException>(() => customerService.UpdateCustomer(id, name, address, phoneNumber));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Theory]
        [InlineData(1, "", "Updated Address", 1234567890)]
        [InlineData(1, "Updated Name", "", 1234567890)]
        public void UpdateCustomer_ShouldThrowArgumentNullException(int id, string name, string address, long phoneNumber)
        {
            // Arrange
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            customerRepositoryMock.Setup(repo => repo.GetById(id)).Returns(new Customer());
            var customerService = new CustomerService(customerRepositoryMock.Object, null!);

            // Act and Assert
            var exception = Assert.Throws<ArgumentNullException>(() => customerService.UpdateCustomer(id, name, address, phoneNumber));
        }

        [Fact]
        public void DeleteCustomer_ShouldCallDeleteAndSaveChanges()
        {
            // Arrange
            int id = 1;
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanRepositoryMock = new Mock<ILoanRepository>();

            var existingCustomer = new Customer();
            customerRepositoryMock.Setup(repo => repo.GetById(id)).Returns(existingCustomer);
            loanRepositoryMock.Setup(repo => repo.GetAll()).Returns(new List<Loan>());

            var customerService = new CustomerService(customerRepositoryMock.Object, loanRepositoryMock.Object);

            // Act
            customerService.DeleteCustomer(id);

            // Assert
            // Verify that the customer was deleted and SaveChanges was called
            customerRepositoryMock.Verify(repo => repo.Delete(existingCustomer), Times.Once);
            customerRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(1)]
        public void DeleteCustomer_ShouldThrowArgumentException(int id)
        {
            // Arrange
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanRepositoryMock = new Mock<ILoanRepository>();

            customerRepositoryMock.Setup(repo => repo.GetById(id)).Returns((Customer)null!);
            loanRepositoryMock.Setup(repo => repo.GetAll()).Returns(new List<Loan>());

            var customerService = new CustomerService(customerRepositoryMock.Object, loanRepositoryMock.Object);

            // Act and Assert
            var exception = Assert.Throws<ArgumentException>(() => customerService.DeleteCustomer(id));
        }

        [Theory]
        [InlineData(1)]
        public void DeleteCustomer_ShouldThrowNotReturnBooksException(int id)
        {
            // Arrange
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanRepositoryMock = new Mock<ILoanRepository>();

            var existingCustomer = new Customer() { Id = 1 };
            var loanWithCustomer = new List<Loan>()
            {
                new Loan() { CustomerId = 1 }
            };

            customerRepositoryMock.Setup(repo => repo.GetById(id)).Returns(existingCustomer);
            loanRepositoryMock.Setup(repo => repo.GetAll()).Returns(loanWithCustomer);

            var customerService = new CustomerService(customerRepositoryMock.Object, loanRepositoryMock.Object);

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => customerService.DeleteCustomer(id));
            Assert.Equal(NotificationConstants.CustomerHasNotReturned, exception.Message);
        }
    }
}
