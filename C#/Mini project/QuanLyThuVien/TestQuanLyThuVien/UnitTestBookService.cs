using Moq;
using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;
using QuanLyThuVien.Service.Implements;

namespace TestQuanLyThuVien
{
    public class UnitTestBookService
    {
        [Fact]
        public void GetAll_ShouldReturnList()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();

            ICollection<Book> fakeBooks = new List<Book>()
            {
                new Book()
                {
                    Id = 0,
                    Title = "Test",
                    Author = "Test",
                    PublicationYear = 1,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Test",
                },
                new Book()
                {
                    Id = 1,
                    Title = "Test1",
                    Author = "Test1",
                    PublicationYear = 1,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Test1",
                }
            };

            mockBookRepository.Setup(b => b.GetAll()).Returns(fakeBooks);
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            var books = bookService.GetAll();

            //Assert
            Assert.NotNull(books);
            Assert.Equal(2, books.Count);
            Assert.Equal(fakeBooks, books);
        }

        [Fact]
        public void GetAll_ShouldThrowEmptyException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetAll()).Returns(new List<Book>());
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<Exception>(() => bookService.GetAll());
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void GetById_ShouldReturnObject()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();

            var fakeBook = new Book()
            {
                Id = 0,
                Title = "Test",
                Author = "Test",
                PublicationYear = 1,
                Quantity = 1,
                IsAvailable = true,
                Genre = "Test",
            };

            mockBookRepository.Setup(b => b.GetById(0)).Returns(fakeBook);
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            var book = bookService.GetById(0);

            //Assert
            Assert.NotNull(book);
            Assert.Equal(fakeBook, book);
        }

        [Fact]
        public void GetById_ShouldThrowInvalidNumberException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentException>(() => bookService.GetById(-1));
        }

        [Fact]
        public void GetById_ShouldThrowIdNotExistException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentException>(() => bookService.GetById(1));
        }

        [Fact]
        public void GetAvailableBooks_ShouldReturnList()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();

            ICollection<Book> fakeBooks = new List<Book>()
            {
                new Book()
                {
                    Id = 0,
                    Title = "Test",
                    Author = "Test",
                    PublicationYear = 1,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Test",
                },
                new Book()
                {
                    Id = 1,
                    Title = "Test1",
                    Author = "Test1",
                    PublicationYear = 1,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Test1",
                }
            };

            mockBookRepository.Setup(b => b.GetAvailableBooks()).Returns(fakeBooks);
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            var books = bookService.GetAvailableBooks();

            //Assert
            Assert.NotNull(books);
            Assert.Equal(2, books.Count);
            Assert.Equal(fakeBooks, books);
        }

        [Fact]
        public void GetVailableBooks_ShouldThrowEmptyException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetAvailableBooks()).Returns(new List<Book>());
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<Exception>(() => bookService.GetAvailableBooks());
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void GetBooksByAuthor_ShouldReturnList()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();

            ICollection<Book> fakeBooks = new List<Book>()
            {
                new Book()
                {
                    Id = 0,
                    Title = "Test",
                    Author = "Author",
                    PublicationYear = 1,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Test",
                },
                new Book()
                {
                    Id = 1,
                    Title = "Test1",
                    Author = "Author",
                    PublicationYear = 1,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Test1",
                }
            };

            mockBookRepository.Setup(b => b.GetByAuthor("Author")).Returns(fakeBooks);
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            var books = bookService.GetBooksByAuthor("Author");

            //Assert
            Assert.NotNull(books);
            Assert.Equal(2, books.Count);
            Assert.Equal(fakeBooks, books);
        }

        [Fact]
        public void GetBooksByAuthor_ShoudThrowArgumentNullException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetByAuthor(null!));
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentNullException>(() => bookService.GetBooksByAuthor(null!));
        }

        [Fact]
        public void GetBooksByAuhor_ShouldThrowEmptyException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetByAuthor("Author")).Returns(new List<Book>());
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<Exception>(() => bookService.GetBooksByAuthor("Author"));
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void GetBooksByGenre_ShouldReturnList()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();

            ICollection<Book> fakeBooks = new List<Book>()
            {
                new Book()
                {
                    Id = 0,
                    Title = "Test",
                    Author = "Author",
                    PublicationYear = 1,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Genre",
                },
                new Book()
                {
                    Id = 1,
                    Title = "Test1",
                    Author = "Author",
                    PublicationYear = 1,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Genre",
                }
            };

            mockBookRepository.Setup(b => b.GetByGenre("Genre")).Returns(fakeBooks);
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            var books = bookService.GetBooksByGenre("Genre");

            //Assert
            Assert.NotNull(books);
            Assert.Equal(2, books.Count);
            Assert.Equal(fakeBooks, books);
        }

        [Fact]
        public void GetBooksByGenre_ShoudThrowArgumentNullException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetByGenre(null!));
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentNullException>(() => bookService.GetBooksByGenre(null!));
        }

        [Fact]
        public void GetBooksByGenre_ShouldThrowEmptyException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetByGenre("Genre")).Returns(new List<Book>());
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<Exception>(() => bookService.GetBooksByGenre("Genre"));
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void GetBooksByPublicationYear_ShouldReturnList()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();

            ICollection<Book> fakeBooks = new List<Book>()
            {
                new Book()
                {
                    Id = 0,
                    Title = "Test",
                    Author = "Author",
                    PublicationYear = 2000,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Genre",
                },
                new Book()
                {
                    Id = 1,
                    Title = "Test1",
                    Author = "Author",
                    PublicationYear = 2000,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Genre",
                }
            };

            mockBookRepository.Setup(b => b.GetByPublicationYear(2000)).Returns(fakeBooks);
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            var books = bookService.GetBooksByPublicationYear(2000);

            //Assert
            Assert.NotNull(books);
            Assert.Equal(2, books.Count);
            Assert.Equal(fakeBooks, books);
        }

        [Fact]
        public void GetBooksByPublicationYear_ShoudThrowInvalidNumberException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetByPublicationYear(-1));
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentException>(() => bookService.GetBooksByPublicationYear(-1));
        }

        [Fact]
        public void GetBooksByPublicationYear_ShoudThrowInvalidPublicationYearException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetByPublicationYear(DateTime.Now.Year + 1));
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentException>(() => bookService.GetBooksByPublicationYear(3000));
        }

        [Fact]
        public void GetBooksByPublicationYear_ShouldThrowEmptyException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetByPublicationYear(2000)).Returns(new List<Book>());
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<Exception>(() => bookService.GetBooksByPublicationYear(2000));
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void GetBooksByTitle_ShouldReturnList()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();

            ICollection<Book> fakeBooks = new List<Book>()
            {
                new Book()
                {
                    Id = 0,
                    Title = "Title",
                    Author = "Author",
                    PublicationYear = 1,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Genre",
                },
                new Book()
                {
                    Id = 1,
                    Title = "Title",
                    Author = "Author",
                    PublicationYear = 1,
                    Quantity = 1,
                    IsAvailable = true,
                    Genre = "Genre",
                }
            };

            mockBookRepository.Setup(b => b.GetByTitle("Title")).Returns(fakeBooks);
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            var books = bookService.GetBooksByTitle("Title");

            //Assert
            Assert.NotNull(books);
            Assert.Equal(2, books.Count);
            Assert.Equal(fakeBooks, books);
        }

        [Fact]
        public void GetBooksByTitle_ShoudThrowArgumentNullException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetByTitle(null!));
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<ArgumentNullException>(() => bookService.GetBooksByTitle(null!));
        }

        [Fact]
        public void GetBooksByTitle_ShouldThrowEmptyException()
        {
            //Arrange
            var mockBookRepository = new Mock<IBookRepository>();
            mockBookRepository.Setup(b => b.GetByTitle("Title")).Returns(new List<Book>());
            var bookService = new BookService(mockBookRepository.Object, null!);

            //Act
            //Assert
            var exception = Assert.Throws<Exception>(() => bookService.GetBooksByTitle("Title"));
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void UpdateBook_ShouldCallUpdateAndSaveChanges()
        {
            // Arrange
            int id = 1;
            string title = "Sample Title";
            string author = "Sample Author";
            int publicationYear = 2000;
            int quantity = 5;
            string genre = "Fiction";

            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object, null!);

            // Act
            bookService.UpdateBook(id, title, author, publicationYear, quantity, genre);

            // Assert
            // Verify that Update and SaveChanges were called on the repository with the expected arguments
            bookRepositoryMock.Verify(repo => repo.Update(id, It.IsAny<Book>()), Times.Once);
            bookRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
        }

        [Theory]
        [InlineData(0, "", "Sample Author", 2000, 5, "Fiction")]
        [InlineData(0, "Sample Title", "", 2000, 5, "Fiction")]
        public void UpdateBook_ShouldThrowArgumentNullException(int id, string title, string author, int publicationYear, int quantity, string genre)
        {
            // Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object, null!);

            // Act and Assert
            Assert.Throws<ArgumentNullException>(() => bookService.UpdateBook(id, title, author, publicationYear, quantity, genre));
        }

        [Theory]
        [InlineData(-1, "Sample Title", "Sample Author", 2000, 5, "Fiction")]
        [InlineData(0, "Sample Title", "Sample Author", -1, 5, "Fiction")]
        [InlineData(0, "Sample Title", "Sample Author", 2000, -1, "Fiction")]
        public void UpdateBook_ShouldThrowArgumentException(int id, string title, string author, int publicationYear, int quantity, string genre)
        {
            // Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object, null!);

            // Act and Assert
            Assert.Throws<ArgumentException>(() => bookService.UpdateBook(id, title, author, publicationYear, quantity, genre));
        }

        [Fact]
        public void UpdateBook_WithInvalidPublicationYear_ShouldThrowException()
        {
            // Arrange
            int id = 1;
            string title = "Sample Title";
            string author = "Sample Author";
            int publicationYear = DateTime.Now.Year + 1;
            int quantity = 5;
            string genre = "Fiction";

            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object, null!);

            // Act and Assert
            Assert.Throws<ArgumentException>(() => bookService.UpdateBook(id, title, author, publicationYear, quantity, genre));
        }

        [Fact]
        public void AddBook_ShouldCallInsertAndSaveChanges()
        {
            // Arrange
            string title = "Sample Title";
            string author = "Sample Author";
            int publicationYear = 2000;
            int quantity = 5;
            string genre = "Fiction";

            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object, null!);

            // Act
            bookService.AddBook(title, author, publicationYear, quantity, genre);

            // Assert
            // Verify that Update and SaveChanges were called on the repository with the expected arguments
            bookRepositoryMock.Verify(repo => repo.Insert(It.IsAny<Book>()), Times.Once);
            bookRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
        }

        [Theory]
        [InlineData("", "Sample Author", 2000, 5, "Fiction")]
        [InlineData("Sample Title", "", 2000, 5, "Fiction")]
        public void AddBook_ShouldThrowArgumentNullException(string title, string author, int publicationYear, int quantity, string genre)
        {
            // Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object, null!);

            // Act and Assert
            Assert.Throws<ArgumentNullException>(() => bookService.AddBook(title, author, publicationYear, quantity, genre));
        }

        [Theory]
        [InlineData("Sample Title", "Sample Author", -1, 5, "Fiction")]
        [InlineData("Sample Title", "Sample Author", 2000, -1, "Fiction")]
        public void AddBook_ShouldThrowArgumentException(string title, string author, int publicationYear, int quantity, string genre)
        {
            // Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object, null!);

            // Act and Assert
            Assert.Throws<ArgumentException>(() => bookService.AddBook(title, author, publicationYear, quantity, genre));
        }

        [Fact]
        public void AddBook_WithInvalidPublicationYear_ShouldThrowException()
        {
            // Arrange
            string title = "Sample Title";
            string author = "Sample Author";
            int publicationYear = DateTime.Now.Year + 1;
            int quantity = 5;
            string genre = "Fiction";

            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object, null!);

            // Act and Assert
            Assert.Throws<ArgumentException>(() => bookService.AddBook(title, author, publicationYear, quantity, genre));
        }

        [Fact]
        public void DeleteBook_ShouldCallDeleteAndSaveChanges()
        {
            // Arrange
            int id = 1;
            var bookRepositoryMock = new Mock<IBookRepository>();
            var detailLoanRepositoryMock = new Mock<IDetailLoanRepository>();
            var bookService = new BookService(bookRepositoryMock.Object, detailLoanRepositoryMock.Object);

            var book = new Book { Id = id };
            bookRepositoryMock.Setup(repo => repo.GetById(id)).Returns(book);
            detailLoanRepositoryMock.Setup(repo => repo.GetAll()).Returns(new List<DetailLoan>());

            // Act
            bookService.DeleteBook(id);

            // Assert
            // Verify that Delete and SaveChanges were called on the repository with the expected book
            bookRepositoryMock.Verify(repo => repo.Delete(book), Times.Once);
            bookRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(1)]
        public void DeleteBook_ShouldThrowArgumentException(int id)
        {
            // Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var detailLoanRepositoryMock = new Mock<IDetailLoanRepository>();
            var bookService = new BookService(bookRepositoryMock.Object, detailLoanRepositoryMock.Object);

            // Set up the book to be found and possibly borrowed
            var fakeBook = new Book { Id = 1 };

            var fakeDetailsLoan = new List<DetailLoan>()
            {
                new DetailLoan { BookId = 1 }
            };

            bookRepositoryMock.Setup(repo => repo.GetById(id)).Returns(fakeBook);
            detailLoanRepositoryMock.Setup(repo => repo.GetAll()).Returns(fakeDetailsLoan);

            // Act and Assert
            var exception = Assert.Throws<ArgumentException>(() => bookService.DeleteBook(id));
        }
    }
}