﻿using Moq;
using QuanLyThuVien.Common.Constants;
using QuanLyThuVien.Models;
using QuanLyThuVien.Repository.Contracts;
using QuanLyThuVien.Service.Implements;
using QuanLyThuVien.ViewModels;
using FluentAssertions;
using Castle.Core.Resource;
using static System.Reflection.Metadata.BlobBuilder;

namespace TestQuanLyThuVien
{
    public class UnitTestLoanService
    {
        [Fact]
        public void GetAll_ShouldReturnList()
        {
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var loans = new List<Loan>
            {
                new Loan { Id = 1, CustomerId = 1},
                new Loan { Id = 2, CustomerId = 2},
            };
            var customers = new List<Customer>
            {
                new Customer { Id = 1, Name = "Customer1" },
                new Customer { Id = 2, Name = "Customer2" },
            };
            var expected = new List<LoanVM>()
            {
                new LoanVM { Id = 1, CustomerName = "Customer1"},
                new LoanVM { Id = 2, CustomerName = "Customer2" }
            };

            loanUnitOfWorkMock.Setup(l => l.LoanRepository.GetAll()).Returns(loans);
            customerRepositoryMock.Setup(c => c.GetAll()).Returns(customers);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, customerRepositoryMock.Object);

            // Act
            var result = loanService.GetAll();

            // Assert
            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count);
            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void GetAll_ShouldThrowEmptyException()
        {
            // Arrange
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            loanUnitOfWorkMock.Setup(l => l.LoanRepository.GetAll()).Returns(new List<Loan>());
            customerRepositoryMock.Setup(c => c.GetAll()).Returns(new List<Customer>());

            var loanService = new LoanService(loanUnitOfWorkMock.Object, customerRepositoryMock.Object);

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => loanService.GetAll());
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void GetLoanById_ShoudReturnObject()
        {
            // Arrange
            int id = 1;
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            // Simulate an existing loan and customer
            var loan = new Loan
            {
                Id = id,
                CustomerId = 1,
                DateStart = DateTime.Now,
                DateEnd = DateTime.Now.AddMonths(1),
                IsReturn = false
            };
            var customer = new Customer { Id = 1, Name = "Customer1" };

            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetById(id)).Returns(loan);
            customerRepositoryMock.Setup(repo => repo.GetById(loan.CustomerId)).Returns(customer);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, customerRepositoryMock.Object);

            // Act
            var result = loanService.GetLoanById(id);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(id, result.Id);
            Assert.Equal(customer.Name, result.CustomerName);
            Assert.Equal(loan.DateStart, result.DateStart);
            Assert.Equal(loan.DateEnd, result.DateEnd);
            Assert.Equal(loan.IsReturn, result.IsReturn);
        }

        [Theory]
        [InlineData(-1, NotificationConstants.InvalidNumber)]
        [InlineData(1, NotificationConstants.IdNotExist)]
        public void GetLoanById_ShouldThrowArgumentException(int id, string expectedErrorMessage)
        {
            // Arrange
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetById(id)).Returns((Loan)null!);
            customerRepositoryMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((Customer)null!);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, customerRepositoryMock.Object);

            // Act and Assert
            var exception = Assert.Throws<ArgumentException>(() => loanService.GetLoanById(id));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public void GetDetailLoansByLoanId_ShouldReturnList()
        {
            // Arrange
            int loanId = 1;

            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var detailLoans = new List<DetailLoan>
            {
                new DetailLoan { Id = 1, BookId = 1 },
                new DetailLoan { Id = 2, BookId = 2 },
            };
            var books = new List<Book>
            {
                new Book { Id = 1, Title = "Book1" },
                new Book { Id = 2, Title = "Book2" },
            };
            var expected = new List<DetailLoanVM>
            {
                new DetailLoanVM { Id = 1, BookTitle = "Book1" },
                new DetailLoanVM { Id = 2, BookTitle = "Book2"}
            };

            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetByLoanId(loanId)).Returns(detailLoans);
            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetAll()).Returns(books);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            // Act
            var result = loanService.GetDetailLoansByLoanId(loanId);

            // Assert
            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count);
            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void GetDetailLoansByLoanId_ShouldThrowEmptyException()
        {
            // Arrange
            int loanId = 1;
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetByLoanId(loanId)).Returns(new List<DetailLoan>());
            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetAll()).Returns(new List<Book>());

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => loanService.GetDetailLoansByLoanId(loanId));
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void GetLoansByCustomerName_ShouldReturnList()
        {
            // Arrange
            string customerName = "Customer";
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var customers = new List<Customer>
            {
                new Customer { Id = 1, Name = "Customer1" },
                new Customer { Id = 2, Name = "Customer2" },
            };
            var loans = new List<Loan>
            {
                new Loan { Id = 1, CustomerId = 1 },
                new Loan { Id = 2, CustomerId = 2 },
            };
            var expected = new List<LoanVM>
            {
                new LoanVM { Id = 1, CustomerName = "Customer1" },
                new LoanVM { Id = 2, CustomerName = "Customer2" }
            };

            customerRepositoryMock.Setup(repo => repo.GetCustomersByName(customerName)).Returns(customers);
            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetAll()).Returns(loans);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, customerRepositoryMock.Object);

            // Act
            var result = loanService.GetLoansByCustomerName(customerName);

            // Assert
            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count);
            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void GetLoansByCustomerName_ShouldThrowEmptyException()
        {
            // Arrange
            string customerName = "Customer1";
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            customerRepositoryMock.Setup(repo => repo.GetCustomersByName(customerName)).Returns(new List<Customer>());
            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetAll()).Returns(new List<Loan>());

            var loanService = new LoanService(loanUnitOfWorkMock.Object, customerRepositoryMock.Object);

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => loanService.GetLoansByCustomerName(customerName));
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void GetLoansByBookTitle_ShouldReturnList()
        {
            // Arrange
            string bookTitle = "Book";
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var books = new List<Book>
            {
                new Book { Id = 1, Title = "Book1" },
                new Book { Id = 2, Title = "Book2" },
            };
            var detailLoans = new List<DetailLoan>
            {
                new DetailLoan { Id = 1, LoanId = 1, BookId = 1 },
                new DetailLoan { Id = 2, LoanId = 2, BookId = 1 },
            };
            var loans = new List<Loan>
            {
                new Loan { Id = 1, CustomerId = 1 },
                new Loan { Id = 2, CustomerId = 2 },
            };
            var customers = new List<Customer>
            {
                new Customer { Id = 1, Name = "Customer1" },
                new Customer { Id = 2, Name = "Customer2" },
            };
            var expected = new List<LoanVM>()
            {
                new LoanVM { Id = 1, CustomerName = "Customer1"},
                new LoanVM { Id = 2, CustomerName = "Customer2"}
            };

            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetByTitle(bookTitle)).Returns(books);
            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetAll()).Returns(detailLoans);
            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetAll()).Returns(loans);
            customerRepositoryMock.Setup(repo => repo.GetAll()).Returns(customers);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, customerRepositoryMock.Object);

            // Act
            var result = loanService.GetLoansByBookTitle(bookTitle);

            // Assert
            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count);
            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void GetLoansByBookTitle_ShouldThrowEmptyException()
        {
            // Arrange
            string bookTitle = "Book";
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetByTitle(bookTitle)).Returns(new List<Book>());
            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetAll()).Returns(new List<DetailLoan>());
            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetAll()).Returns(new List<Loan>());
            customerRepositoryMock.Setup(repo => repo.GetAll()).Returns(new List<Customer>());

            var loanService = new LoanService(loanUnitOfWorkMock.Object, customerRepositoryMock.Object);

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => loanService.GetLoansByBookTitle(bookTitle));
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void AddLoan_ShouldReturnLoanId()
        {
            // Arrange
            int customerId = 1;
            DateTime dateEnd = DateTime.Now.AddMonths(1);
            var loanRepositoryMock = new Mock<ILoanRepository>();
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            loanUnitOfWorkMock.Setup(loanUnitOfWork => loanUnitOfWork.LoanRepository.GetLatestId()).Returns(1);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            // Act
            var result = loanService.AddLoan(customerId, dateEnd);

            // Assert
            loanUnitOfWorkMock.Verify(repo => repo.LoanRepository.Insert(It.IsAny<Loan>()), Times.Once);
            Assert.Equal(2, result);
        }

        [Fact]
        public void AddLoan_ShouldThrowArgumentException()
        {
            // Arrange
            int customerId = -1;
            DateTime dateEnd = DateTime.Now.AddMonths(-1);
            var loanRepositoryMock = new Mock<ILoanRepository>();
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            loanUnitOfWorkMock.Setup(loanUnitOfWork => loanUnitOfWork.LoanRepository.GetLatestId()).Returns(1);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            // Act and Assert
            var exception1 = Assert.Throws<ArgumentException>(() => loanService.AddLoan(customerId, DateTime.Now));
            Assert.Equal(NotificationConstants.InvalidNumber, exception1.Message);

            var exception2 = Assert.Throws<ArgumentException>(() => loanService.AddLoan(1, dateEnd));
            Assert.Equal(NotificationConstants.DateEndMustBeLaterThanDateStart, exception2.Message);
        }

        [Fact]
        public void AddDetailLoan_ShouldCallInsert()
        {
            // Arrange
            int loanId = 1;
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var book = new Book
            {
                Id = 1,
                Quantity = 5,
                IsAvailable = true,
            };

            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetById(1)).Returns(book);
            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetLatestId()).Returns(0);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            var bookQuantity = new Dictionary<int, int>
            {
                { 1, 2 }
            };

            // Act
            loanService.AddDetailLoan(loanId, bookQuantity);

            // Assert
            loanUnitOfWorkMock.Verify(repo => repo.DetailLoanRepository.Insert(It.IsAny<DetailLoan>()), Times.Once);
            Assert.Equal(3, book.Quantity);
            Assert.True(book.IsAvailable);
        }

        [Fact]
        public void AddDetailLoan_ShouldThrowBookNotAvailabeException()
        {
            // Arrange
            int loanId = 1;
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var book = new Book
            {
                Id = 1,
                Quantity = 1,
                IsAvailable = true,
            };

            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetById(1)).Returns(book);
            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetLatestId()).Returns(0);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            var bookQuantity = new Dictionary<int, int>
            {
                { 1, 2 }
            };

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => loanService.AddDetailLoan(loanId, bookQuantity));
            Assert.Equal(NotificationConstants.BookNotAvailable, exception.Message);
        }

        [Fact]
        public void BorrowBook_ShouldCallSaveChanges()
        {
            // Arrange
            int customerId = 1;
            DateTime dateEnd = DateTime.Now.AddMonths(1);            
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();
            var book = new Book
            {
                Id = 1,
                Quantity = 5,
                IsAvailable = true,
            };
            var bookQuantity = new Dictionary<int, int> { { 1, 2 } };

            loanUnitOfWorkMock.Setup(loanUnitOfWork => loanUnitOfWork.LoanRepository.GetLatestId()).Returns(1);
            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetById(1)).Returns(book);
            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetLatestId()).Returns(0);
            
            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            // Act
            loanService.BorrowBook(customerId, dateEnd, bookQuantity);

            // Assert
            loanUnitOfWorkMock.Verify(uow => uow.SaveChanges(), Times.Once);
        }

        [Fact]
        public void GetDetailLoansByLoanIdToReturn_ShouldReturnList()
        {
            // Arrange
            int loanId = 1;
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var loan = new Loan
            {
                Id = loanId,
                IsReturn = false,
            };
            var detailLoans = new List<DetailLoan>
            {
                new DetailLoan { Id = 1, LoanId = loanId },
                new DetailLoan { Id = 2, LoanId = loanId },
            };

            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetById(loanId)).Returns(loan);
            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetByLoanId(loanId)).Returns(detailLoans);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            // Act
            var result = loanService.GetDetailLoansByLoanIdToReturn(loanId);

            // Assert
            Assert.True(loan.IsReturn);
            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count);
            loanUnitOfWorkMock.Verify(uow => uow.LoanRepository.Update(loanId, It.IsAny<Loan>()), Times.Once);
        }

        [Fact]
        public void GetDetailLoansByLoanIdToReturn_ShoudThrowIdNotExistException()
        {
            // Arrange
            int loanId = 1;
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetById(loanId)).Returns((Loan)null!);
            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetByLoanId(loanId)).Returns(new List<DetailLoan>());

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            // Act and Assert
            var exception = Assert.Throws<ArgumentException>(() => loanService.GetDetailLoansByLoanIdToReturn(loanId));
            Assert.Equal(NotificationConstants.IdNotExist, exception.Message);
        }

        [Fact]
        public void GetDetailLoansByLoanIdToReturn_ShoudThrowLoanHasBeenReturnedException()
        {
            // Arrange
            int loanId = 1;
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var loan = new Loan()
            {
                IsReturn = true
            };

            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetById(loanId)).Returns(loan);
            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetByLoanId(loanId)).Returns(new List<DetailLoan>());

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => loanService.GetDetailLoansByLoanIdToReturn(loanId));
            Assert.Equal(NotificationConstants.LoanHasBeenReturned, exception.Message);
        }

        [Fact]
        public void GetDetailLoansByLoanIdToReturn_ShoudThrowEmptyException()
        {
            // Arrange
            int loanId = 1;
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var loan = new Loan()
            {
                Id = 1,
                IsReturn = false
            };

            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetById(loanId)).Returns(loan);
            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetByLoanId(loanId)).Returns(new List<DetailLoan>());

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => loanService.GetDetailLoansByLoanIdToReturn(loanId));
            Assert.Equal(NotificationConstants.Empty, exception.Message);
        }

        [Fact]
        public void UpdateBookWhenReturn_ShouldCallUpdate()
        {
            // Arrange
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var book1 = new Book
            {
                Id = 1,
                Quantity = 3,
                IsAvailable = true,
            };
            var book2 = new Book
            {
                Id = 2,
                Quantity = 0,
                IsAvailable = false,
            };

            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetById(1)).Returns(book1);
            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetById(2)).Returns(book2);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            var detailLoans = new List<DetailLoan>
            {
                new DetailLoan { BookId = 1, Quantity = 2 },
                new DetailLoan { BookId = 2, Quantity = 1 },
            };

            // Act
            loanService.UpdateBookWhenReturn(detailLoans);

            // Assert
            loanUnitOfWorkMock.Verify(repo => repo.BookRepository.Update(1, It.Is<Book>(b => b.Quantity == 5 && b.IsAvailable == true)), Times.Once);
            loanUnitOfWorkMock.Verify(repo => repo.BookRepository.Update(2, It.Is<Book>(b => b.Quantity == 1 && b.IsAvailable == true)), Times.Once);
        }

        [Fact]
        public void ReturnBook_ShouldCallSaveChanges()
        {
            // Arrange
            int loanId = 1;
            var loanUnitOfWorkMock = new Mock<ILoanUnitOfWork>();

            var loan = new Loan
            {
                Id = 1,
                IsReturn = false,
            };
            var detailLoans = new List<DetailLoan>
            {
                new DetailLoan { Id = 1, LoanId = loanId, BookId = 1, Quantity = 2 },
                new DetailLoan { Id = 2, LoanId = loanId, BookId = 2, Quantity = 1 },
            };
            var book1 = new Book
            {
                Id = 1,
                Quantity = 3,
                IsAvailable = true,
            };
            var book2 = new Book
            {
                Id = 2,
                Quantity = 0,
                IsAvailable = false,
            };

            loanUnitOfWorkMock.Setup(repo => repo.LoanRepository.GetById(loanId)).Returns(loan);
            loanUnitOfWorkMock.Setup(repo => repo.DetailLoanRepository.GetByLoanId(loanId)).Returns(detailLoans);
            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetById(1)).Returns(book1);
            loanUnitOfWorkMock.Setup(repo => repo.BookRepository.GetById(2)).Returns(book2);

            var loanService = new LoanService(loanUnitOfWorkMock.Object, null!);

            // Act
            loanService.ReturnBook(loanId);

            // Assert
            loanUnitOfWorkMock.Verify(uow => uow.SaveChanges(), Times.Once);
        }
    }
}
