﻿using System.Runtime.CompilerServices;
using System.Text;

namespace DemoExtension
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string s = "abcdef";
            Console.WriteLine(s.ReverseString());

            List<int> numbers = new List<int> {1, 2, 3, 4, 5, 7};
            Console.WriteLine(numbers.GetAverage());

            string input = "Hello World";
            Console.WriteLine(input.RemoveDuplicate());
        }
    }

    public static class MyExtension
    {
        public static string ReverseString(this string s)
        {
            StringBuilder result = new StringBuilder();

            for (int i = s.Length - 1; i >= 0; i--)
            {
                result.Append(s[i]);
            }

            return result.ToString();
        }

        public static double GetAverage(this List<int> numbers)
        {
            double result = 0;

            for (int i = 0; i < numbers.Count; i++)
            {
                result += numbers[i];
            }

            return result / numbers.Count;
        }

        public static string RemoveDuplicate(this string s)
        {
            string result = "";

            HashSet<char> set = new HashSet<char>();

            for (int i = 0; i < s.Length; i++)
            {
                set.Add(s[i]);
            }

            foreach (var item in set)
            {
                result += item;
            }

            return result;
        }
    }
}