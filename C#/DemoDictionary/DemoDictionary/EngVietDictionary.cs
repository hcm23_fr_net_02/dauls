﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoDictionary
{
    internal class EngVietDictionary
    {
        private Dictionary<string, string> dictionary;

        public EngVietDictionary()
        {
            dictionary = new Dictionary<string, string>();
        }

        public void AddWord(string word, string meaning)
        {
            //Word already exists
            if (dictionary.ContainsKey(word))
            {
                throw new Exception($"The word '{word}' already exists in the dictionary.");
            }

            dictionary.Add(word, meaning);
        }

        public string FindWord(string word)
        {
            //Word does not exist
            if (!dictionary.ContainsKey(word))
            {
                throw new Exception($"The word '{word}' was not found in the dictionary.");
            }

            return dictionary[word];
        }

        public void UpdateWord(string word, string meaning)
        {
            dictionary[word] = meaning;
        }

        public bool RemoveWord(string word)
        {
            return dictionary.Remove(word);
        }

        public void DisplayAllWords()
        {
            foreach (KeyValuePair<string, string> item in dictionary)
            {
                Console.WriteLine("English word: {0}, Meaning: {1}", item.Key, item.Value);
            }
        }

        public void DisplayJustEng()
        {
            foreach (string item in dictionary.Keys)
            {
                Console.WriteLine("English word: {0}", item);
            }
        }

        public void DisplayJustViet()
        {
            foreach (string item in dictionary.Values)
            {
                Console.WriteLine("English word: {0}", item);
            }
        }
    }
}
