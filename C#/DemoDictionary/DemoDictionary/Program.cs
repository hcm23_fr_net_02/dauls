﻿namespace DemoDictionary
{
    internal class Program
    {
        static void Main(string[] args)
        {
            EngVietDictionary dictionary = new EngVietDictionary();
            dictionary.AddWord("Car", "Xe");
            dictionary.AddWord("Bike", "Xe dap");
            dictionary.AddWord("House", "Nha");

            Console.WriteLine("1. Add word");
            Console.WriteLine("2. Find and update word");
            Console.WriteLine("3. Remove word");
            Console.WriteLine("4. Display all words");
            Console.WriteLine("5. Display English words");
            Console.WriteLine("6. Display Vietnamese words");

            Console.WriteLine("Press 1-6: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.WriteLine("Input word information (English, Vietnamese): ");
                    string[] wordAdd = Console.ReadLine().Split();
                    dictionary.AddWord(wordAdd[0], wordAdd[1]);
                    break;
                case 2:
                    Console.WriteLine("Input word want to find: ");
                    string wordFind = Console.ReadLine();
                    Console.WriteLine(dictionary.FindWord(wordFind));
                    Console.WriteLine("Do you want to update the meaning of this word ? (Y/N)");
                    ConsoleKey choiceUpdate = Console.ReadKey().Key;
                    Console.WriteLine();
                    switch (choiceUpdate)
                    {
                        case ConsoleKey.Y:
                            Console.WriteLine($"Please input the new meaning of this word '{wordFind}': ");
                            string newMeaning = Console.ReadLine();
                            dictionary.UpdateWord(wordFind, newMeaning);
                            dictionary.DisplayAllWords();
                            break;
                        default:
                            break;
                    }
                    break;
                case 3:
                    Console.WriteLine("Input word want to delete: ");
                    string wordRemove = Console.ReadLine();
                    dictionary.RemoveWord(wordRemove);
                    break;
                case 4:
                    dictionary.DisplayAllWords();
                    break;
                case 5:
                    dictionary.DisplayJustEng();
                    break;
                case 6:
                    dictionary.DisplayJustViet();
                    break;
                default:
                    break;
            }
        }
    }
}