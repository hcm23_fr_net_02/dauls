﻿namespace DemoHttpClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            List<string> imageUrls = new List<string>
            {
                "https://images.pexels.com/photos/268533/pexels-photo-268533.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
                "https://images.pexels.com/photos/220429/pexels-photo-220429.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
                "https://images.pexels.com/photos/1478685/pexels-photo-1478685.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
            };

            string saveDirectory = "C:\\Images\\"; // Replace with your desired directory

            await DownloadImagesAsync(imageUrls, saveDirectory);
        }

        static async Task DownloadImagesAsync(List<string> imageUrls, string saveDirectory)
        {
            using (HttpClient client = new HttpClient())
            {
                List<Task> tasks = new List<Task>();

                for (int i = 0; i < imageUrls.Count; i++)
                {
                    tasks.Add(DownloadSingleImageAsync(client, imageUrls[i], saveDirectory));
                }

                await Task.WhenAll(tasks);
            }
        }

        static async Task DownloadSingleImageAsync(HttpClient httpClient, string imageUrl, string saveDirectory)
        {
            try
            {
                byte[] imageData = await httpClient.GetByteArrayAsync(imageUrl);

                string fileName = Path.GetFileName(new Uri(imageUrl).AbsolutePath);
                string filePath = Path.Combine(saveDirectory, fileName);

                using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await fileStream.WriteAsync(imageData, 0, imageData.Length);
                }

                Console.WriteLine($"Downloaded and saved: {fileName}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error downloading {imageUrl}: {ex.Message}");
            }
        }
    }
}