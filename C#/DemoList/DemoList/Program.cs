﻿namespace DemoList
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StudentManager studentManager = new StudentManager();
            studentManager.AddStudent(new Student("A", 20, 3.2));
            studentManager.AddStudent(new Student("B", 21, 3.5));
            studentManager.AddStudent(new Student("C", 22, 3.8));

            Console.WriteLine("1. Add Student");
            Console.WriteLine("2. Remove Student By Name");
            Console.WriteLine("3. Display Students");
            Console.WriteLine("4. Find Student By Name");

            Console.Write("Press 1-4: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.WriteLine("Input student information (Name, Age, GPA): ");
                    string[] studentInfo = Console.ReadLine().Split();
                    studentManager.AddStudent(new Student(studentInfo[0], int.Parse(studentInfo[1]), double.Parse(studentInfo[2])));
                    break;
                case 2:
                    Console.WriteLine("Input student name to remove: ");
                    string nameRemove = Console.ReadLine();
                    studentManager.RemoveStudent(nameRemove);
                    break;
                case 3:
                    studentManager.DisplayStudents();
                    break;
                case 4:
                    Console.WriteLine("Input student name to find: ");
                    string nameFind = Console.ReadLine();
                    Console.WriteLine(studentManager.FindStudentByName(nameFind));
                    break;
                default:
                    break;
            }
        }
    }
}