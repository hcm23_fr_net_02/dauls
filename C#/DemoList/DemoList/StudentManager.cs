﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DemoList
{
    public class StudentManager
    {
        public List<Student> Students { get; set; }

        public StudentManager()
        {
            Students = new List<Student>();
        }

        public void AddStudent(Student student)
        {
            Students.Add(student);
        }

        public void RemoveStudent(string name)
        {
            Students.Remove(Students.Where(x => x.Name == name).FirstOrDefault());
        }

        public void DisplayStudents()
        {
            foreach (var student in Students)
            {
                Console.WriteLine(student.ToString());
            }
        }

        public Student FindStudentByName(string name) 
        {
            return Students.Where(x => x.Name.ToLower().Equals(name.ToLower())).FirstOrDefault();
        }
    }
}
