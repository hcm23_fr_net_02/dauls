﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DempHttpClientTodoList
{
    public class TodoController
    {
        ITodoApiClient _apiClient;

        public TodoController(ITodoApiClient apiClient)
        {
            this._apiClient = apiClient;
        }

        public async Task CreateTodoItem(string name, int priorityChoice)
        {
            PriorityEnum priority = PriorityEnum.Low;

            switch (priorityChoice)
            {
                case 1:
                    priority = PriorityEnum.Low;
                    break;
                case 2:
                    priority = PriorityEnum.Medium;
                    break;
                case 3:
                    priority = PriorityEnum.High;
                    break;
                default:
                    break;
            }

            TodoItem todoItemCreate = new TodoItem
            {
                Name = name,
                CreatedDate = DateTime.Now,
                Priority = priority,
                IsCompleted = false,
            };

            bool resultCreate = await _apiClient.CreateTodoItem(todoItemCreate);

            if (resultCreate)
            {
                Console.WriteLine($"Task '{name}' successfully created.");
            }
        }

        public async Task GetTodoItemSortByDate()
        {
            List<TodoItem> todoItemsByDate = await _apiClient.GetTodoItemSortByDate();

            foreach (var item in todoItemsByDate)
            {
                Console.WriteLine($"{item.Id}, {item.Name}, {item.CreatedDate}, {item.Priority}, {item.IsCompleted}");
            }
        }

        public async Task GetTodoItemSortByPriority()
        {
            List<TodoItem> todoItemsByPriority = await _apiClient.GetTodoItemSortByPriority();

            foreach (var item in todoItemsByPriority)
            {
                Console.WriteLine($"{item.Id}, {item.Name}, {item.CreatedDate}, {item.Priority}, {item.IsCompleted}");
            }
        }

        public async Task MarkCompleteTodoItem(string id)
        {
            TodoItem todoItem = await _apiClient.GetTodoItemById(id);

            todoItem.IsCompleted = true;

            await _apiClient.UpdateTodoItem(todoItem);
        }

        public async Task UpdatePriorityTodoItem(string id, int priorityChoice)
        {
            TodoItem todoItem = await _apiClient.GetTodoItemById(id);

            PriorityEnum priority = PriorityEnum.Low;

            switch (priorityChoice)
            {
                case 1:
                    priority = PriorityEnum.Low;
                    break;
                case 2:
                    priority = PriorityEnum.Medium;
                    break;
                case 3:
                    priority = PriorityEnum.High;
                    break;
                default:
                    break;
            }

            todoItem.Priority = priority;

            await _apiClient.UpdateTodoItem(todoItem);
        }

        public async Task DeleteTodoItem(string id)
        {
            await _apiClient.DeleteTodoItem(id);
        }
    }
}
