﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DempHttpClientTodoList
{
    public class TodoItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public PriorityEnum Priority { get; set; }
        public bool IsCompleted { get; set; }
    }
}
