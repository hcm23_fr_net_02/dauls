﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DempHttpClientTodoList
{
    public interface ITodoApiClient
    {
        public Task<bool> CreateTodoItem(TodoItem item);
        public Task<List<TodoItem>> GetTodoItemSortByDate();
        public Task<List<TodoItem>> GetTodoItemSortByPriority();
        public Task<TodoItem> GetTodoItemById(string id);
        public Task UpdateTodoItem(TodoItem item);
        public Task DeleteTodoItem(string id);
    }
}
