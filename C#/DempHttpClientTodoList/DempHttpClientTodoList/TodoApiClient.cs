﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Reflection.Metadata;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DempHttpClientTodoList
{
    public class TodoApiClient : ITodoApiClient
    {
        private const string ApiBaseUrl = "https://651e68bd44a3a8aa47684a14.mockapi.io/";

        private readonly HttpClient httpClient;

        public TodoApiClient()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(ApiBaseUrl);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<bool> CreateTodoItem(TodoItem todoItem)
        {
            HttpResponseMessage response = await httpClient.PostAsJsonAsync("api/TodoList", todoItem);
            response.EnsureSuccessStatusCode();

            return true;
        }

        public async Task<List<TodoItem>> GetTodoItemSortByDate()
        {
            HttpResponseMessage response = await httpClient.GetAsync("api/TodoList");
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStreamAsync();
            List<TodoItem> todoItems = await JsonSerializer.DeserializeAsync<List<TodoItem>>(content, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
                Converters = { new JsonStringEnumConverter() },
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
            });

            return todoItems.OrderBy(x => x.CreatedDate).ToList();
        }

        public async Task<List<TodoItem>> GetTodoItemSortByPriority()
        {
            HttpResponseMessage response = await httpClient.GetAsync("api/TodoList");
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStreamAsync();
            List<TodoItem> todoItems = await JsonSerializer.DeserializeAsync<List<TodoItem>>(content, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
                Converters = { new JsonStringEnumConverter() },
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
            });


            return todoItems.OrderByDescending(x => x.Priority).ToList();
        }

        public async Task<TodoItem> GetTodoItemById(string id)
        {
            HttpResponseMessage response = await httpClient.GetAsync($"api/TodoList/{id}");
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStreamAsync();

            TodoItem todoItem = await JsonSerializer.DeserializeAsync<TodoItem>(content, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
                Converters = { new JsonStringEnumConverter() },
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
            });

            return todoItem;
        }

        public async Task UpdateTodoItem(TodoItem todoItem)
        {
            HttpResponseMessage response = await httpClient.PutAsJsonAsync($"api/TodoList/{todoItem.Id}", todoItem);
            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteTodoItem(string id)
        {
            HttpResponseMessage response = await httpClient.DeleteAsync($"api/TodoList/{id}");
            response.EnsureSuccessStatusCode();
        }
    }
}
