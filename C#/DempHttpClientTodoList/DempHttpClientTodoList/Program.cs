﻿namespace DempHttpClientTodoList
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            TodoApiClient apiClient = new TodoApiClient();
            TodoController todoController = new TodoController(apiClient);

        BEGIN:
            Console.Clear();
            Console.WriteLine("1. Display to do list by created date");
            Console.WriteLine("2. Display to do list by priority");
            Console.WriteLine("3. Add a new task");
            Console.WriteLine("4. Remove a task");
            Console.WriteLine("5. Mark complete");
            Console.WriteLine("6. Update priority");
            Console.WriteLine("7. Exit");

            Console.WriteLine("Press 1-7: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    await todoController.GetTodoItemSortByDate();
                    Console.ReadKey();
                    goto BEGIN;
                case 2:
                    await todoController.GetTodoItemSortByPriority();
                    Console.ReadKey();
                    goto BEGIN;
                case 3:
                    Console.Write("Input to-do name: ");
                    string name = Console.ReadLine();
                    Console.Write("Choose the priority (1-3): ");
                    int priorityCreate = int.Parse(Console.ReadLine());

                    await todoController.CreateTodoItem(name, priorityCreate);
                    Console.ReadKey();
                    goto BEGIN;
                case 4:
                    await todoController.GetTodoItemSortByDate();
                    Console.Write("Input the id of to-do that you want to remove: ");
                    string idRemove = Console.ReadLine();

                    await todoController.DeleteTodoItem(idRemove);
                    await todoController.GetTodoItemSortByDate();
                    Console.ReadKey();
                    goto BEGIN;
                case 5:
                    await todoController.GetTodoItemSortByDate();
                    Console.Write("Input the id of to-do that you want to mark complete: ");
                    string idMarkComplete = Console.ReadLine();

                    await todoController.MarkCompleteTodoItem(idMarkComplete);
                    await todoController.GetTodoItemSortByDate();
                    Console.ReadKey();
                    goto BEGIN;
                case 6:
                    await todoController.GetTodoItemSortByDate();
                    Console.Write("Input the id of to-do that you want to update priority: ");
                    string idUpdatePriority = Console.ReadLine();
                    Console.Write("Choose the priority (1-3): ");
                    int priorityUpdate = int.Parse(Console.ReadLine());

                    await todoController.UpdatePriorityTodoItem(idUpdatePriority, priorityUpdate);
                    await todoController.GetTodoItemSortByDate();
                    Console.ReadKey();
                    goto BEGIN;
                default:
                    break;
            }
        }
    }
}