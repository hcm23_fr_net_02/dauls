﻿namespace DemoDelegate
{
    internal class Program
    {
        delegate int MathOperation(int a, int b);
        delegate int Custom(int number);
        delegate T NewMathOperation<T> (T a, T b);

        struct Point
        {
            public int X { get; set; }
            public int Y { get; set; }

            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }
        }
        
        static int PerformOperation(int a, int b, MathOperation operation)
        {
            return operation(a, b);
        }

        static T NewPerformOperation<T>(T a, T b, NewMathOperation<T> operation)
        {
            return operation(a, b);
        }

        static void PrintResult(int number, Custom operation)
        {
            Console.WriteLine(operation(number));
        }

        static void PrintNumbers(int[] arr, Action<int> del)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                del(arr[i]);
            }
        }

        static void ProcessNumber(int[] arr, Func<int,int> del)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(del(arr[i]));
            }
        }

        static List<string> FilterWords(List<string> words, Predicate<string> del)
        {
            List<string> result = new List<string>();

            for (int i = 0; i < words.Count; i++)
            {
                if (del(words[i]))
                {
                    result.Add(words[i]);
                }
            }

            return result;
        }

        static void Main(string[] args)
        {
            //Exercise 1
            MathOperation Addition = (int a, int b) => a + b;
            MathOperation Subtraction = (int a, int b) => a - b;
            MathOperation Multiplication = (int a, int b) => a * b;
            MathOperation Division = (int a, int b) => a / b;

            Console.WriteLine(PerformOperation(10, 5, Addition));
            Console.WriteLine(PerformOperation(10, 5, Subtraction));
            Console.WriteLine(PerformOperation(10, 5, Multiplication));
            Console.WriteLine(PerformOperation(10, 5, Division));

            Console.WriteLine();

            //Exercise 2
            Custom Double = (int a) => a * 2;
            Custom Triple = (int a) => a * 3;
            Custom Square = (int a) => a * a;
            Custom Cube = (int a) => a * a * a;

            PrintResult(5, Double);
            PrintResult(5, Triple);
            PrintResult(5, Square);
            PrintResult(5, Cube);

            Console.WriteLine();

            //Exercise 3
            Action<int> DetermineNumberDel = delegate(int a) 
            {
                if (a > 0)
                {
                    Console.WriteLine("POSITIVE");
                }
                else if (a < 0)
                {
                    Console.WriteLine("NEGATIVE");
                }
                else
                {
                    Console.WriteLine("ZERO");
                }
            };

            int[] numbers1 = { -1, 0, 2, -3 };
            PrintNumbers(numbers1, DetermineNumberDel);

            Console.WriteLine();

            //Exercise 4
            Func<int, int> ProcessNumberDel = delegate (int a)
            {
                if (a > 0)
                {
                    return a * a;
                }
                else if (a < 0)
                {
                    return Math.Abs(a);
                }
                else
                {
                    return a;
                }
            };

            int[] numbers2 = { -1, 0, 2, -3, 4, 0, -5 };
            ProcessNumber(numbers2, ProcessNumberDel);

            Console.WriteLine();

            //Exercise 5
            Predicate<string> FilterLenWordsDel = delegate (string s)
            {
                if (s.Length >= 6)
                {
                    return true;
                }

                return false;
            };

            List<string> words = new List<string> { "apple", "banana", "orange", "kiwi", "mango" };

            List<string> result =  FilterWords(words, FilterLenWordsDel);

            foreach (string item in result)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            //Exercise 6
            int result1 = NewPerformOperation(5, 3, (a, b) => a - b);
            Console.WriteLine("5 + 3 = " + result1);

            double result2 = NewPerformOperation(7.5, 2.5, (a, b) => a * b);
            Console.WriteLine("7.5 * 2.5 = " + result2);

            float result3 = NewPerformOperation(4.0f, 1.5f, (a, b) => a / b);
            Console.WriteLine("4.0 / 1.5 = " + result3);

            string result4 = NewPerformOperation("Hello, ", "world!", (a, b) => a + b);
            Console.WriteLine(result4);

            Point point1 = new Point(1, 2);
            Point point2 = new Point(3, 4);
            Point result5 = NewPerformOperation(point1, point2, (a, b) => new Point(a.X + b.X, a.Y + b.Y));
            Console.WriteLine($"Point1 + Point2 = ({result5.X}, {result5.Y})");
        }
    }
}