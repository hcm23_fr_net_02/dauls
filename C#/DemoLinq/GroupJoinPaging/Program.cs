﻿using Newtonsoft.Json;

Console.WriteLine("1. Exercise 1");
Console.WriteLine("2. Exercise 2");
int masterChoice = int.Parse(Console.ReadLine());

switch (masterChoice)
{
    case 1:
        Exercise1();
        break;
    case 2:
        Exercise2();
        break;
    default:
        break;
}

static void Exercise1()
{
    Console.WriteLine("Exercise 1:");
    var numbers = Enumerable.Range(0, 50).ToList();
    int pageIndex = 0;
    int elementsPerPage = 10;

    do
    {
        PrintPage(numbers, ref pageIndex, elementsPerPage);
    } while (pageIndex != -1);
}

static void Exercise2()
{
    Console.WriteLine("Exercise 2:");
    var json = File.ReadAllText("MOCK_DATA.json");
    List<Product> products = JsonConvert.DeserializeObject<List<Product>>(json);
    int pageIndex = 0;
    int elementsPerPage = 10;

    do
    {
        PrintPage(products, ref pageIndex, elementsPerPage);
    } while (pageIndex != -1);
}

static void PrintPage<T>(IEnumerable<T> numbers, ref int pageIndex, int elementsPerPage)
{
    Console.WriteLine("Page {0}", pageIndex);
    var paging = numbers.Skip(elementsPerPage * pageIndex).Take(elementsPerPage);
    foreach (var item in paging)
    {
        Console.WriteLine(item);
    }

    Console.WriteLine("--------------------Menu--------------------");
    int choice;
    double pages = Math.Ceiling(numbers.Count() * 1.0 / elementsPerPage);

    if (pageIndex > 0)
    {
        Console.WriteLine("1. Previous");
    }

    if (pageIndex < pages - 1)
    {
        Console.WriteLine("2. Next");
    }

    Console.WriteLine("3. End");
    Console.Write("Input your choice: ");
    choice = int.Parse(Console.ReadLine());

    switch (choice)
    {
        case 1:
            if (pageIndex > 0)
            {
                pageIndex--;
            }
            break;
        case 2:
            if (pageIndex < pages - 1)
            {
                pageIndex++;
            }
            break;
        case 3:
            pageIndex = -1;
            break;
        default:
            break;
    }
}

public class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Category { get; set; }
    public double Price { get; set; }

    public override string ToString()
    {
        return Id + " " + Name + " " + Category + " " + Price;
    }
}