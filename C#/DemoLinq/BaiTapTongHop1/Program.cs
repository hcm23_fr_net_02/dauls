﻿using System.Text;

Console.InputEncoding = Encoding.UTF8;
Console.OutputEncoding = Encoding.UTF8;

var products = new List<Product>
{
    new Product { Name = "Áo sơ mi", Price = 300000, Category = "Áo" },
    new Product { Name = "Quần jeans", Price = 500000, Category = "Quần" },
    new Product { Name = "Giày thể thao", Price = 800000, Category = "Giày" },
    new Product { Name = "Ví da", Price = 200000, Category = "Phụ kiện" },
    new Product { Name = "Mũ nón", Price = 100000, Category = "Phụ kiện" },
    new Product { Name = "Đầm dạ hội", Price = 1500000, Category = "Đầm" },
};

// Tìm danh sách các sản phẩm có giá lớn hơn 500,000 đồng:


// Lấy ra tên của các sản phẩm có giá nhỏ hơn 300,000 đồng và sắp xếp theo thứ tự giảm dần của giá:


// Tạo một dictionary với danh mục sản phẩm là key và danh sách các sản phẩm trong cùng danh mục là value:
Console.WriteLine("\nCau 3:");
var groupByCategory = products.GroupBy(x => x.Category).ToDictionary(x => x.Key, x => x);
foreach (var group in groupByCategory)
{
    Console.WriteLine("----------------------");
    Console.WriteLine("Category: " + group.Key);
    foreach (var item in group.Value)
    {
        Console.WriteLine(item);
    }
}

// Tính tổng giá tiền của các sản phẩm trong danh mục "Áo":
Console.WriteLine("\nCau 4:");
var totalByAo = groupByCategory.Where(x => x.Key == "Áo").Select(x => new
{
    Category = x.Key,
    Total = x.Value.Select(y => y.Price).Sum()
}).Single();

Console.WriteLine("Danh muc: " + totalByAo.Category + ", Total: " + totalByAo.Total);

// Tìm sản phẩm có giá cao nhất:


// Tạo danh sách các sản phẩm với tên đã viết hoa:
Console.WriteLine("\nCau 6:");
var upperCase = products.Select(x => new Product
{
    Name = x.Name.ToUpper(),
    Price = x.Price,
    Category = x.Category
});
foreach (var item in upperCase)
{
    Console.WriteLine(item);
}
class Product
{
    public string Name { get; set; }
    public int Price { get; set; }
    public string Category { get; set; }
    public override string ToString()
    {
        return Name + " " + Price + " " + Category;
    }
}
