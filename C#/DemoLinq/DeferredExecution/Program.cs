﻿using DeferredExecution;
using Newtonsoft.Json;

var json = File.ReadAllText("MOCK_DATA.json");
List<Order> orders = JsonConvert.DeserializeObject<List<Order>>(json);

Console.WriteLine("Tong so tien cua tat ca cac don hang");
var sum = orders.Sum(x => x.TotalAmount);
Console.WriteLine(sum);

Console.WriteLine("\nLay don hang co gia tri total amount cao nhat");
var maxTotal = orders.Max(x => x.TotalAmount);
var orderWithMaxTotal = orders.First(x => x.TotalAmount == maxTotal);
Console.WriteLine(orderWithMaxTotal);

Console.WriteLine("\nLay don hang co gia tri total amount thap nhat");
var minTotal = orders.Min(x => x.TotalAmount);
var orderWithMinTotal = orders.First(x => x.TotalAmount == minTotal);
Console.WriteLine(orderWithMinTotal);

Console.WriteLine("\nGia tri trung binh cua cac don hang");
var avg = orders.Average(x => x.TotalAmount);
Console.WriteLine(avg);

Console.WriteLine("\nTong so don hang cua mot khach hang");
var ordersByCustomer = orders.GroupBy(x => x.CustomerName);
foreach (var item in ordersByCustomer)
{
    Console.WriteLine(item.Key + " " + item.Count());
}

Console.WriteLine("\nTong so tien cua tat ca don hang cua mot khach hang");
var sumTotalByCustomer = orders.GroupBy(x => x.CustomerName, x => x.TotalAmount);
foreach (var item in sumTotalByCustomer)
{
    Console.WriteLine(item.Key + " " + item.Sum());
}

Console.WriteLine("\nLay ra don hang co gia tri cao nhat cua mot khach hang");
var maxTotalByCustomer = orders.GroupBy(x => x.CustomerName, x => x.TotalAmount);
foreach(var item in maxTotalByCustomer)
{
    Console.WriteLine(item.Key + " " + item.Max());
}

Console.WriteLine("\nLay ra don hang co gia tri thap nhat cua mot khach hang");
var minxTotalByCustomer = orders.GroupBy(x => x.CustomerName, x => x.TotalAmount);
foreach (var item in minxTotalByCustomer)
{
    Console.WriteLine(item.Key + " " + item.Min());
}

Console.WriteLine("\nTinh gia tri trung binh cac don dat hang cua mot khach hang cu the");
var avgTotalByCustomer = orders.GroupBy(x => x.CustomerName, x => x.TotalAmount);
foreach (var item in avgTotalByCustomer)
{
    Console.WriteLine(item.Key + " " + item.Average());
}

var numbers = new List<int> { 1, 2, 3 };
var first = numbers.First();
Console.WriteLine(first);