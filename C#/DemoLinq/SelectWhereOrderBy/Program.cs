﻿// đọc file mock-data.json trong project để lấy dữ liệu
using Newtonsoft.Json;
using SelectWhereOrderBy;

var json = File.ReadAllText("mock-data.json");
List<Employee> employees = JsonConvert.DeserializeObject<List<Employee>>(json);

// sử dụng LinQ để lọc các dữ liệu như yêu cầu bên dưới

//Lấy ra danh sách các FirstName của tất cả các nhân viên.
Console.WriteLine("---------------------------------------------");
Console.WriteLine("Danh sach FirstName: ");
var firstNames = employees.Select(x => x.FirstName);
foreach (var item in firstNames)
{
    Console.WriteLine(item);
}

//Lấy ra danh sách các nhân viên có Salary lớn hơn 50000$.
Console.WriteLine("---------------------------------------------");
Console.WriteLine("Danh sach nhan vien co salary > 50000");
var empWithSalary = employees.Where(x => x.Salary > 50000);
foreach (var item in empWithSalary)
{
    Console.WriteLine(item.FirstName + " " + item.Salary);
}

//Lấy ra danh sách các nhân viên có Gender là "Male" và sắp xếp tăng dần theo FirstName.
Console.WriteLine("---------------------------------------------");
Console.WriteLine("Danh sach nhan vien co gender la Male va sap xep tang dan theo firstname");
var sortedEmpMale = employees.Where(x => x.Gender == "Male").OrderBy(x => x.FirstName);
foreach (var item in sortedEmpMale)
{
    Console.WriteLine(item.FirstName + " " + item.Gender);
}

//Lấy ra danh sách các nhân viên có Country là "Indonesia" và JobTitle chứa "Manager".
Console.WriteLine("---------------------------------------------");
Console.WriteLine("Danh sach nhan vien co Country la Indonesia va JobTitle chua Manager");
var empIndoManager = employees.Where(x => !string.IsNullOrEmpty(x.Country) && !string.IsNullOrEmpty(x.JobTitle)).Where(x => x.Country.Equals("Indonesia") && x.JobTitle.Contains("Manager"));
foreach (var item in empIndoManager)
{
    Console.WriteLine(item.FirstName + " " + item.Country + " " + item.JobTitle);
}

//Lấy ra danh sách các nhân viên có Email và sắp xếp giảm dần theo LastName.
Console.WriteLine("---------------------------------------------");
Console.WriteLine("Danh sach nhan vien co Email va sap xep giam dan theo LastName");
var empEmailLastName = employees.Where(x => !string.IsNullOrEmpty(x.Email)).OrderByDescending(x => x.LastName);
foreach (var item in empEmailLastName)
{
    Console.WriteLine(item.LastName + " " + item.Email);
}

//Lấy ra danh sách các nhân viên có StartDate trước ngày "2022-01-01" và Salary lớn hơn 60000$.
Console.WriteLine("---------------------------------------------");
Console.WriteLine("Danh sach nhan vien co StartDate truoc 2022-01-01 va Salary > 60000$");
var empStartDateSalary = employees.Where(x => x.StartDate?.CompareTo(new DateTime(2022, 01, 01)) < 0 && x.Salary > 60000);
foreach (var item in empStartDateSalary)
{
    Console.WriteLine(item.FirstName + " " + item.StartDate + " " + item.Salary);
}

//Lấy ra danh sách các nhân viên có PostalCode là null hoặc rỗng và sắp xếp tăng dần theo LastName.
Console.WriteLine("---------------------------------------------");
Console.WriteLine("Danh sach nhan vien co PostalCode null va sap xep tang dan theo LastName");
var empPostalCodeLastName = employees.Where(x => string.IsNullOrEmpty(x.PostalCode)).OrderBy(x => x.LastName);
foreach (var item in empPostalCodeLastName)
{
    Console.WriteLine(item);
}

//Lấy ra danh sách các nhân viên có FirstName và LastName được viết hoa và sắp xếp giảm dần theo Id.
Console.WriteLine("---------------------------------------------");
Console.WriteLine("Danh sach nhan vien co FirstName va LastName duoc viet hoa va sap xep giam dan theo Id");
var empFirstLastUper = employees.Where(x => char.IsUpper(x.FirstName[0]) && char.IsUpper(x.LastName[0])).OrderByDescending(x => x.Id);
foreach (var item in empFirstLastUper)
{
    Console.WriteLine(item.FirstName + " " + item.LastName + " " + item.Id);
}

//Lấy ra danh sách các nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ và sắp xếp tăng dần theo Salary.
Console.WriteLine("---------------------------------------------");
Console.WriteLine("Danh sach nhan vien co Salary nam trong 50000-70000 va sap xep tang dan theo Salary");
var sortedEmpSalary = employees.Where(x => x.Salary >= 50000 && x.Salary <= 70000).OrderBy(x => x.Salary);
foreach (var item in sortedEmpSalary)
{
    Console.WriteLine(item.FirstName + " " + item.Salary);
}

//Lấy ra danh sách các nhân viên có FirstName chứa chữ "a" hoặc "A" và sắp xếp giảm dần theo LastName.
Console.WriteLine("---------------------------------------------");
Console.WriteLine("Danh sach nhan vien co FirstName chua chu 'a' hoac 'A' va sap xep giam dan theo LastName");
var sortedFirstName = employees.Where(x => x.FirstName.Contains('a') || x.FirstName.Contains('A')).OrderBy(x => x.LastName);
foreach (var item in sortedFirstName)
{
    Console.WriteLine(item.FirstName + " " + item.LastName);
}

