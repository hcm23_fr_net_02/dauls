﻿//Projection data
Console.WriteLine("Projection data: ");

//Exercise 1
Console.WriteLine("Exercise 1:");
var fruits = new List<string>
            {
                "apple",
                "banana",
                "orange",
                "pineapple",
                "mango"
            };

var fruitsUpperCase = fruits.Select(x => x.ToUpper());
foreach (var item in fruitsUpperCase)
{
    Console.WriteLine(item);
}

Console.WriteLine();

var fruitsWithLen = fruits.Select(x => new
{
    Name = x,
    Len = x.Length
});
foreach (var item in fruitsWithLen)
{
    Console.WriteLine(item.Name + ": " + item.Len);
}

Console.WriteLine();

var fruitsChar = fruits.Select(x => x[0]);
foreach (var item in fruitsChar)
{
    Console.WriteLine(item);
}

Console.WriteLine();

var fruitsReverse = fruits.Select(x => string.Join("", x.ToCharArray().Reverse()));
foreach (var item in fruitsReverse)
{
    Console.WriteLine(item);
}

Console.WriteLine();

//Exercise 2
Console.WriteLine("Exercise 2:");
List<DateTime> dates = new List<DateTime>()
            {
                new DateTime(2021, 1, 1),
                new DateTime(2022, 2, 1),
                new DateTime(2023, 3, 1),
                new DateTime(2024, 4, 1),
                new DateTime(2025, 5, 1)
            };

var months = dates.Select(x => x.Month);
foreach (var item in months)
{
    Console.WriteLine(item);
}

Console.WriteLine();

var formatDates = dates.Select(x => x.ToString("dd/MM/yyyy"));
foreach (var item in formatDates)
{
    Console.WriteLine(item);
}

Console.WriteLine();

//Exercise 3
Console.WriteLine("Exercise 3:");

var employees = new List<Employee>
{
    new Employee { Name = "Alice", Department = "Sales", Salary = 50000 },
    new Employee { Name = "Bob", Department = "Marketing", Salary = 60000 },
    new Employee { Name = "Charlie", Department = "Sales", Salary = 55000 },
    new Employee { Name = "David", Department = "IT", Salary = 70000 }
};

var empNames = employees.Select(x => x.Name);
foreach (var item in empNames)
{
    Console.WriteLine(item);
}

Console.WriteLine();

var empNamesWithDep = employees.Select(x => x.Name + " - " + x.Department);
foreach (var item in empNamesWithDep)
{
    Console.WriteLine(item);
}

Console.WriteLine();

var empNamesReverse = employees.Select(x => string.Join("", x.Name.ToCharArray().Reverse()));
foreach (var item in empNamesReverse)
{
    Console.WriteLine(item);
}

Console.WriteLine();

//Filter data
Console.WriteLine("Filter data:");

//Exercise 1
Console.WriteLine("Exercise 1: ");
var fruitsWithLen6 = fruits.Where(x => x.Length == 6);
foreach (var item in fruitsWithLen6)
{
    Console.WriteLine(item);
}

Console.WriteLine();

var fruisStartWithA = fruits.Where(x => x[0] == 'a');
foreach (var item in fruisStartWithA)
{
    Console.WriteLine(item);
}

Console.WriteLine();

var fruistContainsA = fruits.Where(x => x.Contains('a'));
foreach (var item in fruistContainsA)
{
    Console.WriteLine(item);
}

Console.WriteLine();

//Sort data
Console.WriteLine("Sort data:");

//Exercise 1
Console.WriteLine("Exercise 1:");

var names = new List<string> {
    "John",
    "Alice",
    "Bob",
    "David",
    "Emily"
};

var sortedNames = names.OrderBy(x => x);
foreach (var item in sortedNames)
{
    Console.WriteLine(item);
}

Console.WriteLine();

//Exercise 2
Console.WriteLine("Exercise 2:");

var sortedDates = dates.OrderBy(x => x);
foreach (var item in sortedDates)
{
    Console.WriteLine(item);
}

Console.WriteLine();

//Exercise 3
Console.WriteLine("Exercise 3:");
List<Person> people = new List<Person>
{
    new Person {Name = "Alice", Age = 25},
    new Person {Name = "Bob", Age = 30},
    new Person {Name = "Charlie", Age = 22},
    new Person {Name = "David", Age = 28},
    new Person {Name = "Emily", Age = 27}
};

Console.WriteLine("Sorted acs");
var sortedPeopleAsc = people.OrderBy(x => x.Age);
foreach (var item in sortedPeopleAsc)
{
    Console.WriteLine(item.Name + " " + item.Age);
}

Console.WriteLine();

Console.WriteLine("Sorted desc");
var sortedPeopleDesc = people.OrderByDescending(x => x.Age);
foreach (var item in sortedPeopleDesc)
{
    Console.WriteLine(item.Name + " " + item.Age);
}

Console.WriteLine();

// Bai tap tong hop 1
Console.WriteLine("Bai tap tong hop 1");
List<int> numbers = new List<int> { 3, 8, 1, 5, 2, 10, 7 };

Console.WriteLine("Even nums");
var evenNums = numbers.Where(x => x % 2 == 0).OrderBy(x => x);
foreach (var item in evenNums)
{
    Console.WriteLine(item);
}

Console.WriteLine();

Console.WriteLine("Odd nums");
var oddNums = numbers.Where(x => x % 2 != 0).OrderBy(x => x);
foreach (var item in oddNums)
{
    Console.WriteLine(item);
}

Console.WriteLine();

Console.WriteLine("Nums > 5 and < 10");
var nums = numbers.Where(x => x > 5 && x < 10).OrderBy(x => x);
foreach (var item in nums)
{
    Console.WriteLine(item);
}

Console.WriteLine();

//Bai tap tong hop 2
Console.WriteLine("Bai tap tong hop 2");
List<Product> products = new List<Product>
{
    new Product { Id = 1, Name = "Laptop", Price = 1000 },
    new Product { Id = 2, Name = "Phone", Price = 500 },
    new Product { Id = 3, Name = "Headphones", Price = 50 },
    new Product { Id = 4, Name = "Mouse", Price = 20 },
    new Product { Id = 5, Name = "Keyboard", Price = 30 }
};

Console.WriteLine("Names with price > 100, order by price desc");
var namesWithPrice = products.Where(x => x.Price > 100)
                            .OrderByDescending(x => x.Price)
                            .Select(x => x.Name);
foreach (var item in namesWithPrice)
{
    Console.WriteLine(item);
}

Console.WriteLine();

Console.WriteLine("Items with name contains 'o', order by name asc");
var namesContainsO = products.Where(x => x.Name.Contains('o')).OrderBy(x => x.Name);
foreach (var item in namesContainsO)
{
    Console.WriteLine(item.Id + " " + item.Name + " " + item.Price);
}



public class Employee
{
    public string Name { get; set; }
    public string Department { get; set; }
    public decimal Salary { get; set; }
}

class Person
{
    public string Name { get; set; }
    public int Age { get; set; }
}

class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
}

