﻿// Đọc dữ liệu từ file JSON
using Newtonsoft.Json;

var productsJson = File.ReadAllText("products.json");
var categoriesJson = File.ReadAllText("categories.json");

// Chuyển đổi JSON thành danh sách các đối tượng
var products = JsonConvert.DeserializeObject<List<Product>>(productsJson);
var categories = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);

var productsJoinCategories = products.Join(categories, x => x.CategoryId, x => x.Id, (products, categories) => new
{
    CategoryName = categories.Name,
    Products = products
});

//Lấy danh sách tên sản phẩm và giá cho các sản phẩm có giá trị lớn hơn $100, sắp xếp theo giá giảm dần.
Console.WriteLine("\nCau 1:");
var sortedProduct = products.Where(x => x.Price > 100).OrderByDescending(x => x.Price);
foreach (var item in sortedProduct)
{
    Console.WriteLine(item);
}

//Lấy danh sách tất cả các danh mục và số lượng sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
Console.WriteLine("\nCau 2:");
var groupByCategory = productsJoinCategories.GroupBy(x => x.CategoryName).ToDictionary(x => x.Key, x => x.Count()).OrderBy(x => x.Key);
foreach (var item in groupByCategory)
{
    Console.WriteLine(item.Key + " " + item.Value);
}

//Lấy giá trung bình của các sản phẩm cho mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
Console.WriteLine("\nCau 3:");
var avgByCategoryAsc = productsJoinCategories.GroupBy(x => x.CategoryName, x => x.Products.Price)
                                            .ToDictionary(x => x.Key, x => x.Average().ToString("#.##"))
                                            .OrderBy(x => x.Key);
foreach (var group in avgByCategoryAsc)
{
    Console.WriteLine(group.Key + " " + group.Value);
}

//Lấy danh sách tên sản phẩm và giá cho 10 sản phẩm đắt nhất, sắp xếp theo giá giảm dần.
Console.WriteLine("\nCau 4:");
var top10Product = products.OrderByDescending(x => x.Price).Take(10).Select(x => new
{
    Name = x.Name, 
    Price = x.Price
});

foreach (var item in top10Product)
{
    Console.WriteLine(item.Name + " " + item.Price);
}

//Lấy danh sách tất cả các danh mục và giá trung bình của các sản phẩm trong mỗi danh mục, sắp xếp theo giá trung bình giảm dần.
Console.WriteLine("\nCau 5:");
var avgByCategoryDesc = productsJoinCategories.GroupBy(x => x.CategoryName, x => x.Products.Price)
                                            .ToDictionary(x => x.Key, x => x.Average().ToString("#.##"))
                                            .OrderBy(x => x.Key);
foreach (var group in avgByCategoryDesc)
{
    Console.WriteLine(group.Key + " " + group.Value);
}

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có giá trị nhỏ hơn $50, sắp xếp theo tên danh mục tăng dần.
Console.WriteLine("\nCau 6:");
var nameSortedByCategory = productsJoinCategories.Where(x => x.Products.Price < 50).Select(x => new
                                                                                    {
                                                                                        CategoryName = x.CategoryName,
                                                                                        ProductName = x.Products.Name,
                                                                                        Price = x.Products.Price
                                                                                    }).OrderBy(x => x.CategoryName);

foreach (var item in nameSortedByCategory)
{
    Console.WriteLine("Category: " + item.CategoryName + ", Product: " + item.ProductName + ", Price: " + item.Price);
}

//Tính tổng giá của tất cả sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
Console.WriteLine("\nCau 7:");
var sumByCategoryAsc = productsJoinCategories.GroupBy(x => x.CategoryName, x => x.Products.Price)
                                            .ToDictionary(x => x.Key, x => x.Sum().ToString("#.##"))
                                            .OrderBy(x => x.Key);
foreach (var group in sumByCategoryAsc)
{
    Console.WriteLine(group.Key + " " + group.Value);
}

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có tên chứa từ "Apple", sắp xếp theo tên sản phẩm tăng dần.
Console.WriteLine("\nCau 8:");
var nameApple = productsJoinCategories.Where(x => x.Products.Name.Contains("Apple")).Select(x => new
{
    Name = x.Products.Name,
    Category = x.CategoryName
});
foreach (var item in nameApple)
{
    Console.WriteLine(item.Name + " " + item.Category);
}

//Lấy danh sách tất cả các danh mục và tổng giá của các sản phẩm trong mỗi danh mục, chỉ lấy những danh mục có tổng giá trị lớn hơn $1000.
Console.WriteLine("\nCau 9:");
var sum1000ByCategoryAsc = productsJoinCategories.GroupBy(x => x.CategoryName, x => x.Products.Price)
                                            .ToDictionary(x => x.Key, x => x.Sum())
                                            .Where(x => x.Value > 1000)
                                            .OrderBy(x => x.Key);
foreach (var group in sumByCategoryAsc)
{
    Console.WriteLine(group.Key + " " + group.Value);
}

//Kiểm tra xem có danh mục nào có sản phẩm có giá trị nhỏ hơn $10 không, nếu có, lấy danh sách tên của những danh mục đó.

//Lấy danh sách các sản phẩm có giá trị lớn nhất trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.

//Lấy danh sách tất cả các danh mục và tổng số tiền của các sản phẩm trong mỗi danh mục, sắp xếp theo tổng số tiền giảm dần.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị lớn hơn giá trị trung bình của tất cả các sản phẩm.

//Tính tổng số tiền của tất cả các sản phẩm.
Console.WriteLine("\n Cau 14:");
var sum = products.Sum(x => x.Price);
Console.WriteLine(sum);

//Lấy danh sách các danh mục và số lượng sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo số lượng sản phẩm giảm dần.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị giảm dần.

//Lấy danh sách các danh mục có ít sản phẩm nhất.
Console.WriteLine("\n Cau 17:");
var groupCountByCategory = productsJoinCategories.GroupBy(x => x.CategoryName).ToDictionary(x => x.Key, x => x.Count());
var minCount = groupCountByCategory.Min(x => x.Value);
var minCountByCategory = groupCountByCategory.Where(x => x.Value == minCount);
foreach (var item in minCountByCategory)
{
    Console.WriteLine(item.Key + " " + item.Value);
}

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.

//Tính tổng số tiền của các sản phẩm có giá trị lớn hơn $50.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.


class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int CategoryId { get; set; }
    public decimal Price { get; set; }
    public override string ToString()
    {
        return JsonConvert.SerializeObject(this);
    }
}

class Category
{
    public int Id { get; set; }
    public string Name { get; set; }
}
