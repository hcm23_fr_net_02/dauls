using DemoStack;

namespace TestDemoStack
{
    public class UnitTestStack
    {
        // Normal Stack
        [Fact]
        public void Test_Pop_Stack_Normal()
        {
            //arrange
            DemoStack<string> demoStack = new DemoStack<string>(5);

            for (int i = 1; i <= demoStack.Capacity; i++)
            {
                demoStack.Push(i + "");
            }

            List<string> expected = new List<string>() { "5", "4", "3", "2", "1" };

            //act
            List<string> actual = new List<string>();
            while (!demoStack.IsEmpty())
            {
                actual.Add(demoStack.Pop());
            }

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test_Peek_Stack_Normal()
        {
            //arrange
            DemoStack<string> demoStack = new DemoStack<string>(5);

            for (int i = 1; i <= demoStack.Capacity; i++)
            {
                demoStack.Push(i + "");
            }

            string expected = "5";

            //act
            string actual = demoStack.Peek();

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test_IsEmpty_Stack_Normal()
        {
            //arrange
            DemoStack<string> demoStack = new DemoStack<string>(5);

            for (int i = 1; i <= demoStack.Capacity; i++)
            {
                demoStack.Push(i + "");
            }

            bool expected = false;

            //act
            bool actual = demoStack.IsEmpty();


            //assert
            Assert.Equal(expected, actual);
        }

        //Empty Stack
        [Fact]
        public void Test_Pop_Stack_Empty()
        {
            //arrange
            DemoStack<string> demoStack = new DemoStack<string>(0);

            //act
            //assert
            Assert.Throws<InvalidOperationException>(() => demoStack.Pop());
        }

        [Fact]
        public void Test_Peek_Stack_Empty()
        {
            //arrange
            DemoStack<string> demoStack = new DemoStack<string>(0);

            //act
            //assert
            Assert.Throws<InvalidOperationException>(() => demoStack.Peek());
        }


        [Fact]
        public void Test_IsEmpty_Stack_Empty()
        {
            //arrange
            DemoStack<string> demoStack = new DemoStack<string>(0);

            bool expected = true;

            //act
            bool actual = demoStack.IsEmpty();


            //assert
            Assert.Equal(expected, actual);
        }

        //Full Stack
        [Fact]
        public void Test_Push_Stack_Full()
        {
            //arrange
            DemoStack<string> demoStack = new DemoStack<string>(5);

            for (int i = 1; i <= demoStack.Capacity; i++)
            {
                demoStack.Push(i + "");
            }

            //act
            //assert
            Assert.Throws<InvalidOperationException>(() => demoStack.Push("a"));
        }
    }
}