﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoStack
{
    public class StackNode<T>
    {
        public StackNode<T> next { get; set; }
        public T data { get; set; }
    }

    public class DemoStack<T>
    {
        private StackNode<T> top;
        public int Capacity { get; set; }
        public int CurrentCount { get; set; }

        public DemoStack(int capacity)
        {
            top = null;
            Capacity = capacity;
            CurrentCount = 0;
        }

        public T Peek()
        {
            if (IsEmpty())
            {
                throw new InvalidOperationException();
            }

            return top.data;
        }

        public bool IsEmpty()
        {
            return top == null;
        }

        public void Push(T item)
        {
            if (CurrentCount == Capacity)
            {
                throw new InvalidOperationException();
            }

            StackNode<T> newNode = new StackNode<T>();
            newNode.data = item;
            newNode.next = top;

            CurrentCount++;
            top = newNode;
        }

        public T Pop()
        {
            if (IsEmpty())
            {
                throw new InvalidOperationException();
            }

            StackNode<T> topNode = top;
            top = top.next;
            CurrentCount++;
            return topNode.data;
        }
    }
}
