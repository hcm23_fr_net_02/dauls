﻿namespace DemoStack
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DemoStack<string> demoStack = new DemoStack<string>(5);
            Console.WriteLine("Demo stack: ");

            for (int i = 1; i <= demoStack.Capacity; i++)
            {
                demoStack.Push(i + "");
            }

            while (!demoStack.IsEmpty())
            {
                Console.WriteLine(demoStack.Pop());
            }
        }
    }
}