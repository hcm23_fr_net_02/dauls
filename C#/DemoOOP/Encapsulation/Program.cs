﻿using Encapsulation;

int masterChoice = 0;
Library library = new Library();

while (masterChoice != 4)
{
BEGIN:
    Console.Clear();
    Console.WriteLine("---------------Menu---------------");
    Console.WriteLine("1. Xem danh sach cac cuon sach trong thu vien");
    Console.WriteLine("2. Muon sach");
    Console.WriteLine("3. Tra sach");
    Console.WriteLine("4. Exit");
    Console.Write("Nhap lua chon cua ban (1-4): ");
    string masterInput = Console.ReadLine();


    if (!int.TryParse(masterInput, out masterChoice))
    {
        Console.WriteLine("Hay nhap lua chon bang so!");
        Console.WriteLine("Nhan phim bat ky de tiep tuc");
        goto BEGIN;
    }

    switch (masterChoice)
    {
        case 1:
            library.PrintBookInfo();
            Console.WriteLine("Nhan phim bat ky de tiep tuc");
            Console.ReadKey();
            break;
        case 2:
        CHECKOUT:
            library.PrintAvailableBook();

            Console.WriteLine("Nhap book id de muon sach: ");
            string checkoutInput = Console.ReadLine();
            int checkoutId = 0;

            if (!int.TryParse(checkoutInput, out checkoutId))
            {
                Console.WriteLine("Hay nhap lua chon bang so!");
                Console.WriteLine("Nhan phim bat ky de tiep tuc");
                Console.ReadKey();
                goto CHECKOUT;
            }

            Book checkOutBook = new Book();

            try
            {
                checkOutBook = library.CheckOut(checkoutId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Nhan phim bat ky de tiep tuc");
                Console.ReadKey();
                goto CHECKOUT;
            }

            Console.WriteLine(checkOutBook.Id + " " + checkOutBook.Title);
            break;
        case 3:
        RETURNBOOK:
            Console.WriteLine("Nhap book id de tra sach: ");
            string returnbookInput = Console.ReadLine();
            int returnbookId = 0;

            if(!int.TryParse(returnbookInput, out returnbookId))
            {
                Console.WriteLine("Hay nhap lua chon bang so!");
                Console.WriteLine("Nhan phim bat ky de tiep tuc");
                Console.ReadKey();
                goto RETURNBOOK;
            }

            bool result = false;

            try
            {
                result = library.ReturnBook(returnbookId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Nhan phim bat ky de tiep tuc");
                Console.ReadKey();
                goto RETURNBOOK;
            }

            if (result)
            {
                Console.WriteLine("Tra sach thanh cong");
            }

            break;
        default:
            break;
    }
}