﻿namespace Encapsulation
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public bool Available { get; set; }

        public Book()
        {

        }

        public Book(int id, string title, string author, bool available)
        {
            this.Id = id;
            this.Title = title;
            this.Author = author;
            this.Available = available;
        }
    }
}
