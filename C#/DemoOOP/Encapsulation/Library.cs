﻿using Newtonsoft.Json;

namespace Encapsulation
{
    public class Library
    {
        List<Book> library;
        public Library()
        {
            library = new List<Book>();
            LoadLibraryFromFile();
        }

        public Book CheckOut(int id)
        {
            Book book = library.SingleOrDefault(x => x.Id == id);

            if (book is null)
            {
                throw new Exception("This book id do not exist");
            }

            if (book.Available is false)
            {
                throw new Exception("This book is not available");
            }

            book.Available = false;

            SaveLibraryToFile();

            return book;
        }

        public bool ReturnBook(int id)
        {
            Book book = library.SingleOrDefault(x => x.Id == id);

            if (book is null)
            {
                throw new Exception("This book id do not exist");
            }

            if (book.Available is false)
            {
                throw new Exception("This book is already available");
            }

            book.Available = true;

            SaveLibraryToFile();

            return true;
        }

        public void PrintAvailableBook()
        {
            var availableBooks = library.Where(x => x.Available is true);

            Console.WriteLine("Books are available: ");

            foreach (var item in availableBooks)
            {
                Console.WriteLine(item.Id + " " + item.Title);
            }
        }

        public void PrintBookInfo()
        {
            foreach (Book item in library)
            {
                Console.WriteLine(item.Id + " " + item.Title);
            }
        }

        public void SaveLibraryToFile()
        {
            string json = JsonConvert.SerializeObject(library);
            File.WriteAllText("library.json", json);
        }

        public void LoadLibraryFromFile()
        {
            if (File.Exists("library.json"))
            {
                string json = File.ReadAllText("library.json");
                library = JsonConvert.DeserializeObject<List<Book>>(json);
            }
        }
    }
}
