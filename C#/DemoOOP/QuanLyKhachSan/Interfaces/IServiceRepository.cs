﻿using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Interfaces
{
    public interface IServiceRepository
    {
        public List<Service> PrintListServices();
        public Service FindServiceById(int id);
    }
}
