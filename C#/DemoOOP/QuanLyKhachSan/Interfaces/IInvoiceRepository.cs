﻿using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Interfaces
{
    public interface IInvoiceRepository
    {
        public int AddInvoice(Invoice invoice);
        public List<Invoice> PrintListInvoices();
        public void RemoveInvoice(int id);
    }
}
