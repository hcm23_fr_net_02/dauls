﻿using QuanLyKhachSan.Enums;
using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Interfaces
{
    public interface IRoomRepository
    {
        public void AddRoom(Room room);
        public void RemoveRoom(int id);
        public List<Room> PrintListRooms();
        public Room FindRoomById(int id);
        public List<Room> FindRoomByType(RoomType type);
    }
}
