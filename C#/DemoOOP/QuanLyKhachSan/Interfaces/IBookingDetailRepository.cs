﻿using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Interfaces
{
    public interface IBookingDetailRepository
    {
        public void AddBookingDetail(BookingDetail bookingDetail);
        public List<BookingDetail> PrintListBookingDetailByBookingId(int bookingId);
    }
}
