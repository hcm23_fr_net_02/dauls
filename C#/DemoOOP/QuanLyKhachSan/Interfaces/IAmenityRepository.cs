﻿using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Interfaces
{
    public interface IAmenityRepository
    {
        public List<Amenity> PrintListAmenities();
        public Amenity FindAmenityById(int id);
    }
}
