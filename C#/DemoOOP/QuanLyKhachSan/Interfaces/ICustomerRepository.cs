﻿using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Interfaces
{
    public interface ICustomerRepository
    {
        public int AddCustomer(Customer customer);
        public Customer FindCustomerByPhoneNumber(long phoneNumber);
    }
}
