﻿using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Interfaces
{
    public interface IInvoiceServiceRepository
    {
        public void AddInvoiceService(InvoiceService invoiceService);
        public List<InvoiceService> PrintListInvoiceServiceByInvoiceId(int invoiceId);
    }
}
