﻿using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Interfaces
{
    public interface IBookingRepository
    {
        public int AddBooking(Booking booking);
        public List<Booking> PrintListBooking();
        public Booking FindBookingById(int id);
        public void RemoveBooking(int id);
    }
}
