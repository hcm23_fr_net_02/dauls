﻿using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Interfaces
{
    public interface IRoomAmenityRepository
    {
        public void AddRoomAmenity(RoomAmenity roomAmenity);
        public List<RoomAmenity> PrintListRoomAmenitiesByRoomId(int roomId);
    }
}
