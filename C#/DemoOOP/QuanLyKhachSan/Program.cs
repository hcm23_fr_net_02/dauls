﻿using QuanLyKhachSan;
using QuanLyKhachSan.Repositories;

DbContext dbContext = new DbContext();
AmenityRepository amenityRepository = new AmenityRepository(dbContext);
BookingDetailRepository bookingDetailRepository = new BookingDetailRepository(dbContext);
BookingRepository bookingRepository = new BookingRepository(dbContext);
CustomerRepository customerRepository = new CustomerRepository(dbContext);
InvoiceRepository invoiceRepository = new InvoiceRepository(dbContext);
InvoiceServiceRepository invoiceServiceRepository = new InvoiceServiceRepository(dbContext);
RoomAmenityRepository roomAmenityRepository = new RoomAmenityRepository(dbContext);
RoomRepository roomRepository = new RoomRepository(dbContext);
ServiceRepository serviceRepository = new ServiceRepository(dbContext);

Hotel hotel = new Hotel(amenityRepository, bookingDetailRepository, bookingRepository,
    customerRepository, invoiceRepository, invoiceServiceRepository, roomAmenityRepository,
    roomRepository, serviceRepository);

int masterChoice = 0;

while (masterChoice != 7)
{
BEGIN:
    Console.Clear();

    Console.WriteLine("--------------Menu--------------");
    Console.WriteLine("1. Add new room");
    Console.WriteLine("2. Delete a room");
    Console.WriteLine("3. Print all rooms");
    Console.WriteLine("4. Book rooms");
    Console.WriteLine("5. Print all booking");
    Console.WriteLine("6. Check out and create invoice");
    Console.WriteLine("7. Exit");

    Console.WriteLine("Input your choice (1-7)");
    string inputMaster = Console.ReadLine();

    if (!int.TryParse(inputMaster, out masterChoice))
    {
        Console.WriteLine("Choice must be a number");
        Console.WriteLine(NotificationConstant.PressKeyToContinue);
        Console.ReadKey();
        goto BEGIN;
    }

    switch (masterChoice)
    {
        case 1:

            try
            {
                hotel.AddRoom();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 1;
            }

            break;
        case 2:

            try
            {
                hotel.PrintListRooms();
                hotel.DeleteRoom();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 2;
            }

            break;
        case 3:

            try
            {
                hotel.PrintListRooms();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 3;
            }

            break;
        case 4:

            try
            {
                int cusomterId = hotel.AddCustomer();
                hotel.BookRoom(cusomterId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 4;
            }

            break;
        case 5:

            try
            {
                hotel.PrintListBooking();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 5;
            }

            break;
        case 6:

            try
            {
                hotel.CheckOut();
                hotel.PrintListInvoices();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 6;
            }

            break;
        default:
            break;
    }

    Console.WriteLine(NotificationConstant.PressKeyToContinue);
    Console.ReadKey();
}