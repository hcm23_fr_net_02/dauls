﻿using QuanLyKhachSan.Interfaces;
using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Repositories
{
    public class ServiceRepository : IServiceRepository
    {
        private readonly DbContext _dbContext;

        public ServiceRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Service FindServiceById(int id)
        {
            if (id < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            Service service = _dbContext.Service.SingleOrDefault(x => x.Id == id);

            if (service is null)
            {
                throw new Exception(NotificationConstant.ObjectDoesNotExist);
            }

           return service;
        }

        public List<Service> PrintListServices()
        {
            if (_dbContext.Service is null)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return _dbContext.Service;
        }
    }
}
