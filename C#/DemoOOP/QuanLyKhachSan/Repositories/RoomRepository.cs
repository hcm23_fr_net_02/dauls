﻿using QuanLyKhachSan.Enums;
using QuanLyKhachSan.Interfaces;
using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Repositories
{
    public class RoomRepository : IRoomRepository
    {
        private readonly DbContext _dbContext;

        public RoomRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddRoom(Room room)
        {
            if (_dbContext.Room.Count != 0)
            {
                int latestId = _dbContext.Room[_dbContext.Room.Count - 1].Id;
                room.Id = latestId + 1;
            }

            _dbContext.Room.Add(room);
        }

        public Room FindRoomById(int id)
        {
            if (id < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            Room room = _dbContext.Room.SingleOrDefault(x => x.Id == id);

            if (room is null)
            {
                throw new Exception(NotificationConstant.ObjectDoesNotExist);
            }

            return room;
        }

        public List<Room> FindRoomByType(RoomType type)
        {
            List<Room> listRooms = _dbContext.Room.Where(x => x.Type == type).ToList();

            if (listRooms is null)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return listRooms;
        }

        public List<Room> PrintListRooms()
        {
            if (_dbContext.Room is null)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return _dbContext.Room;
        }

        public void RemoveRoom(int id)
        {
            if (id < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            Room room = _dbContext.Room.SingleOrDefault(x => x.Id == id);

            if (room is null)
            {
                throw new Exception(NotificationConstant.ObjectDoesNotExist);
            }

            _dbContext.Room.Remove(room);
        }
    }
}
