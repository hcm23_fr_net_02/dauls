﻿using QuanLyKhachSan.Interfaces;
using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Repositories
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private readonly DbContext _dbContext;

        public InvoiceRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int AddInvoice(Invoice invoice)
        {
            if (_dbContext.Invoice.Count != 0)
            {
                int latestId = _dbContext.Invoice[_dbContext.Invoice.Count - 1].Id;
                invoice.Id = latestId + 1;
            }

            _dbContext.Invoice.Add(invoice);

            return invoice.Id;
        }

        public List<Invoice> PrintListInvoices()
        {
            if (_dbContext.Invoice is null)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return _dbContext.Invoice;
        }

        public void RemoveInvoice(int id)
        {
            if (id < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            Invoice invoice = _dbContext.Invoice.SingleOrDefault(x => x.Id == id);

            if (invoice is null)
            {
                throw new Exception(NotificationConstant.ObjectDoesNotExist);
            }

            _dbContext.Invoice.Remove(invoice);
        }
    }
}
