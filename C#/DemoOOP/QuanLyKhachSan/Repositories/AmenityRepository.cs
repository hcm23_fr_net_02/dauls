﻿using QuanLyKhachSan.Interfaces;
using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Repositories
{
    public class AmenityRepository : IAmenityRepository
    {
        private readonly DbContext _dbContext;

        public AmenityRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Amenity FindAmenityById(int id)
        {
            if (id < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            Amenity amenity = _dbContext.Amenity.SingleOrDefault(x => x.Id == id);

            if (amenity is null)
            {
                throw new Exception(NotificationConstant.ObjectDoesNotExist);
            }

            return amenity;
        }

        public List<Amenity> PrintListAmenities()
        {
            if (_dbContext.Amenity is null)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return _dbContext.Amenity;
        }
    }
}
