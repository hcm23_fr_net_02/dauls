﻿using QuanLyKhachSan.Interfaces;
using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Repositories
{
    public class BookingDetailRepository : IBookingDetailRepository
    {
        private readonly DbContext _dbContext;

        public BookingDetailRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddBookingDetail(BookingDetail bookingDetail)
        {
            if (_dbContext.BookingDetail.Count != 0)
            {
                int latestId = _dbContext.BookingDetail[_dbContext.BookingDetail.Count - 1].Id;
                bookingDetail.Id = latestId + 1;
            }

            _dbContext.BookingDetail.Add(bookingDetail);
        }

        public List<BookingDetail> PrintListBookingDetailByBookingId(int bookingId)
        {
            if (_dbContext.BookingDetail is null)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return _dbContext.BookingDetail.Where(x => x.BookingId == bookingId).ToList();
        }
    }
}
