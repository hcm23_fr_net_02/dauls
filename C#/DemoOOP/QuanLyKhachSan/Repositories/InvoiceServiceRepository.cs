﻿using QuanLyKhachSan.Interfaces;
using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Repositories
{
    public class InvoiceServiceRepository : IInvoiceServiceRepository
    {
        private readonly DbContext _dbContext;

        public InvoiceServiceRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddInvoiceService(InvoiceService invoiceService)
        {
            if (_dbContext.InvoiceService.Count != 0)
            {
                int latestId = _dbContext.InvoiceService[_dbContext.InvoiceService.Count - 1].Id;
                invoiceService.Id = latestId + 1;
            }

            _dbContext.InvoiceService.Add(invoiceService);
        }

        public List<InvoiceService> PrintListInvoiceServiceByInvoiceId(int invoiceId)
        {
            if (_dbContext.InvoiceService is null)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return _dbContext.InvoiceService;
        }
    }
}
