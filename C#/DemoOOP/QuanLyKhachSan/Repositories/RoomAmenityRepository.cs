﻿using QuanLyKhachSan.Interfaces;
using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Repositories
{
    public class RoomAmenityRepository : IRoomAmenityRepository
    {
        private readonly DbContext _dbContext;

        public RoomAmenityRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddRoomAmenity(RoomAmenity roomAmenity)
        {
            if (_dbContext.RoomAmenity.Count != 0)
            {
                int latestId = _dbContext.RoomAmenity[_dbContext.RoomAmenity.Count - 1].Id;
                roomAmenity.Id = latestId + 1;
            }

            _dbContext.RoomAmenity.Add(roomAmenity);
        }

        public List<RoomAmenity> PrintListRoomAmenitiesByRoomId(int roomId)
        {
            if (_dbContext.RoomAmenity is null)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return _dbContext.RoomAmenity.Where(x => x.RoomId == roomId).ToList();
        }
    }
}
