﻿using QuanLyKhachSan.Interfaces;
using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Repositories
{
    internal class CustomerRepository : ICustomerRepository
    {
        private readonly DbContext _dbContext;

        public CustomerRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int AddCustomer(Customer customer)
        {
            if (_dbContext.Customer.Count != 0)
            {
                int latestId = _dbContext.Customer[_dbContext.Customer.Count - 1].Id;
                customer.Id = latestId + 1;
            }

            _dbContext.Customer.Add(customer);

            return customer.Id;
        }

        public Customer FindCustomerByPhoneNumber(long phoneNumber)
        {
            if (phoneNumber < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            Customer customer = _dbContext.Customer.FirstOrDefault(x => x.PhoneNumber == phoneNumber);

            return customer;
        }
    }
}
