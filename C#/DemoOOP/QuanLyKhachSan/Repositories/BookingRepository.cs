﻿using QuanLyKhachSan.Interfaces;
using QuanLyKhachSan.Models;

namespace QuanLyKhachSan.Repositories
{
    internal class BookingRepository : IBookingRepository
    {
        private readonly DbContext _dbContext;

        public BookingRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int AddBooking(Booking booking)
        {
            if (_dbContext.Booking.Count != 0)
            {
                int latestId = _dbContext.Booking[_dbContext.Booking.Count - 1].Id;
                booking.Id = latestId + 1;
            }

            _dbContext.Booking.Add(booking);

            return booking.Id;
        }

        public Booking FindBookingById(int id)
        {
            if (id < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            Booking booking = _dbContext.Booking.SingleOrDefault(x => x.Id == id);

            if (booking is null)
            {
                throw new Exception(NotificationConstant.ObjectDoesNotExist);
            }

            return booking;
        }

        public List<Booking> PrintListBooking()
        {
            if (_dbContext.Booking is null)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return _dbContext.Booking;
        }

        public void RemoveBooking(int id)
        {
            if (id < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            Booking booking = _dbContext.Booking.SingleOrDefault(x => x.Id == id);

            if (booking is null)
            {
                throw new Exception(NotificationConstant.ObjectDoesNotExist);
            }

            _dbContext.Booking.Remove(booking);
        }
    }
}
