﻿using QuanLyKhachSan.Models;

namespace QuanLyKhachSan
{
    public class DbContext
    {
        public List<Room> Room { get; set; }
        public List<Booking> Booking { get; set; }
        public List<Customer> Customer { get; set; }
        public List<Invoice> Invoice { get; set; }
        public List<Amenity> Amenity { get; set; }
        public List<Service> Service { get; set; }
        public List<BookingDetail> BookingDetail { get; set; }
        public List<RoomAmenity> RoomAmenity { get; set; }
        public List<InvoiceService> InvoiceService { get; set; }

        public DbContext()
        {
            Room = new List<Room>()
            {
                new Room(0, Enums.RoomType.Single, 100),
                new Room(1, Enums.RoomType.Single, 100),
                new Room(2, Enums.RoomType.Double, 200),
                new Room(3, Enums.RoomType.Double, 200),
                new Room(4, Enums.RoomType.Family, 300),
                new Room(5, Enums.RoomType.Family, 300),
            };

            Amenity = new List<Amenity>()
            {
                new Amenity(0, "Wifi"),
                new Amenity(1, "TV"),
                new Amenity(2, "Minibar")
            };

            RoomAmenity = new List<RoomAmenity>()
            {
                new RoomAmenity(0, 0, 0),
                new RoomAmenity(1, 0, 1),
                new RoomAmenity(2, 1, 0),
                new RoomAmenity(3, 1, 1),
                new RoomAmenity(4, 2, 1),
                new RoomAmenity(5, 2, 2),
                new RoomAmenity(6, 3, 1),
                new RoomAmenity(7, 3, 2),
                new RoomAmenity(8, 4, 0),
                new RoomAmenity(9, 4, 2),
                new RoomAmenity(10, 5, 0),
                new RoomAmenity(11, 5, 2),
            };

            Service = new List<Service>()
            {
                new Service(0, "Dinner"),
                new Service(1, "Room service")
            };

            Customer = new List<Customer>();

            Booking = new List<Booking>();

            Invoice = new List<Invoice>();

            BookingDetail = new List<BookingDetail>();

            InvoiceService = new List<InvoiceService>();
        }
    }
}
