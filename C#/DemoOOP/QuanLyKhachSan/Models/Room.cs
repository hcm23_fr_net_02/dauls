﻿using QuanLyKhachSan.Enums;

namespace QuanLyKhachSan.Models
{
    public class Room
    {
        public int Id { get; set; }
        public RoomType Type { get; set; }
        public int Price { get; set; }
        public bool IsAvailable { get; set; }

        public Room(int id, RoomType type, int price)
        {
            Id = id;
            Type = type;
            Price = price;
            IsAvailable = true;
        }

        public override string ToString()
        {
            return $"Room number: {Id}, type: {Type}, price per night: {Price}, available: {IsAvailable}";
        }
    }
}
