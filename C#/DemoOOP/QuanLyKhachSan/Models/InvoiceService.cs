﻿namespace QuanLyKhachSan.Models
{
    public class InvoiceService
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public int ServiceId { get; set; }

        public InvoiceService(int id, int invoiceId, int serviceId)
        {
            Id = id;
            InvoiceId = invoiceId;
            ServiceId = serviceId;
        }
    }
}
