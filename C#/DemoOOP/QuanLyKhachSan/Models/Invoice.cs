﻿using System.Xml.Linq;

namespace QuanLyKhachSan.Models
{
    public class Invoice
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public DateTime CheckOut { get; set; }
        public int TotalPrice { get; set; }

        public Invoice(int id, int bookingId, int totalPrice)
        {
            Id = id;
            BookingId = bookingId;
            CheckOut = DateTime.Now;
            TotalPrice = totalPrice;
        }

        public override string ToString()
        {
            return $"{Id}, {BookingId}, {CheckOut}, {TotalPrice}";
        }
    }
}
