﻿using System.Diagnostics;

namespace QuanLyKhachSan.Models
{
    public class Booking
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime Booked { get; set; }
        public DateTime CheckIn { get; set; }
        public string? Message { get; set; }

        public Booking(int id, int customerId, DateTime checkIn, string message)
        {
            Id = id;
            CustomerId = customerId;
            CheckIn = checkIn;
            Booked = DateTime.Now;
            Message = message;
        }

        public override string ToString()
        {
            return $"Booking number: {Id}, booked date: {Booked}, check in date: {CheckIn}, message: {Message}";
        }
    }
}
