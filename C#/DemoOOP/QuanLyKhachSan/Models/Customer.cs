﻿namespace QuanLyKhachSan.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public long PhoneNumber { get; set; }
        public string? Address { get; set; }

        public Customer()
        {
            
        }

        public Customer(int id, string name, long phoneNumber, string address)
        {
            Id = id;
            Name = name;
            PhoneNumber = phoneNumber;
            Address = address;
        }
    }
}
