﻿namespace QuanLyKhachSan.Models
{
    public class RoomAmenity
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int AmenityId { get; set; }

        public RoomAmenity(int id, int roomId, int amenityId)
        {
            Id = id;
            RoomId = roomId;
            AmenityId = amenityId;
        }
    }
}
