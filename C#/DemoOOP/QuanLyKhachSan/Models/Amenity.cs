﻿namespace QuanLyKhachSan.Models
{
    public class Amenity
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public Amenity(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public override string ToString()
        {
            return $"{Id}. {Name}";
        }
    }
}
