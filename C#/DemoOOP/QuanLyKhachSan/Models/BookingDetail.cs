﻿namespace QuanLyKhachSan.Models
{
    public class BookingDetail
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public int RoomId { get; set; }

        public BookingDetail(int id, int bookingId, int roomId)
        {
            Id = id;
            BookingId = bookingId;
            RoomId = roomId;
        }
    }
}
