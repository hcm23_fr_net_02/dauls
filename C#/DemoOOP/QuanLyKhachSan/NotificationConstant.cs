﻿namespace QuanLyKhachSan
{
    public static class NotificationConstant
    {
        public const string AllInfoMustBeFilled = "All information fields must be filled";
        public const string NumberMustGreaterThanOrEqualZero = "Number must be greater than or equal zero";
        public const string FieldMustBeANumber = "This field must be a number";
        public const string IdNotExist = "This id does not exist";
        public const string ObjectDoesNotExist = "This object does not exist";
        public const string HaveNoObject = "Have no object";
        public const string Success = "Success";
        public const string PressKeyToContinue = "Press any key to continue";
    }
}
