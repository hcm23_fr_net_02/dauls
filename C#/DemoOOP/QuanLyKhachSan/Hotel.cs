﻿using QuanLyKhachSan.Enums;
using QuanLyKhachSan.Interfaces;
using QuanLyKhachSan.Models;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace QuanLyKhachSan
{
    public class Hotel
    {
        private readonly IAmenityRepository _amenityRepository;
        private readonly IBookingDetailRepository _bookDetailRepository;
        private readonly IBookingRepository _bookingRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IInvoiceServiceRepository _invoiceServiceRepository;
        private readonly IRoomAmenityRepository _roomAmenityRepository;
        private readonly IRoomRepository _roomRepository;
        private readonly IServiceRepository _serviceRepository;

        public Hotel(IAmenityRepository amenityRepository, IBookingDetailRepository bookingDetailRepository, IBookingRepository bookingRepository, ICustomerRepository customerRepository,
            IInvoiceRepository invoiceRepository, IInvoiceServiceRepository invoiceServiceRepository, IRoomAmenityRepository roomAmenityRepository,
            IRoomRepository roomRepository, IServiceRepository serviceRepository)
        {
            _amenityRepository = amenityRepository;
            _bookDetailRepository = bookingDetailRepository;
            _bookingRepository = bookingRepository;
            _customerRepository = customerRepository;
            _invoiceRepository = invoiceRepository;
            _invoiceServiceRepository = invoiceServiceRepository;
            _roomAmenityRepository = roomAmenityRepository;
            _roomRepository = roomRepository;
            _serviceRepository = serviceRepository;
        }

        public void AddRoom()
        {
            Console.Clear();

            //-------------------------------- Create Booking -------------------------------- 

            Console.WriteLine("1. Single");
            Console.WriteLine("2. Double");
            Console.WriteLine("3. Family");

            Console.WriteLine("Input new room information (Type 1-3, Price): ");
            string[] roomInfo = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);

            int type = 0;
            int price = 0;

            if (roomInfo.Length < 2)
            {
                throw new Exception(NotificationConstant.AllInfoMustBeFilled);
            }

            if (!int.TryParse(roomInfo[0], out type))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            if (!int.TryParse(roomInfo[1], out price))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            Room newRoom = new Room(0, RoomType.Single, price);

            switch (type)
            {
                case 2:
                    newRoom.Type = RoomType.Double;
                    break;
                case 3:
                    newRoom.Type = RoomType.Family;
                    break;
                default:
                    break;
            }

            _roomRepository.AddRoom(newRoom);

            //-------------------------------- Add Amenities For Room -------------------------------- 

            List<Amenity> listAmenities = _amenityRepository.PrintListAmenities();

            foreach (Amenity amenity in listAmenities)
            {
                Console.WriteLine(amenity);
            }

            Console.WriteLine("Input amenities to this room (1, 2, 3,...): ");
            string[] inputAmenities = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);

            for (int i = 0; i < inputAmenities.Length; i++)
            {
                int amenityId = 0;

                if (!int.TryParse(inputAmenities[i], out amenityId))
                {
                    throw new Exception(NotificationConstant.FieldMustBeANumber);
                }

                RoomAmenity roomAmenity = new RoomAmenity(0, newRoom.Id, amenityId);

                _roomAmenityRepository.AddRoomAmenity(roomAmenity);
            }
        }

        public void DeleteRoom()
        {
            Console.Write("Input room id that you want to delete: ");
            string input = Console.ReadLine();
            int roomId = 0;

            if (int.TryParse(input, out roomId))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            _roomRepository.RemoveRoom(roomId);
        }

        public void PrintListRooms()
        {
            Console.Clear();

            List<Room> listRooms = _roomRepository.PrintListRooms();

            foreach (Room room in listRooms)
            {
                Console.WriteLine(room);
                List<RoomAmenity> listRoomAmenities = _roomAmenityRepository.PrintListRoomAmenitiesByRoomId(room.Id);
                Console.WriteLine($"Amenities of room number {room.Id}: ");

                foreach (RoomAmenity roomAmenity in listRoomAmenities)
                {
                    Amenity amenity = _amenityRepository.FindAmenityById(roomAmenity.AmenityId);
                    Console.WriteLine(amenity.Name);
                }

                Console.WriteLine();
            }
        }

        public List<int> PrintListRoomsByType()
        {
            Console.Clear();

            List<int> listRoomsId = new List<int>();
            List<Room> listRoomsByType = new List<Room>();

            Console.WriteLine("1. Single");
            Console.WriteLine("2. Double");
            Console.WriteLine("3. Family");

            Console.Write("Input type of room you want to book: ");
            string inputType = Console.ReadLine();
            int type = 0;

            if (!int.TryParse(inputType, out type))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            switch (type)
            {
                case 1:
                    listRoomsByType = _roomRepository.FindRoomByType(RoomType.Single);
                    break;
                case 2:
                    listRoomsByType = _roomRepository.FindRoomByType(RoomType.Double);
                    break;
                case 3:
                    listRoomsByType = _roomRepository.FindRoomByType(RoomType.Family);
                    break;
                default:
                    break;
            }

            foreach (Room room in listRoomsByType)
            {
                Console.WriteLine(room);
                List<RoomAmenity> listRoomAmenities = _roomAmenityRepository.PrintListRoomAmenitiesByRoomId(room.Id);
                listRoomsId.Add(room.Id);
                Console.WriteLine($"Amenities of room number {room.Id}: ");

                foreach (RoomAmenity roomAmenity in listRoomAmenities)
                {
                    Amenity amenity = _amenityRepository.FindAmenityById(roomAmenity.AmenityId);
                    Console.WriteLine(amenity.Name);
                }

                Console.WriteLine();
            }

            return listRoomsId;
        }

        public int AddCustomer()
        {
            Console.Clear();

            Console.WriteLine("Input customer phone number:");
            string inputPhoneNumber = Console.ReadLine();
            long phoneNumber = 0;
            int customerId = 0;

            if (inputPhoneNumber.Length < 10)
            {
                throw new Exception("Phone number must be 10 digits");
            }

            if (!long.TryParse(inputPhoneNumber, out phoneNumber))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            Customer customer = customer = _customerRepository.FindCustomerByPhoneNumber(phoneNumber);

            if (customer is null)
            {
                //If this customer does not exist in hotel then create new customer

                Console.WriteLine("This is a new customer, you need to input customer information");
                Console.WriteLine("Input customer information (Name, Address):");
                string[] customerInfo = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);

                if (customerInfo.Length < 0)
                {
                    throw new Exception(NotificationConstant.AllInfoMustBeFilled);
                }

                Customer newCustomer = new Customer(0, customerInfo[0], phoneNumber, customerInfo[1]);

                customerId = _customerRepository.AddCustomer(newCustomer);
            }
            else
            {
                customerId = customer.Id;
            }

            return customerId;
        }

        public void BookRoom(int customerId)
        {
            Console.Clear();

            //-------------------------------- Create Booking -------------------------------- 

            Console.WriteLine("Input check in date (dd/mm/yyyy): ");
            string[] inputDate = Console.ReadLine().Split('/', StringSplitOptions.TrimEntries);
            int month = 0;
            int day = 0;
            int year = 0;

            if (inputDate.Length < 3)
            {
                throw new Exception(NotificationConstant.AllInfoMustBeFilled);
            }

            if (!int.TryParse(inputDate[0], out day) || !int.TryParse(inputDate[1], out month) || !int.TryParse(inputDate[2], out year))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            DateTime checkIn = new DateTime(year, month, day);

            if (checkIn < DateTime.Now)
            {
                throw new Exception("Check in date cannot be earlier current time");
            }

            Console.WriteLine("Input special notes (if any): ");
            string message = Console.ReadLine();

            Booking booking = new Booking(0, customerId, checkIn, message);
            int bookingId = _bookingRepository.AddBooking(booking);

            //-------------------------------- Create booking detail -------------------------------- 

            List<int> listRoomsId = PrintListRoomsByType();

            Console.WriteLine("Input rooms' id that you want to book (1, 2, 3,...):");
            string[] inputRoomsId = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);

            try
            {
                for (int i = 0; i < inputRoomsId.Length; i++)
                {
                    int roomId = 0;

                    if (!int.TryParse(inputRoomsId[i], out roomId))
                    {
                        throw new Exception(NotificationConstant.FieldMustBeANumber);
                    }

                    if (!listRoomsId.Contains(roomId))
                    {
                        throw new Exception($"{roomId} does not belong to this type");
                    }

                    Room room = _roomRepository.FindRoomById(roomId);

                    if (room.IsAvailable is false)
                    {
                        throw new Exception($"Room {roomId} has been booked");
                    }

                    room.IsAvailable = false;
                    BookingDetail bookingDetail = new BookingDetail(0, bookingId, roomId);

                    _bookDetailRepository.AddBookingDetail(bookingDetail);
                }
            }
            catch (Exception)
            {
                _bookingRepository.RemoveBooking(bookingId);
                throw;
            }     
        }

        public void CheckOut()
        {
            Console.Clear();

            PrintListBooking();

            //-------------------------------- Create Invoice -------------------------------- 

            Console.Write("Input booking id that you want to check out: ");
            string input = Console.ReadLine();
            int bookingId = 0;

            if (!int.TryParse(input, out bookingId))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            Booking booking = _bookingRepository.FindBookingById(bookingId);
            List<BookingDetail> listBookingDetails = _bookDetailRepository.PrintListBookingDetailByBookingId(bookingId);
            DateTime fakeCheckout = booking.CheckIn;
            fakeCheckout = fakeCheckout.AddDays(5);
            TimeSpan retalDuration = fakeCheckout - booking.CheckIn;
            int days = retalDuration.Days;
            int totalPrice = 0;

            if (days <= 0)
            {
                throw new Exception("Cannot check out this booking because there is no rental duration");
            }

            foreach (BookingDetail item in listBookingDetails)
            {
                Room room = _roomRepository.FindRoomById(item.RoomId);
                room.IsAvailable = true;
                totalPrice += room.Price * days;
            }

            Invoice invoice = new Invoice(0, bookingId, totalPrice);
            int invoiceId = _invoiceRepository.AddInvoice(invoice);

            //-------------------------------- Create Invoice Service -------------------------------- 

            List<Service> listServices = _serviceRepository.PrintListServices();

            foreach (Service service in listServices)
            {
                Console.WriteLine(service);
            }

            Console.WriteLine("Input service to this invoice (1, 2, 3,...): ");
            string[] inputServices = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);

            try
            {
                for (int i = 0; i < inputServices.Length; i++)
                {
                    int serviceId = 0;

                    if (!int.TryParse(inputServices[i], out serviceId))
                    {
                        throw new Exception(NotificationConstant.FieldMustBeANumber);
                    }

                    InvoiceService invoiceService = new InvoiceService(0, invoice.Id, serviceId);

                    _invoiceServiceRepository.AddInvoiceService(invoiceService);
                }
            }
            catch (Exception)
            {
                _invoiceRepository.RemoveInvoice(invoiceId);
                throw;
            }
        }

        public void PrintListBooking()
        {
            Console.Clear();

            try
            {
                List<Booking> listBookings = _bookingRepository.PrintListBooking();

                foreach (Booking booking in listBookings)
                {
                    Console.WriteLine(booking);
                    List<BookingDetail> listBookingDetails = _bookDetailRepository.PrintListBookingDetailByBookingId(booking.Id);
                    Console.WriteLine($"Booking details of book id {booking.Id}: ");

                    foreach (BookingDetail bookingDetail in listBookingDetails)
                    {
                        Room room = _roomRepository.FindRoomById(bookingDetail.RoomId);
                        Console.WriteLine(room);
                    }

                    Console.WriteLine();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void PrintListInvoices()
        {
            Console.Clear();

            List<Invoice> listInvoices = _invoiceRepository.PrintListInvoices();

            foreach (Invoice invoice in listInvoices)
            {
                Console.WriteLine(invoice);
                List<InvoiceService> listInvoiceService = _invoiceServiceRepository.PrintListInvoiceServiceByInvoiceId(invoice.Id);
                Console.WriteLine($"Services of invoice {invoice.Id}: ");

                foreach (InvoiceService invoiceService in listInvoiceService)
                {
                    Service service = _serviceRepository.FindServiceById(invoiceService.ServiceId);
                    Console.WriteLine(service.Name);
                }

                Console.WriteLine();
            }
        }
    }
}
