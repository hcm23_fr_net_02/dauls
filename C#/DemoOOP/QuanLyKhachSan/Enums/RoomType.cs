﻿namespace QuanLyKhachSan.Enums
{
    public enum RoomType
    {
        Single,
        Double,
        Family
    }
}
