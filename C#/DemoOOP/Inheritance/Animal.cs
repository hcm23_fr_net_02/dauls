﻿namespace Inheritance
{
    public class Animal
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Type { get; set; }

        public Animal(string name, int age, string type)
        {
            Name = name;
            Age = age;
            Type = type;
        }

        public void Speak()
        {
            Console.Write("The {0} named {1} says ", Type, Name);
        }
    }
}
