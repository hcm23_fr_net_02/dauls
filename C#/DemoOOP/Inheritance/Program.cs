﻿using Inheritance;

Animal[] animals = new Animal[6]
{
    new Dog("A", 2, "Dog", "England"),
    new Cat("B", 3, "Cat", "Black"),
    new Dog("C", 2, "Dog", "England"),
    new Dog("D", 4, "Dog", "Chinese"),
    new Cat("E", 1, "Cat", "White"),
    new Dog("F", 2, "Dog", "England"),
};

for (int i = 0; i < animals.Length; i++)
{
    animals[i].Speak();

    if (animals[i] is Dog)
    {
        Dog dog = (Dog)animals[i];
        dog.Bark();
    }
    else
    {
        Cat cat = (Cat)animals[i];
        cat.Meow();
    }
}