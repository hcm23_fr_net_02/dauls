﻿namespace Inheritance
{
    public class Dog : Animal
    {
        public string Breed { get; set; }
        
        public Dog(string name, int age, string type, string breed) : base (name, age, type)
        {
            Name = name;
            Age = age;
            Type = type;
            Breed = breed;
        }

        public void Bark()
        {
            Console.WriteLine("Woof! Woof!");
        }
    }
}
