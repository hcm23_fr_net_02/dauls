﻿namespace Inheritance
{
    public class Cat : Animal
    {
        public string FurColor { get; set; }
        
        public Cat(string name, int age, string type, string furcolor) : base(name, age, type)
        {
            Name = name;
            Age = age;
            Type = type;
            FurColor = furcolor;
        }

        public void Meow()
        {
            Console.WriteLine("Meow! Meow!");
        }
    }
}
