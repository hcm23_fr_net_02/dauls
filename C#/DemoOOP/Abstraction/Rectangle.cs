﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraction
{
    public class Rectangle : Shape
    {
        public double ChieuDai { get; set; }
        public double ChieuRong { get; set; }

        public Rectangle(double chieudai, double chieurong)
        {
            ChieuDai = chieudai;
            ChieuRong = chieurong;
        }

        public override void CalculateArea()
        {
            Console.WriteLine("Dien tich Rectangle: {0}", ChieuDai * ChieuRong);
        }
    }
}
