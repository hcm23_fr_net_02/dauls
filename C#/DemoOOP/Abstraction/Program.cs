﻿using Abstraction;

Shape[] shape = new Shape[3]
{
    new Circle(4.3),
    new Rectangle (4.3, 9.6),
    new Triangle(4.3, 8, 10)
};

for (int i = 0; i < shape.Length; i++)
{
    shape[i].CalculateArea();
}