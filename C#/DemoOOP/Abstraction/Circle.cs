﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraction
{
    public class Circle : Shape
    {
        private const double PI = Math.PI;
        public double BanKinh { get; set; }
        public Circle(double bankinh)
        {
            BanKinh = bankinh;
        }
        public override void CalculateArea()
        {
            Console.WriteLine("Dien tich Circle: {0}", BanKinh * BanKinh * PI);
        }
    }
}
