﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraction
{
    public class Triangle : Shape
    {
        public double Canh1 { get; set; }
        public double Canh2 { get; set; }
        public double Canh3 { get; set; }

        public Triangle(double canh1, double canh2, double canh3)
        {
            Canh1 = canh1;
            Canh2 = canh2;
            Canh3 = canh3;
        }

        public override void CalculateArea()
        {
            double p = (Canh1 + Canh2 + Canh3) / 2;
            Console.WriteLine("Dien tich Triangle: {0}", Math.Sqrt(p * (p - Canh1) * (p-Canh2) * (p-Canh3)));
        }
    }
}
