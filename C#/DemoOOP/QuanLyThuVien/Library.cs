﻿using QuanLyThuVien.Models;

namespace QuanLyThuVien
{
    public class Library : ILibrary
    {
        private List<Book> _books;
        private List<Customer> _customer;
        private List<Loan> _loan;
        private int _increaseBookId = 5;
        private int _increaseCustomerId = 5;
        private int _increaseLoanId = 0;

        public Library()
        {
            _books = new List<Book>()
            {
                new Book(0, "Book A", "Mr. A", 10, 100),
                new Book(1, "Book B", "Mr. B", 10, 100),
                new Book(2, "Book C", "Mr. C", 10, 100),
                new Book(3, "Book D", "Mr. D", 10, 100),
                new Book(4, "Book E", "Mr. E", 10, 100),
                new Book(5, "Book F", "Mr. F", 10, 100)
            };

            _customer = new List<Customer>()
            {
                new Customer(0, "Mr A", "DN"),
                new Customer(1, "Mr B", "HCM"),
                new Customer(2, "Mr C", "DN"),
                new Customer(3, "Mr D", "HN"),
                new Customer(4, "Mr E", "HCM"),
                new Customer(5, "Mr F", "HN"),

            };

            _loan = new List<Loan>();
        }

        public void AddBook(Book book)
        {
            book.Id = ++_increaseBookId;

            _books.Add(book);
        }

        public int AddCustomer(Customer customer)
        {
            customer.Id = ++_increaseCustomerId;

            _customer.Add(customer);

            return customer.Id;
        }

        public Book FindBookById(int bookId)
        {
            var book = _books.SingleOrDefault(x => x.Id == bookId);

            if (book is null)
            {
                throw new Exception($"This book id {bookId} does not exist!");
            }

            return book;
        }

        public void LendBooks(int customerId, Dictionary<int, int> borrowQuantity)
        {
            List<Book> borrowBooks = new List<Book>();

            foreach (var borrowBook in borrowQuantity)
            {
                var bookInLibrary = _books.SingleOrDefault(x => x.Id == borrowBook.Key);

                if (bookInLibrary is null)
                {
                    throw new Exception($"This book id {borrowBook.Key} does not exist!");
                }

                if (bookInLibrary.StockQuantity < 1)
                {
                    throw new Exception($"This book id {borrowBook.Key} is not available!");
                }

                if (borrowQuantity[borrowBook.Key] < 1)
                {
                    borrowQuantity.Remove(borrowBook.Key);
                }

                bookInLibrary.StockQuantity -= borrowQuantity[borrowBook.Key];

                borrowBooks.Add(bookInLibrary);
            }

            Loan loan = new Loan()
            {
                Id = ++_increaseLoanId,
                DateStart = DateTime.Now,
                DateExpired = DateTime.Now.AddDays(10),
                BooksLoan = borrowBooks,
                CustomerId = customerId,
            };

            _loan.Add(loan);
        }

        public void PrintAvailableBooks()
        {
            var availableBooks = _books.Where(x => x.StockQuantity > 0);

            if (availableBooks.Count() < 1)
            {
                throw new Exception("There are no available books!");
            }

            foreach (var item in availableBooks)
            {
                Console.WriteLine(item);
            }
        }

        public void PrintBooks()
        {
            if (_books.Count < 1)
            {
                throw new Exception("There are no books in this library!");
            }

            foreach (var item in _books)
            {
                Console.WriteLine(item);
            }
        }

        public void PrintLoan()
        {
            if (_loan.Count < 1)
            {
                throw new Exception("There is no loan in this library!");
            }

            foreach (var item in _loan)
            {
                Console.WriteLine(item);
                foreach (var booksLoan in item.BooksLoan)
                {
                    Console.WriteLine(booksLoan);
                }
            }
        }

        public void RemoveBook(int bookId)
        {
            var book = _books.SingleOrDefault(x => x.Id == bookId);

            if (book is null)
            {
                throw new Exception($"This book id {bookId} does not exist!");
            }

            _books.Remove(book);
        }

        public void RemoveCustomer(int customerId)
        {
            var customer = _customer.SingleOrDefault(x => x.Id == customerId);

            if (customer is null)
            {
                throw new Exception($"This customer id {customerId} does not exist!");
            }

            _customer.Remove(customer);
        }
    }
}