﻿using QuanLyThuVien.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyThuVien
{
    public class LibraryController
    {
        ILibrary _library;

        public LibraryController(ILibrary library)
        {
            _library = library;
        }

        public bool AddBook(string name, string author, int stockQuantity, int Price)
        {
            Book book = new Book(0, name, author, stockQuantity, Price);

            try
            {
                _library.AddBook(book);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }

        public int AddCustomer(string name, string address)
        {
            Customer customer = new Customer(0, name, address);

            try
            {
                customer.Id = _library.AddCustomer(customer);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return customer.Id;
        }

        public bool RemoveBook(int bookId)
        {
            try
            {
                _library.RemoveBook(bookId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }

        public void PrintBooks()
        {
            try
            {
                _library.PrintBooks();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void PrintLoan()
        {
            try
            {
                _library.PrintLoan();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void PrintAvailableBooks()
        {
            try
            {
                _library.PrintAvailableBooks();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public Book FindBookById(int bookId)
        {
            Book book = new Book();
            try
            {
                book = _library.FindBookById(bookId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return book;
        }

        public bool LendBooks(string name, string address, Dictionary<int, int> borrowQuantity)
        {
            int customerId = AddCustomer(name, address);

            try
            {
                _library.LendBooks(customerId, borrowQuantity);
            }
            catch (Exception e)
            {
                _library.RemoveCustomer(customerId);
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }
    }
}
