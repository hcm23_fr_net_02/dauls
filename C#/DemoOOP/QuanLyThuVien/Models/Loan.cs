﻿namespace QuanLyThuVien.Models
{
    public class Loan
    {
        public int Id { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateExpired { get; set; }
        public List<Book>? BooksLoan { get; set; }
        public int CustomerId { get; set; }

        public override string ToString()
        {
            return $"{Id} {DateStart} {DateExpired} {CustomerId}";
        }
    }
}
