﻿namespace QuanLyThuVien.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public int StockQuantity { get; set; }
        public int Price { get; set; }

        public Book()
        {
            
        }

        public Book(int id, string name, string author, int stockQuantity, int Price)
        {
            this.Id = id;
            this.Name = name;
            this.Author = author;
            this.StockQuantity = stockQuantity;
            this.Price = Price;
        }

        public override string ToString()
        {
            return $"{Id} {Name} {Author} {StockQuantity} {Price}";
        }
    }
}
