﻿using QuanLyThuVien.Models;

namespace QuanLyThuVien
{
    public interface ILibrary
    {
        public void AddBook(Book book);
        public int AddCustomer(Customer customer);
        public void RemoveBook(int bookId);
        public void RemoveCustomer(int customerId);
        public void PrintBooks();
        public void LendBooks(int customerId, Dictionary<int, int> borrowQuantity);
        public void PrintAvailableBooks();
        public Book FindBookById(int id);
        public void PrintLoan();
    }
}
