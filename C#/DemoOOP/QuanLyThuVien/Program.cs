﻿using QuanLyThuVien;

int masterChoice = 0;
Library library = new Library();
LibraryController libraryController = new LibraryController(library);

while (masterChoice != 7)
{
BEGIN:
    Console.Clear();
    Console.WriteLine("----------------------Menu----------------------");
    Console.WriteLine("1. Add new book");
    Console.WriteLine("2. Delete book");
    Console.WriteLine("3. Print books");
    Console.WriteLine("4. Lend books");
    Console.WriteLine("5. Print available books");
    Console.WriteLine("6. Find book by book id");
    Console.WriteLine("7. Exit");

    Console.WriteLine("Input your chocie: ");
    string masterInput = Console.ReadLine();

    if (!int.TryParse(masterInput, out masterChoice))
    {
        Console.WriteLine("Choice must be a number!");
        Console.WriteLine("Press any key to continue");
        Console.ReadKey();
        goto BEGIN;
    }

    switch (masterChoice)
    {
        case 1:

        ADDBOOK:

            Console.Clear();
            Console.WriteLine("Input book information (Name, Author, Stock Quantity, Price): ");
            string[] inputBookInfor = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);
            int stockQuantity = 0;
            int price = 0;

            if (!int.TryParse(inputBookInfor[2], out stockQuantity))
            {
                Console.WriteLine("Stock quantity must be a number!");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                goto ADDBOOK;
            }

            if (!int.TryParse(inputBookInfor[3], out price))
            {
                Console.WriteLine("Price must be a number!");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                goto ADDBOOK;
            }

            bool resultAddBook = libraryController.AddBook(inputBookInfor[0], inputBookInfor[1], stockQuantity, price);

            if (!resultAddBook)
            {
                Console.WriteLine("Cannot add new book!");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }

            Console.WriteLine("Add new book successfully!");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            break;
        case 2:

        REMOVEBOOK:

            Console.Clear();
            libraryController.PrintBooks();
            Console.Write("Input a book id to remove it: ");
            string inputRemoveBookId = Console.ReadLine();
            int removeBookId = 0;

            if (!int.TryParse(inputRemoveBookId, out removeBookId))
            {
                Console.WriteLine("Book id must be a number!");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                goto REMOVEBOOK;
            }

            bool resultRemoveBook = libraryController.RemoveBook(removeBookId);

            if (!resultRemoveBook)
            {
                Console.WriteLine("Cannot remove this book!");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }

            Console.WriteLine("Remove this book successfully");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            break;
        case 3:

            libraryController.PrintBooks();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            break;
        case 4:

        LENDBOOK:

            Console.Clear();
            libraryController.PrintAvailableBooks();

            Console.WriteLine("Input customer information (Name, Adress):");
            string[] customerInfo = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);

            Console.WriteLine("Input books' id to lend books (1, 2, 3, 4, 5):");
            string[] inputLendBookId = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);
            Dictionary<int, int> borrowQuantity = new Dictionary<int, int>();

            for (int i = 0; i < inputLendBookId.Length; i++)
            {
                Console.Write($"Add number of book id {inputLendBookId[i]} that you want to lend: ");
                string inputQuantity = Console.ReadLine();
                int quantity = 0;
                int lendBookId = 0;

                if (!int.TryParse(inputQuantity, out quantity))
                {
                    Console.WriteLine("Quantity must be a number!");
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                    goto LENDBOOK;
                }

                if (!int.TryParse(inputLendBookId[i], out lendBookId))
                {
                    Console.WriteLine("Book id must be a number!");
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                    goto LENDBOOK;
                }

                borrowQuantity.Add(lendBookId, quantity);
            }

            bool resultLendBooks = libraryController.LendBooks(customerInfo[0], customerInfo[1], borrowQuantity);

            if (!resultLendBooks)
            {
                Console.WriteLine("Cannot lend books");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                break;
            }

            Console.WriteLine("Lends book successfully");
            libraryController.PrintLoan();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            break;
        case 5:

            libraryController.PrintAvailableBooks();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            break;
        case 6:

        FINDBOOK:

            Console.Clear();
            Console.Write("Input a book id to find it: ");
            string inputFindBookId = Console.ReadLine();
            int findBookId = 0;

            if (!int.TryParse(inputFindBookId, out findBookId))
            {
                Console.WriteLine("Book id must be a number!");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                goto FINDBOOK;
            }

            Console.WriteLine(libraryController.FindBookById(findBookId));
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            break;
        default:
            break;
    }
}