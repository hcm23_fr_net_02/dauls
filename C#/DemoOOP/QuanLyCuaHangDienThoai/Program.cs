﻿using QuanLyCuaHangDienThoai;
using QuanLyCuaHangDienThoai.Repository;

DbContext dbContext = new DbContext();
PhoneRepository phoneRepository = new PhoneRepository(dbContext);
PurchaseRepository purchaseRepository = new PurchaseRepository(dbContext);
CustomerRepository customerRepository = new CustomerRepository(dbContext);
Store store = new Store(phoneRepository, purchaseRepository, customerRepository);

int masterChoice = 0;

while (masterChoice != 7)
{
BEGIN:
    Console.Clear();
    Console.WriteLine("--------------Menu--------------");
    Console.WriteLine("1. Add new phone");
    Console.WriteLine("2. Delete a phone");
    Console.WriteLine("3. Print all phones");
    Console.WriteLine("4. Buy phones");
    Console.WriteLine("5. Print all purchases");
    Console.WriteLine("6. Find phone by id");
    Console.WriteLine("7. Exit");

    Console.WriteLine("Input your choice (1-7)");
    string inputMaster = Console.ReadLine();

    if (!int.TryParse(inputMaster, out masterChoice))
    {
        Console.WriteLine("Choice must be a number");
        Console.WriteLine(NotificationConstant.PressKeyToContinue);
        Console.ReadKey();
        goto BEGIN;
    }

    switch (masterChoice)
    {
        case 1:

            try
            {
                store.AddPhone();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 1;
            }

            break;
        case 2:

            try
            {
                store.PrintListPhones();
                store.DeletePhone();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 2;
            }
            
            break;
        case 3:

            try
            {
                store.PrintListPhones();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 3;
            }

            break;
        case 4:

            try
            {
                store.PrintListPhones();
                store.BuyPhone();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 4;
            }

            break;
        case 5:

            try
            {
                store.PrintListPurchase();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 5;
            }

            break;
        case 6:

            try
            {
                store.FindPhoneById();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(NotificationConstant.PressKeyToContinue);
                Console.ReadKey();
                goto case 6;
            }

            break;
        default:
            break;
    }

    Console.WriteLine(NotificationConstant.PressKeyToContinue);
    Console.ReadKey();
}