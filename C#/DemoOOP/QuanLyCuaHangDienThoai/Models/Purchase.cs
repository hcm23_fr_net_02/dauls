﻿namespace QuanLyCuaHangDienThoai.Models
{
    public class Purchase
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime DateBuy { get; set; }
        public List<MobilePhone>? PurchasePhones { get; set; }

        public Purchase(int id, int customerId, List<MobilePhone> purchasePhones)
        {
            Id = id;
            CustomerId = customerId;
            DateBuy = DateTime.Now;
            PurchasePhones = purchasePhones;
        }

        public override string ToString()
        {
            return $"{Id} {CustomerId} {DateBuy}";
        }
    }
}