﻿namespace QuanLyCuaHangDienThoai.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Address { get; set; }
        public long PhoneNumber { get; set; }

        public Customer()
        {
            
        }

        public Customer(int id, string name, string address, long phoneNumber)
        {
            Id = id;
            Name = name;
            Address = address;
            PhoneNumber = phoneNumber;
        }
    }
}
