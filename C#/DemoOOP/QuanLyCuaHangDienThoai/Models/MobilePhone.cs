﻿namespace QuanLyCuaHangDienThoai.Models
{
    public class MobilePhone
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Brand { get; set; }
        public int StockQuantity { get; set; }
        public int Price { get; set; }

        public MobilePhone(int id, string name, string brand, int stockQuantity, int price)
        {
            Id = id;
            Name = name;
            Brand = brand;
            StockQuantity = stockQuantity;
            Price = price;
        }

        public override string ToString()
        {
            return $"{Id} {Name} {Brand} {StockQuantity} {Price}";
        }
    }
}
