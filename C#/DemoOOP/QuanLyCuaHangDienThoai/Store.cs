﻿using QuanLyCuaHangDienThoai.Models;
using QuanLyCuaHangDienThoai.Repository;

namespace QuanLyCuaHangDienThoai
{
    public class Store
    {
        private readonly IPhoneRepository _phoneRepository;
        private readonly IPurchaseRepository _purchaseRepository;
        private readonly ICustomerRepository _customerRepository;

        public Store(IPhoneRepository phoneRepository, IPurchaseRepository purchaseRepository, ICustomerRepository customerRepository)
        {
            _phoneRepository = phoneRepository;
            _purchaseRepository = purchaseRepository;
            _customerRepository = customerRepository;
        }

        public void AddPhone()
        {
            Console.Clear();

            Console.WriteLine("Input new phone information (Name, Brand, Stock Quantity, Price):");
            string[] phoneInfo = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);

            int stockQuantity = 0;
            int price = 0;

            if (phoneInfo.Length < 3)
            {
                throw new Exception(NotificationConstant.AllInfoMustBeFilled);
            }

            if (!int.TryParse(phoneInfo[2], out stockQuantity))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            if (!int.TryParse(phoneInfo[3], out price))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            MobilePhone newPhone = new MobilePhone(0, phoneInfo[0], phoneInfo[1], stockQuantity, price);

            try
            {
                _phoneRepository.AddPhone(newPhone);
                Console.WriteLine(NotificationConstant.Success);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeletePhone()
        {
            Console.Write("Input phone id that you want to delete: ");
            string input = Console.ReadLine();
            int phoneId = 0;

            if (!int.TryParse(input, out phoneId))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            try
            {
                _phoneRepository.DeletePhone(phoneId);
                Console.WriteLine(NotificationConstant.Success);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void PrintListPhones()
        {
            Console.Clear();

            List<MobilePhone> mobilePhones = new List<MobilePhone>();

            try
            {
                mobilePhones = _phoneRepository.PrintListPhones();

                foreach (MobilePhone phone in mobilePhones)
                {
                    Console.WriteLine(phone);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void BuyPhone()
        {
            List<MobilePhone> purchasePhones = new List<MobilePhone>();

            Console.WriteLine("Input phones' id that customer want to buy (1, 2, 3, 4, 5):");
            string[] inputPhonesId = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);

            for (int i = 0; i < inputPhonesId.Length; i++)
            {
                Console.Write($"Input quantity of phone id {inputPhonesId[i]} that customer want to buy: ");
                string inputQuantity = Console.ReadLine();
                int quantity = 0;
                int phoneId = 0;

                if (!int.TryParse(inputPhonesId[i], out phoneId))
                {
                    throw new Exception(NotificationConstant.FieldMustBeANumber);
                }

                if (!int.TryParse(inputQuantity, out quantity))
                {
                    throw new Exception(NotificationConstant.FieldMustBeANumber);
                }

                MobilePhone purchasePhone = _phoneRepository.FindPhoneById(phoneId);

                if (purchasePhone.StockQuantity < quantity)
                {
                    throw new Exception($"{purchasePhone} is not available");
                }

                purchasePhone.StockQuantity -= quantity;

                purchasePhones.Add(purchasePhone);
            }

            Console.WriteLine("Input customer phone number:");
            string inputPhoneNumber = Console.ReadLine();
            long phoneNumber = 0;

            if (inputPhoneNumber.Length < 10)
            {
                throw new Exception("Phone number must be 10 digits");
            }

            if (!long.TryParse(inputPhoneNumber, out phoneNumber))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            Customer customer = new Customer();

            try
            {
                customer = _customerRepository.FindCustomerByNumberPhone(phoneNumber);
            }
            catch (Exception)
            {
                throw;
            }

            if (customer is null)
            {
                //If this customer does not exist in store then create new customer

                Console.WriteLine("This is a new customer, you need to input customer information");
                Console.WriteLine("Input customer information (Name, Address):");
                string[] customerInfo = Console.ReadLine().Split(',', StringSplitOptions.TrimEntries);

                if (customerInfo.Length < 0)
                {
                    throw new Exception(NotificationConstant.AllInfoMustBeFilled);
                }

                Customer newCustomer = new Customer(0, customerInfo[0], customerInfo[1], phoneNumber);

                try
                {
                    _customerRepository.AddCustomer(newCustomer);
                    Purchase purchase = new Purchase(0, newCustomer.Id, purchasePhones);
                    _purchaseRepository.AddPurchase(purchase);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                try
                {
                    Purchase purchase = new Purchase(0, customer.Id, purchasePhones);
                    _purchaseRepository.AddPurchase(purchase);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            Console.WriteLine(NotificationConstant.Success);
        }

        public void PrintListPurchase()
        {
            Console.Clear();

            List<Purchase> purchaseList;

            try
            {
                purchaseList = _purchaseRepository.PrintListPurchase();

                foreach (Purchase purchase in purchaseList)
                {
                    Console.WriteLine(purchase);
                    Console.WriteLine("Phones have been sold in this purchase: ");
                    foreach (MobilePhone phone in purchase.PurchasePhones)
                    {
                        Console.WriteLine(phone);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void FindPhoneById()
        {
            Console.Clear();

            Console.Write("Input phone id that you want to see: ");
            string input = Console.ReadLine();
            int phoneId = 0;

            if (!int.TryParse(input, out phoneId))
            {
                throw new Exception(NotificationConstant.FieldMustBeANumber);
            }

            try
            {
                MobilePhone phone = _phoneRepository.FindPhoneById(phoneId);
                Console.WriteLine(phone);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
