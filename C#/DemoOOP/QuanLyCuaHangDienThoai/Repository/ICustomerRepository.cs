﻿using QuanLyCuaHangDienThoai.Models;

namespace QuanLyCuaHangDienThoai.Repository
{
    public interface ICustomerRepository
    {
        public void AddCustomer(Customer customer);
        public Customer FindCustomerByNumberPhone(long numberPhone);
    }
}
