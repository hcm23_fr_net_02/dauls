﻿using QuanLyCuaHangDienThoai.Models;

namespace QuanLyCuaHangDienThoai.Repository
{
    public interface IPurchaseRepository
    {
        public void AddPurchase(Purchase purchase);
        public List<Purchase> PrintListPurchase();
    }
}
