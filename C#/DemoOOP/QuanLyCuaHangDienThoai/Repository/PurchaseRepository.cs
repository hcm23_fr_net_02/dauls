﻿using QuanLyCuaHangDienThoai.Models;


namespace QuanLyCuaHangDienThoai.Repository
{
    public class PurchaseRepository : IPurchaseRepository
    {
        private readonly DbContext _dbContext;

        public PurchaseRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Purchase> PrintListPurchase()
        {
            if (_dbContext.Purhcase.Count == 0)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return _dbContext.Purhcase;
        }

        public void AddPurchase(Purchase purchase)
        {
            if (_dbContext.Purhcase.Count != 0)
            {
                int latestId = _dbContext.Purhcase[_dbContext.Purhcase.Count - 1].Id;
                purchase.Id = latestId + 1;
            }

            _dbContext.Purhcase.Add(purchase);
        }
    }
}
