﻿using QuanLyCuaHangDienThoai.Models;

namespace QuanLyCuaHangDienThoai.Repository
{
    public class PhoneRepository : IPhoneRepository
    {
        private readonly DbContext _dbContext;

        public PhoneRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void DeletePhone(int phoneId)
        {
            if (phoneId < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            MobilePhone phone = _dbContext.MobilePhone.SingleOrDefault(x => x.Id == phoneId);

            if (phone is null)
            {
                throw new Exception(NotificationConstant.ObjectDoesNotExist);
            }

            _dbContext.MobilePhone.Remove(phone);
        }

        public MobilePhone FindPhoneById(int phoneId)
        {
            if (phoneId < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            MobilePhone phone = _dbContext.MobilePhone.SingleOrDefault(x => x.Id == phoneId);

            if (phone is null)
            {
                throw new Exception(NotificationConstant.ObjectDoesNotExist);
            }

            return phone;
        }

        public List<MobilePhone> PrintListPhones()
        {
            if (_dbContext.MobilePhone.Count == 0)
            {
                throw new Exception(NotificationConstant.HaveNoObject);
            }

            return _dbContext.MobilePhone;
        }

        public void AddPhone(MobilePhone phone)
        {
            if (_dbContext.MobilePhone.Count != 0)
            {
                int latestId = _dbContext.MobilePhone[_dbContext.MobilePhone.Count - 1].Id;
                phone.Id = latestId + 1;
            }

            _dbContext.MobilePhone.Add(phone);
        }
    }
}
