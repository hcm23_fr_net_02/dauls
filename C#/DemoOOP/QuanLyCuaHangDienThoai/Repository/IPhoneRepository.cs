﻿using QuanLyCuaHangDienThoai.Models;

namespace QuanLyCuaHangDienThoai.Repository
{
    public interface IPhoneRepository
    {
        public void AddPhone(MobilePhone phone);
        public void DeletePhone(int phoneId);
        public List<MobilePhone> PrintListPhones();
        public MobilePhone FindPhoneById(int phoneId);
    }
}
