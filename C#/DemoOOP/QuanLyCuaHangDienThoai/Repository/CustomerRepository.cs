﻿using QuanLyCuaHangDienThoai.Models;

namespace QuanLyCuaHangDienThoai.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DbContext _dbContext;

        public CustomerRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Customer FindCustomerByNumberPhone(long phoneNumber)
        {
            if (phoneNumber < 0)
            {
                throw new Exception(NotificationConstant.NumberMustGreaterThanOrEqualZero);
            }

            Customer customer = _dbContext.Customer.SingleOrDefault(x => x.PhoneNumber == phoneNumber);

            return customer;
        }

        public void AddCustomer(Customer customer)
        {
            if (_dbContext.Customer.Count != 0)
            {
                int latestId = _dbContext.Customer[_dbContext.Customer.Count - 1].Id;
                customer.Id = latestId + 1;
            }

            _dbContext.Customer.Add(customer);
        }
    }
}
