﻿using QuanLyCuaHangDienThoai.Models;

namespace QuanLyCuaHangDienThoai
{
    public class DbContext
    {
        public List<MobilePhone> MobilePhone { get; set; }
        public List<Purchase> Purhcase { get; set; }
        public List<Customer> Customer { get; set; }

        public DbContext()
        {
            MobilePhone = new List<MobilePhone>()
            {
                new MobilePhone(0, "iPhone 11", "Apple", 10, 12000000),
                new MobilePhone(1, "Samsung Galaxy A05", "Samsung", 10, 3090000),
                new MobilePhone(2, "Samsung Galaxy S23 FE 5G", "Samsung", 10, 14490000),
                new MobilePhone(3, "iPhone 12", "Apple", 10, 14690000),
                new MobilePhone(4, "Xiaomi Redmi 12", "Xiaomi", 10, 3440000),
                new MobilePhone(5, "Xiaomi 13T Ori 5G", "Xiaomi", 10, 14990000),
            };

            Purhcase = new List<Purchase>();

            Customer = new List<Customer>();
        }
    }
}
