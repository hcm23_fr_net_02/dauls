﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Text.Json;

namespace BookStore.Api.Middlewares
{
    public class GlobalErrorHandlingMiddleware : IMiddleware
    {
        private readonly ILogger<GlobalErrorHandlingMiddleware> _logger;

        public GlobalErrorHandlingMiddleware(ILogger<GlobalErrorHandlingMiddleware> logger)
        {
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);

                context.Response.ContentType = "application/json";

                context.Response.StatusCode = e switch
                {
                    _ => (int)HttpStatusCode.InternalServerError
                };

                ProblemDetails problem = new ProblemDetails() {
                    Status = context.Response.StatusCode,
                    Title = e?.Message
                };

                string json = JsonSerializer.Serialize(problem);

                await context.Response.WriteAsync(json);
            }
        }
    }
}