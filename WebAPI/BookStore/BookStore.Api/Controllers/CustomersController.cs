﻿using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IReviewService _reviewService;
        private readonly ICustomerService _customerService;

        public CustomersController(
            IOrderService orderService,
            IReviewService reviewService,
            ICustomerService customerService)
        {
            _orderService = orderService;
            _reviewService = reviewService;
            _customerService = customerService;
        }

        [Authorize()]
        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetById([FromRoute] Guid id)
        {
            return Ok(await _customerService.GetByIdAsync(id));
        }

        [Authorize(Roles = IdentityRoles.User)]
        [HttpGet("{id:guid}/orders")]
        public async Task<IActionResult> GetOrdersByCustomerId(
            [FromRoute] Guid id,
            [FromQuery] PaginationDTO paginationDTO)
        {
            return Ok(await _orderService.GetByCustomerIdAsync(id, paginationDTO));
        }

        [Authorize(Roles = IdentityRoles.User)]
        [HttpGet("{id:guid}/purchased-books")]
        public async Task<IActionResult> GetPurchasedBooksByCustomerId(
            [FromRoute] Guid id,
            [FromQuery] PaginationDTO paginationDTO)
        {
            return Ok(await _orderService.GetPurchasedBooksByCustomerIdAsync(id, paginationDTO));
        }

        [Authorize(Roles = IdentityRoles.User)]
        [HttpGet("{id:guid}/reviews")]
        public async Task<IActionResult> GetReviewsByCustomerId(
            [FromRoute] Guid id,
            [FromQuery] PaginationDTO paginationDTO)
        {
            return Ok(await _reviewService.GetByCustomerIdAsync(id, paginationDTO));
        }
    }
}
