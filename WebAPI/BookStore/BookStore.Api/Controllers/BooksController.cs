﻿using BookStore.Application.DTOs.BookDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookService _bookService;

        public BooksController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [AllowAnonymous]
        [HttpGet()]
        public async Task<IActionResult> GetBooks([FromQuery] PaginationDTO paginationDTO)
        {
            return Ok(await _bookService.GetAllAsync(paginationDTO));
        }

        [AllowAnonymous]
        [HttpGet("filter-books")]
        public async Task<IActionResult> GetFilterBooks([FromQuery] PaginationFilterBookDTO paginatedListFilterBookDTO)
        {
            return Ok(await _bookService.FilterBooksAsync(paginatedListFilterBookDTO));
        }

        [Authorize(Roles = IdentityRoles.Admin)]
        [HttpPost]
        public async Task<IActionResult> CreateBook([FromBody] BookForUpsertDTO bookForUpsertDTO)
        {
            await _bookService.CreateBookAsync(bookForUpsertDTO);

            return Ok();
        }

        [Authorize(Roles = IdentityRoles.Admin)]
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateBook([FromRoute]Guid id, [FromBody] BookForUpsertDTO bookForUpsertDTO)
        {
            await _bookService.UpdateBookAsync(id, bookForUpsertDTO);

            return Ok();
        }

        [Authorize(Roles = IdentityRoles.Admin)]
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteBook([FromRoute] Guid id)
        {
            await _bookService.DeleteBookAsync(id);

            return Ok();
        }
    }
}