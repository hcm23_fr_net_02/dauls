﻿using BookStore.Application.DTOs.OrderDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrdersController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [Authorize(Roles = IdentityRoles.User)]
        [HttpPost]
        public async Task<IActionResult> CreateOrder([FromBody] OrderForInsertDTO orderForInsertDTO)
        {
            await _orderService.CreateOrderAsync(orderForInsertDTO);

            return Ok();
        }

        [Authorize(Roles = IdentityRoles.User)]
        [HttpPut("check-out/{id:guid}")]
        public async Task<IActionResult> CheckOutOrder([FromRoute] Guid id)
        {
            await _orderService.CheckOutOrderAsync(id);

            return Ok();
        }

        [Authorize(Roles = IdentityRoles.User)]
        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetById([FromRoute] Guid id)
        {
            return Ok(await _orderService.GetByIdAsync(id));
        }

        [Authorize(Roles = IdentityRoles.User)]
        [HttpGet("{id:guid}/order-details")]
        public async Task<IActionResult> GetOrderDetailsByOrderIdAsync(
            [FromRoute] Guid id,
            [FromQuery] PaginationDTO paginationDTO)
        {
            return Ok(await _orderService.GetOrderDetailsByOrderIdAsync(id, paginationDTO));
        }
    }
}
