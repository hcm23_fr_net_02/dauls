﻿using BookStore.Application.DTOs.ReviewDTOs;
using BookStore.Application.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewsController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [Authorize(Roles = IdentityRoles.User)]
        [HttpPost]
        public async Task<IActionResult> CreateReview([FromBody] ReviewForInsertDTO reviewForInsertDTO)
        {
            await _reviewService.CreateReview(reviewForInsertDTO);

            return Ok();
        }
    }
}
