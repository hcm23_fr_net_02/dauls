﻿using BookStore.Application.DTOs.CustomerDTOs;
using BookStore.Application.DTOs.OtpCodeDTOs;
using BookStore.Application.DTOs.UserDTOs;
using BookStore.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("signin")]
        public async Task<IActionResult> SignIn([FromBody] UserForRequestDTO userForRequestDTO)
        {
            return Ok(await _userService.SignInAsync(userForRequestDTO));
        }

        [HttpPost("signup")]
        public async Task<IActionResult> SignUp([FromBody] CustomerForInsertDTO customerForInsertDTO)
        {
            await _userService.SignUpAsync(customerForInsertDTO);

            return Ok();
        }

        [HttpPost("forgot-password")]
        public async Task<IActionResult> ForgotPassword([FromQuery] string email)
        {
            await _userService.ForGotPasswordAsync(email);

            return Ok();
        }

        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody] OtpCodeForRequest otpCodeForRequest)
        {
            await _userService.ResetPasswordAsync(otpCodeForRequest);

            return Ok();
        }
    }
}
