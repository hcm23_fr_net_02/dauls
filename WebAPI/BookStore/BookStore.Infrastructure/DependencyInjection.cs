﻿using BookStore.Infrastructure.Persistence.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Persistence.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using BookStore.Application.Interfaces;
using BookStore.Infrastructure.Authentication;
using BookStore.Infrastructure.Authentication.OptionsSetup;
using BookStore.Infrastructure.Identity;
using BookStore.Infrastructure.Services.Email.OptionsSetup;
using BookStore.Infrastructure.Services.Email;
using BookStore.Infrastructure.Services.FileUpload;
using BookStore.Infrastructure.Services.Report;
using BookStore.Infrastructure.Services.Report.OptionsSetup;
using Hangfire;
using Hangfire.MemoryStorage;

namespace BookStore.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, ConfigurationManager configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(auth =>
            {
                auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer();

            services.AddHangfire(config => config.UseMemoryStorage());
            services.AddHangfireServer();

            services.ConfigureOptions<JwtSettingsOptionsSetup>();
            services.ConfigureOptions<JwtBearerOptionsSetup>();
            services.ConfigureOptions<EmailSettingsOptionsSetup>();
            services.ConfigureOptions<ReportOptionsSetup>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IJwtProvider, JwtProvider>();
            services.AddScoped<IUserService, UserService>();
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddScoped<ICloudinaryService, CloudinaryService>();
            services.AddScoped<IReportGenerator, ReportGenerator>();

            services.AddCloudinary(configuration);

            return services;
        }
    }
}