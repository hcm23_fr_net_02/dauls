﻿using BookStore.Application.DTOs.CustomerDTOs;
using BookStore.Application.DTOs.OtpCodeDTOs;
using BookStore.Application.DTOs.UserDTOs;
using BookStore.Application.Interfaces;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using BookStore.Infrastructure.Persistence.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Storage;
using System.Security.Authentication;
using System.Text;

namespace BookStore.Infrastructure.Identity
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _context;
        private readonly IEmailSender _emailSender;
        private readonly IJwtProvider _jwtProvider;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            IEmailSender emailSender,
            IJwtProvider jwtProvider,
            IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _emailSender = emailSender;
            _jwtProvider = jwtProvider;
            _unitOfWork = unitOfWork;
        }

        public async Task<string> SignInAsync(UserForRequestDTO userForRequestDTO)
        {
            var resultSignIn = await _signInManager.PasswordSignInAsync(userForRequestDTO.Email, userForRequestDTO.Password, false, false);

            if (!resultSignIn.Succeeded)
            {
                throw new InvalidCredentialException(ErrorMessages.InvalidCredentials);
            }

            var user = await _userManager.FindByEmailAsync(userForRequestDTO.Email);

            if (user == null)
            {
                throw new Exception(ErrorMessages.CannotFindAppUser);
            }

            var role = await _userManager.GetRolesAsync(user);

            if (role.Count < 1)
            {
                throw new Exception(ErrorMessages.CannotFindRole);
            }

            string name = "Admin";

            if (role.Contains(IdentityRoles.User))
            {
                var customer = await _unitOfWork.CustomerRepository.GetByIdAsync(new Guid(user.Id));
                name = customer!.Name;
            }

            var userForReponseDTO = new UserForReponseDTO()
            {
                Id = new Guid(user.Id),
                Name = name,
                Email = user.Email!,
                Role = role.First()
            };

            return _jwtProvider.GenerateToken(userForReponseDTO);
        }

        public async Task SignUpAsync(CustomerForInsertDTO customerForInsertDTO)
        {
            using (IDbContextTransaction transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var user = await _userManager.FindByEmailAsync(customerForInsertDTO.Email);

                    if (user != null)
                    {
                        throw new Exception(ErrorMessages.EmailAlreadyExisted);
                    }

                    var newUser = new ApplicationUser()
                    {
                        Email = customerForInsertDTO.Email,
                        UserName = customerForInsertDTO.Email
                    };

                    var resultCreateUser = await _userManager.CreateAsync(newUser, customerForInsertDTO.Password);

                    if (!resultCreateUser.Succeeded)
                    {
                        throw new Exception(ErrorMessages.CannotCreateAppUser);
                    }

                    var userFindByEmail = await _userManager.FindByEmailAsync(newUser.Email);

                    if (userFindByEmail == null)
                    {
                        throw new Exception(ErrorMessages.CannotFindAppUser);
                    }

                    var resultAddRole = await _userManager.AddToRoleAsync(userFindByEmail, IdentityRoles.User);

                    if (!resultAddRole.Succeeded)
                    {
                        throw new Exception(ErrorMessages.CannotAddRole);
                    }

                    var accountBalance = new AccountBalance(1000000);
                    var customer = new Customer(new Guid(userFindByEmail.Id), accountBalance.Id, customerForInsertDTO.Name, DateTime.UtcNow, newUser.Email);
                    customer.AccountBalance = accountBalance;

                    await _unitOfWork.CustomerRepository.AddAsync(customer);
                    await _unitOfWork.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync();

                    throw;
                }
            }
        }

        public async Task ForGotPasswordAsync(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            if (user == null)
            {
                throw new Exception(ErrorMessages.CannotFindAppUser);
            }

            DateTime expirationDate = DateTime.UtcNow.AddMinutes(5);

            OtpCode otpCode = new OtpCode(email, expirationDate);

            bool resultExist;

            do
            {
                otpCode.RandomCode();
                resultExist = await _unitOfWork.OtpCodeRepository.CheckCodeExistsAsync(otpCode.Code);
            } while (resultExist);

            await _unitOfWork.OtpCodeRepository.AddAsync(otpCode);

            string[] emails = new string[]{
                email
            };
            StringBuilder body = new StringBuilder();
            body.Append(EmailConstants.SendOTPBody);
            body.Append(otpCode.Code);

            await _emailSender.SendMailAsync(EmailConstants.SendOTPSubject, body, emails);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task ResetPasswordAsync(OtpCodeForRequest otpCodeForRequest)
        {
            var otpCode = await _unitOfWork.OtpCodeRepository.GetByCodeAndEmail(otpCodeForRequest.Code, otpCodeForRequest.Email);

            if (DateTime.UtcNow > otpCode!.ExpirationDate)
            {
                _unitOfWork.OtpCodeRepository.Remove(otpCode);
                await _unitOfWork.SaveChangesAsync();
                throw new Exception(ErrorMessages.OtpCodeExpired);
            }

            var user = await _userManager.FindByEmailAsync(otpCodeForRequest.Email);

            if (user == null)
            {
                throw new Exception(ErrorMessages.CannotFindAppUser);
            }

            await _userManager.RemovePasswordAsync(user);
            await _userManager.AddPasswordAsync(user, otpCodeForRequest.Password);
            _unitOfWork.OtpCodeRepository.Remove(otpCode);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}