﻿namespace BookStore.Infrastructure.Commons.Constants
{
    public static class IdentityRoles
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}