﻿namespace BookStore.Infrastructure.Commons.Constants
{
    public static class EmailConstants
    {
        public const string SendOTPSubject = "Reset password OTP code";
        public const string SendOTPBody = "This is your OTP code: ";
        public const string SendReportSubject = "Daily Report - Date: ";
        public const string SendReportBody = "This is report file url: ";
    }
}
