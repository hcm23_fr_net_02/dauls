﻿namespace BookStore.Infrastructure.Commons.Constants
{
    public static class ErrorMessages
    {
        public const string IdDoesNotExist = "This id does not exist";
        public const string EmptyPage = "Empty page";
        public const string CannotCreateAppUser = "Cannot create user";
        public const string CannotCreateRole = "Cannot create role";
        public const string CannotAddRole = "Cannot add role";
        public const string CannotAddPassword = "Cannot add password";
        public const string CannotFindAppUser = "Cannot find user";
        public const string CannotFindRole = "Cannot find role";
        public const string CannotFindCustomer = "Cannot find customer";
        public const string InvalidCredentials = "Invalid credentials";
        public const string EmailAlreadyExisted = "This email is already existed";
        public const string InvalidOtpCode = "Invalid otp code";
        public const string OtpCodeExpired = "Otp code has been expired";
    }
}