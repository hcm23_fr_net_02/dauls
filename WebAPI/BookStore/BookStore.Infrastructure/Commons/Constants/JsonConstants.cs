﻿namespace BookStore.Infrastructure.Commons.Constants
{
    public static class JsonConstants
    {
        public const string Folder = "JsonFiles";
        public const string FilePathNotFound = "File path not found: ";
        public const string BooksFileName = "Books.json";
    }
}