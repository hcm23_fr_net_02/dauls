﻿using BookStore.Application.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using BookStore.Infrastructure.Services.Report;
using Hangfire;
using Microsoft.Extensions.Options;
using System.Text;

namespace BookStore.Infrastructure.BackgroundJobs
{
    public class ReportJob
    {
        private readonly ICloudinaryService _cloudinaryService;
        private readonly IEmailSender _emailSender;
        private readonly IReportGenerator _reportGenerator;
        private readonly ReportSettings _reportSettings;

        public ReportJob(
            ICloudinaryService cloudinaryService,
            IEmailSender emailSender,
            IReportGenerator reportGenerator,
            IOptions<ReportSettings> reportSettings)
        {
            _cloudinaryService = cloudinaryService;
            _emailSender = emailSender;
            _reportGenerator = reportGenerator;
            _reportSettings = reportSettings.Value;
        }

        public async Task ProcessReportJobAsync()
        {
            var fileName = string.Concat(DateTime.UtcNow.ToString("MM-dd-yyyy"), " - Report.pdf");
            var pdfBytes = await _reportGenerator.GenerateReportAsync();
            var uploadUrl = _cloudinaryService.UploadFile(pdfBytes, fileName);

            var subject = EmailConstants.SendReportSubject + DateTime.UtcNow.ToString("MM-dd-yyyy");
            var body = new StringBuilder();
            body.Append(EmailConstants.SendReportBody + uploadUrl);

            var emailsTo = new string[]{
                _reportSettings.Email
            };

            await _emailSender.SendMailAsync(subject, body, emailsTo, new KeyValuePair<string, byte[]>(fileName, pdfBytes));
        }

        public void ExecuteAsync()
        {
            BackgroundJob.Enqueue<ReportJob>(job => job.ProcessReportJobAsync());
        }
    }
}