﻿using BookStore.Application.Interfaces;
using Microsoft.Extensions.Options;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace BookStore.Infrastructure.Services.Email
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailSettings _emailSettings;

        public EmailSender(IOptions<EmailSettings> emailSettings)
        {
            _emailSettings = emailSettings.Value;
        }

        public async Task SendMailAsync(
            string subject,
            StringBuilder body,
            string[] emailsTo)
        {
            var smtpClient = new SmtpClient
            {
                Port = _emailSettings.Port,
                EnableSsl = _emailSettings.EnableSsl,
                Host = _emailSettings.Host,
                UseDefaultCredentials = _emailSettings.UseDefaultCredentials,
                Credentials = new NetworkCredential(_emailSettings.EmailForSend, _emailSettings.AppPassword)
            };

            var message = new MailMessage()
            {
                Subject = subject,
                Body = body.ToString(),
                From = new MailAddress(_emailSettings.EmailForSend)
            };

            foreach (var email in emailsTo)
            {
                message.To.Add(new MailAddress(email));
            }

            await smtpClient.SendMailAsync(message);
        
        
        }

        public async Task SendMailAsync(
            string subject,
            StringBuilder body,
            string[] emailsTo,
            KeyValuePair<string, byte[]> file)
        {
            var smtpClient = new SmtpClient
            {
                Port = _emailSettings.Port,
                EnableSsl = _emailSettings.EnableSsl,
                Host = _emailSettings.Host,
                UseDefaultCredentials = _emailSettings.UseDefaultCredentials,
                Credentials = new NetworkCredential(_emailSettings.EmailForSend, _emailSettings.AppPassword)
            };

            var message = new MailMessage()
            {
                Subject = subject,
                Body = body.ToString(),
                From = new MailAddress(_emailSettings.EmailForSend)
            };

            var stream = new MemoryStream(file.Value);

            Attachment attachment = new Attachment(stream, file.Key, MediaTypeNames.Application.Octet);

            message.Attachments.Add(attachment);

            foreach (var email in emailsTo)
            {
                message.To.Add(new MailAddress(email));
            }

            await smtpClient.SendMailAsync(message);
        }
    }
}