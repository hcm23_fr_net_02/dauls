﻿using BookStore.Infrastructure.Services.Email;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace BookStore.Infrastructure.Services.Email.OptionsSetup
{
    public class EmailSettingsOptionsSetup : IConfigureOptions<EmailSettings>
    {
        private readonly IConfiguration _configuration;

        public EmailSettingsOptionsSetup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Configure(EmailSettings options)
        {
            _configuration.GetSection(EmailSettings.SectionName).Bind(options);
        }
    }
}