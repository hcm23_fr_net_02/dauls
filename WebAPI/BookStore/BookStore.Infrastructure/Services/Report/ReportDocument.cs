﻿using QuestPDF.Fluent;
using QuestPDF.Helpers;
using QuestPDF.Infrastructure;

namespace BookStore.Infrastructure.Services.Report
{
    public class ReportDocument : IDocument
    {
        public ReportModel Model { get; }

        public ReportDocument(ReportModel model)
        {
            Model = model;
        }

        public DocumentMetadata GetMetadata() => DocumentMetadata.Default;

        public DocumentSettings GetSettings() => DocumentSettings.Default;

        public void Compose(IDocumentContainer container)
        {
            container
                .Page(page =>
                {
                    page.Margin(50);

                    page.Header().Element(ComposeHeader);
                    page.Content().Element(ComposeContent);


                    page.Footer().AlignCenter().Text(x =>
                    {
                        x.CurrentPageNumber();
                        x.Span(" / ");
                        x.TotalPages();
                    });
                });
        }

        void ComposeHeader(IContainer container)
        {
            var titleStyle = TextStyle.Default.FontSize(20).SemiBold().FontColor(Colors.Blue.Medium);

            container.Row(row =>
            {
                row.RelativeItem().Column(column =>
                {
                    column.Item().Text("Book Store Daily Report").Style(titleStyle);

                    column.Item().Text(text =>
                    {
                        text.Span("Date: ").SemiBold();
                        text.Span($"{DateTime.UtcNow:d}");
                    });
                });

                row.ConstantItem(100).Height(50).Placeholder();
            });
        }

        void ComposeContent(IContainer container)
        {
            container
                .PaddingVertical(40)
                .Height(250)
                .Background(Colors.Grey.Lighten3)
                .AlignCenter()
                .AlignMiddle()
                .Column(column =>
                {
                    column.Item()
                    .AlignLeft()
                    .Text($"Total orders that have been placed today: {Model.CountOrder}")
                    .FontSize(15);

                    column.Item()
                    .AlignLeft()
                    .Text($"Total orders that have been checked out today: {Model.CountPurchasedOrder}")
                    .FontSize(15);

                    column.Item()
                    .AlignLeft()
                    .Text($"Revenue today: {Model.Revenue}")
                    .FontSize(15);

                    column.Item()
                    .AlignLeft()
                    .Text($"Total new users account that have been signed up today: {Model.CountNewUsers}")
                    .FontSize(15);
                });
        }
    }
}