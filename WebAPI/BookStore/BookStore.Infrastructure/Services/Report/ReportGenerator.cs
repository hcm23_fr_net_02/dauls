﻿using BookStore.Application.Interfaces;
using BookStore.Domain.Interfaces;
using QuestPDF.Fluent;
using QuestPDF.Infrastructure;

namespace BookStore.Infrastructure.Services.Report
{
    public class ReportGenerator : IReportGenerator
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReportGenerator(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<byte[]> GenerateReportAsync()
        {
            QuestPDF.Settings.License = LicenseType.Community;

            var model = new ReportModel()
            {
                CountOrder = await _unitOfWork.OrderRepository.GetCountByOrderDateAsync(DateTime.UtcNow),
                CountPurchasedOrder = await _unitOfWork.OrderRepository.GetCountByPurchaseDateAsync(DateTime.UtcNow),
                Revenue = await _unitOfWork.OrderRepository.GetRevenueByPurchaseDateAsync(DateTime.UtcNow),
                CountNewUsers = await _unitOfWork.CustomerRepository.GetCountByCreatedDate(DateTime.UtcNow)
            };

            var document = new ReportDocument(model);
            return Document.Create(document.Compose).GeneratePdf();
        }
    }
}
