﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace BookStore.Infrastructure.Services.Report.OptionsSetup
{
    internal class ReportOptionsSetup : IConfigureOptions<ReportSettings>
    {
        private readonly IConfiguration _configuration;

        public ReportOptionsSetup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Configure(ReportSettings options)
        {
            _configuration.GetSection(ReportSettings.SectionName).Bind(options);
        }
    }
}
