﻿using CloudinaryDotNet;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookStore.Infrastructure.Services.FileUpload
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCloudinary(
            this IServiceCollection services,
            ConfigurationManager configuration)
        {
            var options = configuration
                .GetSection(CloudinarySettings.SectionName)
                .Get<CloudinarySettings>();

            var account = new Account(
                options!.CloudName,
                options.ApiKey,
                options.ApiSecret);

            var cloudinary = new Cloudinary(account);

            services.AddSingleton(cloudinary);

            return services;
        }
    }
}