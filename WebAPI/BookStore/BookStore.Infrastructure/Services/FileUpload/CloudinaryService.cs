﻿using BookStore.Application.Interfaces;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;

namespace BookStore.Infrastructure.Services.FileUpload
{
    public class CloudinaryService : ICloudinaryService
    {
        private readonly Cloudinary _cloudinary;

        public CloudinaryService(Cloudinary cloudinary)
        {
            _cloudinary = cloudinary;
        }

        public string UploadFile(byte[] fileBytes, string fileName)
        {
            using var stream = new MemoryStream(fileBytes);

            var uploadParams = new ImageUploadParams()
            {
                File = new FileDescription(fileName, stream),
            };

            var uploadResult = _cloudinary.Upload(uploadParams);

            return uploadResult.SecureUrl.ToString();
        }
    }
}
