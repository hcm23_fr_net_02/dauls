﻿using BookStore.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookStore.Infrastructure.Persistence.Configurations
{
    public class AccountBalanceConfiguration : IEntityTypeConfiguration<AccountBalance>
    {
        public void Configure(EntityTypeBuilder<AccountBalance> builder)
        {
            builder.HasKey(a => a.Id);

            builder.Property(a => a.Balance)
                .HasColumnType("decimal(18, 2)");

            builder.HasQueryFilter(a => a.IsDelete == false);
        }
    }
}
