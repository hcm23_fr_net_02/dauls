﻿using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using BookStore.Infrastructure.Persistence.Data;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class FavoriteBookRepository : Repository<FavoriteBook>, IFavoriteBookRepository
    {
        public FavoriteBookRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<PaginatedList<FavoriteBook>> GetByCustomerIdAsync(
            Guid customerId,
            int pageNumber,
            int pageSize)
        {
            IQueryable<FavoriteBook> query = _context.FavoriteBooks
                 .Where(f => f.CustomerId == customerId)
                 .Include(f => f.Book);

            int count = await query.CountAsync();

            List<FavoriteBook> favoriteBooks = await query
                .OrderBy(b => b.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            if (favoriteBooks.Count < 1)
            {
                throw new Exception(ErrorMessages.EmptyPage);
            }

            return new PaginatedList<FavoriteBook>(
                favoriteBooks,
                count,
                pageNumber,
                pageSize);
        }
    }
}