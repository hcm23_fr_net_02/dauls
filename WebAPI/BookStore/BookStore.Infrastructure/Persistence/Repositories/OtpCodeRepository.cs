﻿using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using BookStore.Infrastructure.Persistence.Data;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class OtpCodeRepository : Repository<OtpCode>, IOtpCodeRepository
    {
        public OtpCodeRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<OtpCode?> GetByCodeAndEmail(
            string code,
            string email)
        {
            var otpCode = await _context.OtpCodes
                .FirstOrDefaultAsync(o => o.Code.ToLower().Trim() == code.ToLower().Trim()
            && o.Email.ToLower().Trim() == email.ToLower().Trim());

            if (otpCode == null)
            {
                throw new Exception(ErrorMessages.InvalidOtpCode);
            }

            return otpCode;
        }

        public async Task<bool> CheckCodeExistsAsync(string code)
        {
            return await _context.OtpCodes.AnyAsync(o => o.Code.ToLower().Trim() == code.ToLower().Trim());
        }

        public void Remove(OtpCode otpCode)
        {
            _context.OtpCodes.Remove(otpCode);
        }
    }
}