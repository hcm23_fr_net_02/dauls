﻿using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Persistence.Data;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class AccountBalanceRepository : Repository<AccountBalance>, IAccountBalanceRepository
    {
        public AccountBalanceRepository(ApplicationDbContext context) : base(context)
        {
            
        }
    }
}