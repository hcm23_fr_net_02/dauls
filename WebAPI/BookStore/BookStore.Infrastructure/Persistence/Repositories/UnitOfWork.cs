﻿using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Persistence.Data;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private IAccountBalanceRepository _accountBalanceRepository = default!;
        private IBookRepository _bookRepository = default!;
        private ICustomerRepository _customerRepository = default!;
        private IFavoriteBookRepository _favoriteBookRepository = default!;
        private IOrderDetailRepository _orderDetailRepository = default!;
        private IOrderRepository _orderRepository = default!;
        private IReviewRepository _reviewRepository = default!;
        private IOtpCodeRepository _otpCodeRepository = default!;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        public IAccountBalanceRepository AccountBalanceRepository
        {
            get
            {
                _accountBalanceRepository ??= new AccountBalanceRepository(_context);
                return _accountBalanceRepository;
            }
        }

        public IBookRepository BookRepository
        {
            get
            {
                _bookRepository ??= new BookRepository(_context);
                return _bookRepository;
            }
        }

        public ICustomerRepository CustomerRepository
        {
            get
            {
                _customerRepository ??= new CustomerRepository(_context);
                return _customerRepository;
            }
        }

        public IFavoriteBookRepository FavoriteBookRepository
        {
            get
            {
                _favoriteBookRepository ??= new FavoriteBookRepository(_context);
                return _favoriteBookRepository;
            }
        }

        public IOrderDetailRepository OrderDetailRepository
        {
            get
            {
                _orderDetailRepository ??= new OrderDetailRepository(_context);
                return _orderDetailRepository;
            }
        }

        public IOrderRepository OrderRepository
        {
            get
            {
                _orderRepository ??= new OrderRepository(_context);
                return _orderRepository;
            }
        }

        public IReviewRepository ReviewRepository
        {
            get
            {
                _reviewRepository ??= new ReviewRepository(_context);
                return _reviewRepository;
            }
        }

        public IOtpCodeRepository OtpCodeRepository
        {
            get
            {
                _otpCodeRepository ??= new OtpCodeRepository(_context);
                return _otpCodeRepository;
            }
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}