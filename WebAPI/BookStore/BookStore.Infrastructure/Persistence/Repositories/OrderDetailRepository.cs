﻿using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using BookStore.Infrastructure.Persistence.Data;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class OrderDetailRepository : Repository<OrderDetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(ApplicationDbContext context) : base(context)
        {
            
        }

        public async Task<PaginatedList<OrderDetail>> GetByOrderIdAsync(
            Guid orderId,
            int pageNumber,
            int pageSize)
        {
            IQueryable<OrderDetail> query = _context.OrderDetails
                 .Where(od => od.OrderId == orderId)
                 .Include(od => od.Book);

            int count = await query.CountAsync();

            List<OrderDetail> orderDetails = await query
                .OrderBy(b => b.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            if (orderDetails.Count < 1)
            {
                throw new Exception(ErrorMessages.EmptyPage);
            }

            return new PaginatedList<OrderDetail>(
                orderDetails,
                count,
                pageNumber,
                pageSize);
        }
    }
}