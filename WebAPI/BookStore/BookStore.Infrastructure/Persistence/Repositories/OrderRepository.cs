﻿using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using BookStore.Infrastructure.Persistence.Data;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
            
        }

        public override async Task<Order?> GetByIdAsync(Guid id)
        {
            var order = await _context.Orders
                .Include(o => o.OrderDetails)
                .ThenInclude(od => od.Book)
                .Include(o => o.Customer)
                .ThenInclude(c => c!.AccountBalance)
                .SingleOrDefaultAsync(o => o.Id == id);

            if (order == null)
            {
                throw new ArgumentException(ErrorMessages.IdDoesNotExist);
            }

            return order;
        }

        public async Task<PaginatedList<Book>> GetPurchasedBooksByCustomerIdAsync(
            Guid customerId,
            int pageNumber,
            int pageSize)
        {
            IQueryable<Book?> query = _context.Orders
                 .Include(o => o.OrderDetails)
                 .ThenInclude(od => od.Book)
                 .Where(o => o.CustomerId == customerId && o.IsPurchase == true)
                 .SelectMany(o => o.OrderDetails)
                 .Select(od => od.Book)
                 .Distinct();

            int count = query.Count();

            List<Book?> books = await query
                .OrderBy(b => b!.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            if (books.Count < 1)
            {
                throw new Exception(ErrorMessages.EmptyPage);
            }

            return new PaginatedList<Book>(
                books!,
                count,
                pageNumber,
                pageSize);
        }

        public async Task<bool> CheckPurchasedBookExistByCustomerIdAsync(
            Guid customerId,
            Guid bookId)
        {
            IQueryable<Book?> query = _context.Orders
                 .Include(o => o.OrderDetails)
                 .ThenInclude(od => od.Book)
                 .Where(o => o.CustomerId == customerId && o.IsPurchase == true)
                 .SelectMany(o => o.OrderDetails)
                 .Select(od => od.Book)
                 .Distinct();

            return await query.AnyAsync(b => b!.Id == bookId);
        }


        public async Task<PaginatedList<Order>> GetByCustomerIdAsync(
            Guid customerId,
            int pageNumber,
            int pageSize)
        {
            IQueryable<Order> query = _context.Orders
                 .Where(o => o.CustomerId == customerId);

            int count = await query.CountAsync();

            List<Order> orders = await query
                .OrderBy(b => b.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            if (orders.Count < 1)
            {
                throw new Exception(ErrorMessages.EmptyPage);
            }

            return new PaginatedList<Order>(
                orders,
                count,
                pageNumber,
                pageSize);
        }

        public async Task<int> GetCountByOrderDateAsync(DateTime orderDate)
        {
            return await _context.Orders.CountAsync(o => o.OrderDate.Date == orderDate.Date);
        }

        public async Task<int> GetCountByPurchaseDateAsync(DateTime purchaseDate)
        {
            return await _context.Orders.CountAsync(o => o.PurchaseDate.Date == purchaseDate.Date && o.IsPurchase == true);
        }

        public async Task<decimal> GetRevenueByPurchaseDateAsync(DateTime purchaseDate)
        {
            var revenue = await _context.Orders
                .Where(o => o.PurchaseDate.Date == purchaseDate.Date && o.IsPurchase == true)
                .Select(o => o.Total)
                .SumAsync();

            return revenue;
        }
    }
}