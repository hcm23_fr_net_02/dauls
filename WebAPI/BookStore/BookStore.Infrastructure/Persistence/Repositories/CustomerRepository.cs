﻿using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using BookStore.Infrastructure.Persistence.Data;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(ApplicationDbContext context) : base(context)
        {
            
        }

        public override async Task<Customer?> GetByIdAsync(Guid id)
        {
            var customer = await _context.Customers
                .Include(c => c.AccountBalance)
                .SingleOrDefaultAsync(c => c.Id == id);

            if (customer == null)
            {
                throw new ArgumentException(ErrorMessages.IdDoesNotExist);
            }

            return customer;
        }

        public async Task<int> GetCountByCreatedDate(DateTime createdDate)
        {
            return await _context.Customers.CountAsync(c => c.CreatedDate.Date == createdDate.Date);
        }
    }
}