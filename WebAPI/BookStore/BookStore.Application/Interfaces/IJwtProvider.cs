﻿using BookStore.Application.DTOs.UserDTOs;

namespace BookStore.Application.Interfaces
{
    public interface IJwtProvider
    {
        string GenerateToken(UserForReponseDTO user);
    }
}