﻿namespace BookStore.Application.Interfaces
{
    public interface ICloudinaryService
    {
        string UploadFile(byte[] fileBytes, string fileName);
    }
}
