﻿using System.Text;

namespace BookStore.Application.Interfaces
{
    public interface IEmailSender
    {
        Task SendMailAsync(string subject, StringBuilder body, string[] emailsTo);
        Task SendMailAsync(string subject, StringBuilder body, string[] emailsTo, KeyValuePair<string, byte[]> file);
    }
}
