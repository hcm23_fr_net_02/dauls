﻿using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.DTOs.ReviewDTOs;
using BookStore.Domain.Commons;

namespace BookStore.Application.Interfaces
{
    public interface IReviewService
    {
        Task CreateReview(ReviewForInsertDTO reviewForInsertDTO);
        Task<PaginatedList<ReviewForGetDTO>> GetByCustomerIdAsync(Guid customerId, PaginationDTO paginationDTO);
    }
}