﻿using BookStore.Application.DTOs.BookDTOs;
using BookStore.Application.DTOs.OrderDetailDTOs;
using BookStore.Application.DTOs.OrderDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Domain.Commons;

namespace BookStore.Application.Interfaces
{
    public interface IOrderService
    {
        Task CreateOrderAsync(OrderForInsertDTO orderForInsertDTO);
        Task CheckOutOrderAsync(Guid id);
        Task<PaginatedList<OrderForGetDTO>> GetByCustomerIdAsync(Guid customerId, PaginationDTO paginationDTO);
        Task<PaginatedList<OrderDetailForGetDTO>> GetOrderDetailsByOrderIdAsync(Guid orderId, PaginationDTO paginationDTO);
        Task<OrderForGetDTO> GetByIdAsync(Guid id);
        Task<PaginatedList<BookForGetDTO>> GetPurchasedBooksByCustomerIdAsync(Guid customerId, PaginationDTO paginationDTO);
    }
}