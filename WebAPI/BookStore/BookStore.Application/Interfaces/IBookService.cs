﻿using BookStore.Application.DTOs.BookDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Domain.Commons;

namespace BookStore.Application.Interfaces
{
    public interface IBookService
    {
        Task CreateBookAsync(BookForUpsertDTO bookForUpsertDTO);
        Task UpdateBookAsync(Guid bookId, BookForUpsertDTO bookForUpsertDTO);
        Task DeleteBookAsync(Guid bookId);
        Task<PaginatedList<BookForGetDTO>> GetAllAsync(PaginationDTO paginatedListDTO);
        Task<PaginatedList<BookForGetDTO>> FilterBooksAsync(PaginationFilterBookDTO paginatedListFilterBookDTO);
    }
}