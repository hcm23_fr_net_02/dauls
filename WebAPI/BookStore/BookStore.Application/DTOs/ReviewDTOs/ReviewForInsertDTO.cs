﻿using BookStore.Domain.Commons.Enums;
using System.ComponentModel.DataAnnotations;

namespace BookStore.Application.DTOs.ReviewDTOs
{
    public class ReviewForInsertDTO
    {
        [Required]
        public Guid CustomerId { get; set; }

        [Required]
        public Guid BookId { get; set; }

        [Required]
        public string Comment { get; set; } = string.Empty;

        [Required]
        public RateEnum Rate { get; set; }

        [Required]
        public string ImgUrl { get; set; } = string.Empty;
    }
}