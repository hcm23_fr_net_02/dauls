﻿using BookStore.Application.DTOs.OrderDetailDTOs;
using System.ComponentModel.DataAnnotations;

namespace BookStore.Application.DTOs.OrderDTOs
{
    public class OrderForInsertDTO
    {
        [Required]
        public Guid CustomerId { get; set; }

        [Required]
        public ICollection<OrderDetailForInsertDTO> OrderDetails { get; set; } = new List<OrderDetailForInsertDTO>();
    }
}