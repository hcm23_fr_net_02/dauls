﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.Application.DTOs.FavoriteBookDTOs
{
    public class FavoriteBookForInsertDTO
    {
        [Required]
        public Guid CustomerId { get; set; }

        [Required]
        public Guid BookId { get; set; }
    }
}