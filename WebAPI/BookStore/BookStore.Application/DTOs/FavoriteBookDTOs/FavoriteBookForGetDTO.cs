﻿using BookStore.Application.DTOs.BookDTOs;

namespace BookStore.Application.DTOs.FavoriteBookDTOs
{
    public class FavoriteBookForGetDTO
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public Guid BookId { get; set; }
        public BookForGetDTO? Book { get; set; }
    }
}