﻿using BookStore.Application.Commons.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace BookStore.Application.DTOs.BookDTOs
{
    public class BookForUpsertDTO
    {
        [Required]
        public string Title { get; set; } = string.Empty;

        [Required]
        public string Author { get; set; } = string.Empty;

        [Required]
        public string Genre { get; set; } = string.Empty;

        [Required]
        [DateLessThanCurrentDate]
        public DateTime PublicationDate { get; set; }

        [Required]
        public string CoverImgUrl { get; set; } = string.Empty;

        [Required]
        public int Quantity { get; set; }

        [Required]
        [Range(1, (double)decimal.MaxValue)]
        public decimal UnitPrice { get; set; }
    }
}