﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.Application.DTOs.OrderDetailDTOs
{
    public class OrderDetailForInsertDTO
    {
        [Required]
        public Guid BookId { get; set; }

        [Required]
        [Range(1, (double)decimal.MaxValue)]
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
    }
}