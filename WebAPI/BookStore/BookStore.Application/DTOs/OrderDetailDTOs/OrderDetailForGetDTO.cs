﻿using BookStore.Application.DTOs.BookDTOs;

namespace BookStore.Application.DTOs.OrderDetailDTOs
{
    public class OrderDetailForGetDTO
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public Guid BookId { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public BookForGetDTO Book { get; set; } = default!;
    }
}