﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.Application.DTOs.OtpCodeDTOs
{
    public class OtpCodeForRequest
    {
        [Required]
        public string Code { get; set; } = string.Empty;

        [Required]
        [EmailAddress]
        public string Email { get; set; } = string.Empty;

        [Required]
        public string Password { get; set; } = string.Empty;
    }
}
