﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.Application.DTOs.PaginatedListDTO
{
    public class PaginationDTO
    {
        [Required]
        public int PageNumber { get; set; }

        [Required]
        public int PageSize { get; set; }
    }
}
