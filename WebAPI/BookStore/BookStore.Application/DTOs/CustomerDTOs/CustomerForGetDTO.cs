﻿using BookStore.Application.DTOs.AccountBalanceDTOs;

namespace BookStore.Application.DTOs.CustomerDTOs
{
    public class CustomerForGetDTO
    {
        public Guid Id { get; set; }
        public Guid AccountBalanceId { get; set; }
        public string Name { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public AccountBalanceForGetDTO? AccountBalance { get; set; }
    }
}