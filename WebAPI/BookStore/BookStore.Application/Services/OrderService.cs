﻿using BookStore.Application.Commons.Constants;
using BookStore.Application.DTOs.BookDTOs;
using BookStore.Application.DTOs.OrderDetailDTOs;
using BookStore.Application.DTOs.OrderDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.Interfaces;
using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using MapsterMapper;
using System.Text;

namespace BookStore.Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IMapper _mapper;
        private readonly IEmailSender _emailSender;
        private readonly IUnitOfWork _unitOfWork;

        public OrderService(IMapper mapper, IEmailSender emailSender,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _emailSender = emailSender;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateOrderAsync(OrderForInsertDTO orderForInsertDTO)
        {
            var customer = await _unitOfWork.CustomerRepository.GetByIdAsync(orderForInsertDTO.CustomerId);
            var order = new Order(orderForInsertDTO.CustomerId, DateTime.UtcNow);

            foreach (var item in orderForInsertDTO.OrderDetails)
            {
                //Update book quantity
                var book = await _unitOfWork.BookRepository.GetByIdAsync(item.BookId);

                if (book!.Quantity < item.Quantity)
                {
                    throw new Exception(ErrorMessages.InsufficientBookQuantity);
                }

                book.UpdateQuantityByOrder(item.Quantity);

                //Cauculate total of order
                order.CalculateTotal(item.UnitPrice, item.Quantity);

                var orderDetail = new OrderDetail(order.Id, book.Id, item.UnitPrice, item.Quantity);

                order.OrderDetails.Add(orderDetail);
            }

            if (customer!.AccountBalance!.Balance < order.Total)
            {
                throw new Exception(ErrorMessages.InsufficientBalance);
            }

            await _unitOfWork.OrderRepository.AddAsync(order);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task CheckOutOrderAsync(Guid orderId)
        {
            var order = await _unitOfWork.OrderRepository.GetByIdAsync(orderId);

            if (order!.IsPurchase)
            {
                throw new Exception(ErrorMessages.OrderAlreadyPurchased);
            }

            order!.Customer!.AccountBalance!.CheckOut(order.Total);
            order.CheckOut();

            string subject = EmailConstants.CheckOutOrderSubject;
            StringBuilder body = new StringBuilder();
            body.Append($"Customer: {order.Customer.Name} \n");
            body.Append($"Order date: {order.OrderDate} \n");
            body.Append($"Purchase date: {order.PurchaseDate} \n");
            body.Append($"Total: {order.Total} \n");

            string[] emails = new string[]
            {
                order.Customer.Email
            };

            await _emailSender.SendMailAsync(subject, body, emails);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<OrderForGetDTO> GetByIdAsync(Guid orderId)
        {
            var order = await _unitOfWork.OrderRepository.GetByIdAsync(orderId);
            var orderForGetDTO = _mapper.Map<OrderForGetDTO>(order!);

            return orderForGetDTO;
        }

        public async Task<PaginatedList<OrderForGetDTO>> GetByCustomerIdAsync(Guid customerId, PaginationDTO paginationDTO)
        {
            var paginatedListOrder = await _unitOfWork.OrderRepository.GetByCustomerIdAsync(
                customerId,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            var paginatedListOrderForGetDTO = _mapper.Map<PaginatedList<OrderForGetDTO>>(paginatedListOrder);

            return paginatedListOrderForGetDTO;
        }

        public async Task<PaginatedList<OrderDetailForGetDTO>> GetOrderDetailsByOrderIdAsync(Guid orderId, PaginationDTO paginationDTO)
        {
            var paginatedListOrderDetail = await _unitOfWork.OrderDetailRepository.GetByOrderIdAsync(
                orderId,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            var paginatedListOrderDetailForGetDTO = _mapper.Map<PaginatedList<OrderDetailForGetDTO>>(paginatedListOrderDetail);

            return paginatedListOrderDetailForGetDTO;
        }

        public async Task<PaginatedList<BookForGetDTO>> GetPurchasedBooksByCustomerIdAsync(Guid customerId, PaginationDTO paginationDTO)
        {
            var paginatedListBook = await _unitOfWork.OrderRepository.GetPurchasedBooksByCustomerIdAsync(
                customerId,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            var paginatedListBookForGetDTO = _mapper.Map<PaginatedList<BookForGetDTO>>(paginatedListBook);

            return paginatedListBookForGetDTO;
        }
    }
}