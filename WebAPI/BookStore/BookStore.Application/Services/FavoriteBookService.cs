﻿using BookStore.Application.DTOs.FavoriteBookDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.Interfaces;
using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using MapsterMapper;

namespace BookStore.Application.Services
{
    public class FavoriteBookService : IFavoriteBookService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public FavoriteBookService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateFavoriteBook(FavoriteBookForInsertDTO favoriteBookForUpsertDTO)
        {
            var favoriteBook = new FavoriteBook(favoriteBookForUpsertDTO.CustomerId, favoriteBookForUpsertDTO.BookId);

            await _unitOfWork.FavoriteBookRepository.AddAsync(favoriteBook);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteFavoriteBook(Guid favoriteBookId)
        {
            var favoriteBook = await _unitOfWork.FavoriteBookRepository.GetByIdAsync(favoriteBookId);
            favoriteBook!.IsDelete = true;

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<PaginatedList<FavoriteBookForGetDTO>> GetByCustomerIdAsync(Guid customerId, PaginationDTO paginationDTO)
        {
            var paginatedListFavoriteBook = await _unitOfWork.FavoriteBookRepository.GetByCustomerIdAsync(
                customerId,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            var paginatedListFavoriteBookForGetDTO = _mapper.Map<PaginatedList<FavoriteBookForGetDTO>>(paginatedListFavoriteBook);

            return paginatedListFavoriteBookForGetDTO;
        }
    }
}