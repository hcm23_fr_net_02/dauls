﻿using BookStore.Application.Commons.Constants;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.DTOs.ReviewDTOs;
using BookStore.Application.Interfaces;
using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using MapsterMapper;

namespace BookStore.Application.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ReviewService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateReview(ReviewForInsertDTO reviewForInsertDTO)
        {
            var resultPurchasedBookExist = await _unitOfWork.OrderRepository.CheckPurchasedBookExistByCustomerIdAsync(
                reviewForInsertDTO.CustomerId,
                reviewForInsertDTO.BookId);

            if (!resultPurchasedBookExist)
            {
                throw new Exception(ErrorMessages.BookHasNotBeenPurchased);
            }

            var review = new Review(
                reviewForInsertDTO.CustomerId,
                reviewForInsertDTO.BookId,
                reviewForInsertDTO.Comment,
                reviewForInsertDTO.Rate,
                reviewForInsertDTO.ImgUrl);

            await _unitOfWork.ReviewRepository.AddAsync(review);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<PaginatedList<ReviewForGetDTO>> GetByCustomerIdAsync(Guid customerId, PaginationDTO paginationDTO)
        {
            var paginatedListReview = await _unitOfWork.ReviewRepository.GetByCustomerIdAsync(
                paginationDTO.PageNumber,
                paginationDTO.PageSize,
                customerId);

            var paginatedListReviewForGetDTO = _mapper.Map<PaginatedList<ReviewForGetDTO>>(paginatedListReview);

            return paginatedListReviewForGetDTO;
        }
    }
}