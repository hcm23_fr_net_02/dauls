﻿using BookStore.Application.DTOs.ReviewDTOs;
using BookStore.Domain.Entities;
using Mapster;

namespace BookStore.Application.Commons.Mapping
{
    public class ReviewMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Review, ReviewForGetDTO>();
        }
    }
}