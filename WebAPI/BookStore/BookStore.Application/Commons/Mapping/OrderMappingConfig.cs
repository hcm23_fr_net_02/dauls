﻿using BookStore.Application.DTOs.OrderDTOs;
using BookStore.Domain.Entities;
using Mapster;

namespace BookStore.Application.Commons.Mapping
{
    public class OrderMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Order, OrderForGetDTO>();
        }
    }
}