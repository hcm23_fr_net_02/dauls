﻿using BookStore.Application.DTOs.CustomerDTOs;
using BookStore.Domain.Entities;
using Mapster;

namespace BookStore.Application.Commons.Mapping
{
    public class CustomerMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Customer, CustomerForGetDTO>();
        }
    }
}