﻿using BookStore.Application.DTOs.FavoriteBookDTOs;
using BookStore.Domain.Entities;
using Mapster;

namespace BookStore.Application.Commons.Mapping
{
    public class FavoriteBookMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<FavoriteBook, FavoriteBookForGetDTO>();
        }
    }
}