﻿using BookStore.Application.DTOs.BookDTOs;
using BookStore.Domain.Entities;
using Mapster;

namespace BookStore.Application.Commons.Mapping
{
    public class BookMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Book, BookForGetDTO>();
        }
    }
}