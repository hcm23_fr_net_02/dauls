﻿using BookStore.Application.DTOs.OrderDetailDTOs;
using BookStore.Domain.Entities;
using Mapster;

namespace BookStore.Application.Commons.Mapping
{
    public class OrderDetailMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<OrderDetail, OrderDetailForGetDTO>();
        }
    }
}