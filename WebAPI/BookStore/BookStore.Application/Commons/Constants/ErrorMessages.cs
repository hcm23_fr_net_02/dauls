﻿namespace BookStore.Application.Commons.Constants
{
    public class ErrorMessages
    {
        public const string NumberMustBePositive = "Number cannot be less than 0";
        public const string StringCannotBeEmpty = "String cannot be empty";
        public const string DateCannotBeLaterThanCurrentDate = "Date cannot be later than the current date";
        public const string PurchaseDateCannotBeEarlierThanOrderDate = "Purchase date cannot be earlier than order date";
        public const string InsufficientBalance = "Insufficient balance";
        public const string InsufficientBookQuantity = "Insufficient book quantity";
        public const string BookHasNotBeenPurchased = "This book has not been purchased";
        public const string OrderAlreadyPurchased = "This order has been already purchased";
    }
}