﻿using BookStore.Application.Commons.Constants;
using System.ComponentModel.DataAnnotations;

namespace BookStore.Application.Commons.DataAnnotations
{
    public class DateLessThanCurrentDate : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value is DateTime dateTimeValue)
            {
                if (dateTimeValue.Date > DateTime.UtcNow.Date)
                {
                    return new ValidationResult(ErrorMessages.DateCannotBeLaterThanCurrentDate);
                }
            }

            return ValidationResult.Success;
        }
    }
}