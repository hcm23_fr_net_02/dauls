using BookStore.Application.DTOs.BookDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.Services;
using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using MapsterMapper;
using Moq;

namespace BookStore.Test
{
    public class BookServiceTest
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly BookService _bookService;

        public BookServiceTest()
        {
            _mapperMock = new Mock<IMapper>();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _bookService = new BookService(_mapperMock.Object, _unitOfWorkMock.Object);
        }

        [Fact]
        public async Task CreateBookAsync_ShouldCallSaveChangesAsync()
        {
            // Arrange
            var bookForUpsertDTO = new BookForUpsertDTO
            {
                Title = "Update Book",
                Author = "Update Author",
                Genre = "Update Genre",
                PublicationDate = DateTime.Now,
                CoverImgUrl = "Update.jpg",
                Quantity = 10,
                UnitPrice = 19.99m
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.AddAsync(It.IsAny<Book>())).Returns(Task.CompletedTask);

            // Act
            await _bookService.CreateBookAsync(bookForUpsertDTO);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.AddAsync(It.IsAny<Book>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task UpdateBookAsync_ShouldCallSaveChangesAsync()
        {
            // Arrange
            var existingBook = new Book("Test Book", "Test Author", "Test Genre", DateTime.Now, "test.jpb", 10, 10.99m);
            _unitOfWorkMock.Setup(u => u.BookRepository.GetByIdAsync(existingBook.Id)).ReturnsAsync(existingBook);

            var bookForUpsertDTO = new BookForUpsertDTO
            {
                Title = "Update Book",
                Author = "Update Author",
                Genre = "Update Genre",
                PublicationDate = DateTime.Now,
                CoverImgUrl = "Update.jpg",
                Quantity = 10,
                UnitPrice = 19.99m
            };

            // Act
            await _bookService.UpdateBookAsync(existingBook.Id, bookForUpsertDTO);

            // Assert
            Assert.Equal(bookForUpsertDTO.Title, existingBook.Title);
            Assert.Equal(bookForUpsertDTO.Author, existingBook.Author);
            Assert.Equal(bookForUpsertDTO.Genre, existingBook.Genre);
            Assert.Equal(bookForUpsertDTO.PublicationDate, existingBook.PublicationDate);
            Assert.Equal(bookForUpsertDTO.CoverImgUrl, existingBook.CoverImgUrl);
            Assert.Equal(bookForUpsertDTO.Quantity, existingBook.Quantity);
            Assert.Equal(bookForUpsertDTO.UnitPrice, existingBook.UnitPrice);

            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetByIdAsync(existingBook.Id), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task DeleteBookAsync_ShouldCallSaveChangesAsync()
        {
            // Arrange
            var existingBook = new Book("Test Book", "Test Author", "Test Genre", DateTime.Now, "test.jpb", 10, 10.99m);
            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetByIdAsync(existingBook.Id)).ReturnsAsync(existingBook);

            // Act
            await _bookService.DeleteBookAsync(existingBook.Id);

            // Assert
            Assert.True(existingBook.IsDelete);

            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetByIdAsync(existingBook.Id), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task GetAllAsync_ShouldCallGetAllAsync()
        {
            // Arrange
            var paginatedListDTO = new PaginationDTO
            {
                PageNumber = 1,
                PageSize = 10
            };

            var books = new List<Book>
            {
                new Book("Book 1", "Author 1", "Genre 1", DateTime.Now, "cover1.jpg", 5, 9.99m),
                new Book("Book 2", "Author 2", "Genre 2", DateTime.Now, "cover2.jpg", 10, 19.99m)
            };

            var booksForGetDTO = new List<BookForGetDTO>
            {
                new BookForGetDTO(),
                new BookForGetDTO()
            };

            var paginatedListBook = new PaginatedList<Book>(
                books,
                books.Count,
                paginatedListDTO.PageNumber,
                paginatedListDTO.PageSize);

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetAllAsync(paginatedListDTO.PageNumber, paginatedListDTO.PageSize)).ReturnsAsync(paginatedListBook);

            var mapperResult = new PaginatedList<BookForGetDTO>(
                booksForGetDTO,
                books.Count,
                paginatedListDTO.PageNumber,
                paginatedListDTO.PageSize);

            _mapperMock.Setup(mapper => mapper.Map<PaginatedList<BookForGetDTO>>(paginatedListBook)).Returns(mapperResult);

            // Act
            var result = await _bookService.GetAllAsync(paginatedListDTO);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetAllAsync(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task FilterBooksAsync_ShouldCallFilterBooksAsync()
        {
            // Arrange
            var filterDTO = new PaginationFilterBookDTO
            {
                PageNumber = 1,
                PageSize = 10,
                Title = "Test Book",
                Author = "Test Author",
                Genre = "Test Genre",
                PublicationDate = DateTime.Now
            };

            var books = new List<Book>
            {
                new Book("Book 1", "Author 1", "Genre 1", DateTime.Now, "cover1.jpg", 5, 9.99m),
                new Book("Book 2", "Author 2", "Genre 2", DateTime.Now, "cover2.jpg", 10, 19.99m)
            };


            var booksForGetDTO = new List<BookForGetDTO>
            {
                new BookForGetDTO(),
                new BookForGetDTO()
            };

            var paginatedListBook = new PaginatedList<Book>(books, books.Count, filterDTO.PageNumber, filterDTO.PageSize);

            _unitOfWorkMock.Setup(uow => uow.BookRepository.FilterBooksAsync(
                filterDTO.PageNumber,
                filterDTO.PageSize,
                filterDTO.Title,
                filterDTO.Author,
                filterDTO.Genre,
                filterDTO.PublicationDate)).ReturnsAsync(paginatedListBook);

            var mapperResult = new PaginatedList<BookForGetDTO>(
                booksForGetDTO,
                books.Count,
                filterDTO.PageNumber,
                filterDTO.PageSize);

            _mapperMock.Setup(mapper => mapper.Map<PaginatedList<BookForGetDTO>>(paginatedListBook)).Returns(mapperResult);

            // Act
            var result = await _bookService.FilterBooksAsync(filterDTO);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.FilterBooksAsync(
                filterDTO.PageNumber,
                filterDTO.PageSize,
                filterDTO.Title,
                filterDTO.Author,
                filterDTO.Genre,
                filterDTO.PublicationDate), Times.Once);
        }
    }
}