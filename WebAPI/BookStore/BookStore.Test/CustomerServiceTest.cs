﻿using BookStore.Application.DTOs.CustomerDTOs;
using BookStore.Application.Services;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using MapsterMapper;
using Moq;

namespace BookStore.Test
{
    public class CustomerServiceTest
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly CustomerService _customerService;

        public CustomerServiceTest()
        {
            _mapperMock = new Mock<IMapper>();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _customerService = new CustomerService(_unitOfWorkMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task GetById_ShouldCallGetByIdAsync()
        {
            // Arrange
            var customer = new Customer(Guid.NewGuid(), It.IsAny<Guid>(), "Customer", DateTime.Now, "Customer@gmail.com");
            _unitOfWorkMock.Setup(uow => uow.CustomerRepository.GetByIdAsync(customer.Id)).ReturnsAsync(customer);

            // Act
            var result = await _customerService.GetByIdAsync(customer.Id);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.CustomerRepository.GetByIdAsync(customer.Id), Times.Once);
        }
    }
}
