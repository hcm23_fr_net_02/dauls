﻿using BookStore.Application.Commons.Constants;
using BookStore.Application.DTOs.FavoriteBookDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.DTOs.ReviewDTOs;
using BookStore.Application.Interfaces;
using BookStore.Application.Services;
using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using MapsterMapper;
using Moq;

namespace BookStore.Test
{
    public class ReviewServiceTest
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly ReviewService _reviewService;

        public ReviewServiceTest()
        {
            _mapperMock = new Mock<IMapper>();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _reviewService = new ReviewService(_mapperMock.Object, _unitOfWorkMock.Object);
        }

        [Fact]
        public async Task CreateReview_ShouldCallSaveChangesAsync()
        {
            // Arrange
            var reviewForInsertDTO = new ReviewForInsertDTO()
            {
                CustomerId = Guid.NewGuid(),
                BookId = Guid.NewGuid(),
                Comment = "Comment",
                Rate = Domain.Commons.Enums.RateEnum.Good,
                ImgUrl = "ImgUrl"
            };

            _unitOfWorkMock.Setup(uow => uow.ReviewRepository.AddAsync(It.IsAny<Review>())).Returns(Task.CompletedTask);
            _unitOfWorkMock.Setup(uow => uow.OrderRepository.CheckPurchasedBookExistByCustomerIdAsync(
                reviewForInsertDTO.CustomerId,
                reviewForInsertDTO.BookId)).ReturnsAsync(true);

            // Act
            await _reviewService.CreateReview(reviewForInsertDTO);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.ReviewRepository.AddAsync(It.IsAny<Review>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task CreateReview_WithPurhcasedBookNotExist_ShouldThrowExeption()
        {
            // Arrange
            var reviewForInsertDTO = new ReviewForInsertDTO()
            {
                CustomerId = Guid.NewGuid(),
                BookId = Guid.NewGuid(),
                Comment = "Comment",
                Rate = Domain.Commons.Enums.RateEnum.Good,
                ImgUrl = "ImgUrl"
            };

            _unitOfWorkMock.Setup(uow => uow.ReviewRepository.AddAsync(It.IsAny<Review>())).Returns(Task.CompletedTask);
            _unitOfWorkMock.Setup(uow => uow.OrderRepository.CheckPurchasedBookExistByCustomerIdAsync(
                reviewForInsertDTO.CustomerId,
                reviewForInsertDTO.BookId)).ReturnsAsync(false);

            // Act
            // Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _reviewService.CreateReview(reviewForInsertDTO));
            Assert.Equal(ErrorMessages.BookHasNotBeenPurchased, exception.Message);
        }

        [Fact]
        public async Task GetByCustomerIdAsync_ShouldCallGetByCustomerIdAsync()
        {
            // Arrange
            var paginationDTO = new PaginationDTO
            {
                PageNumber = 1,
                PageSize = 10
            };

            var reviews = new List<Review>()
            {
                new Review(It.IsAny<Guid>(), It.IsAny<Guid>(), "Comment", Domain.Commons.Enums.RateEnum.Normal, "ImgURL"),
                new Review(It.IsAny<Guid>(), It.IsAny<Guid>(), "Comment", Domain.Commons.Enums.RateEnum.Normal, "ImgURL"),
            };

            var reviewsForGetDTO = new List<ReviewForGetDTO>()
            {
                new ReviewForGetDTO(),
                new ReviewForGetDTO()
            };

            var paginatedListReview = new PaginatedList<Review>(
                reviews,
                reviews.Count,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            _unitOfWorkMock.Setup(
                uow => uow.ReviewRepository.GetByCustomerIdAsync(
                    paginationDTO.PageNumber,
                    paginationDTO.PageSize,
                    It.IsAny<Guid>()));

            var mapperResult = new PaginatedList<ReviewForGetDTO>(
                reviewsForGetDTO,
                reviewsForGetDTO.Count,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            _mapperMock.Setup(m => m.Map<PaginatedList<ReviewForGetDTO>>(paginatedListReview)).Returns(mapperResult);

            // Act
            await _reviewService.GetByCustomerIdAsync(It.IsAny<Guid>(), paginationDTO);

            //Assert
            _unitOfWorkMock.Verify(uow => uow.ReviewRepository.GetByCustomerIdAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<Guid>()));
        }
    }
}
