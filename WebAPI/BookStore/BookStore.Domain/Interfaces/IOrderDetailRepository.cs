﻿using BookStore.Domain.Commons;
using BookStore.Domain.Entities;

namespace BookStore.Domain.Interfaces
{
    public interface IOrderDetailRepository : IRepository<OrderDetail>
    {
        Task<PaginatedList<OrderDetail>> GetByOrderIdAsync(Guid orderId, int pageNumber, int pageSize);
    }
}
