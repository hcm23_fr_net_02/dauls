﻿using BookStore.Domain.Commons;
using BookStore.Domain.Entities;

namespace BookStore.Domain.Interfaces
{
    public interface IReviewRepository : IRepository<Review>
    {
        Task<PaginatedList<Review>> GetByCustomerIdAsync(int pageNumber, int pageSize, Guid customerId);
    }
}