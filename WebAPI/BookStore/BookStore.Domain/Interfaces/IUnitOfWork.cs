﻿namespace BookStore.Domain.Interfaces
{
    public interface IUnitOfWork
    {
        public IAccountBalanceRepository AccountBalanceRepository { get; }
        public IBookRepository BookRepository { get; }
        public ICustomerRepository CustomerRepository { get; }
        public IFavoriteBookRepository FavoriteBookRepository { get; }
        public IOrderDetailRepository OrderDetailRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IReviewRepository ReviewRepository { get; }
        public IOtpCodeRepository OtpCodeRepository { get; }

        Task SaveChangesAsync();
    }
}