﻿using BookStore.Domain.Entities;

namespace BookStore.Domain.Interfaces
{
    public interface IOtpCodeRepository : IRepository<OtpCode>
    {
        Task<bool> CheckCodeExistsAsync(string code);
        Task<OtpCode?> GetByCodeAndEmail(string code, string email);
        void Remove(OtpCode otpCode);
    }
}