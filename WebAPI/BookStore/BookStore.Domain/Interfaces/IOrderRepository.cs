﻿using BookStore.Domain.Commons;
using BookStore.Domain.Entities;

namespace BookStore.Domain.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<PaginatedList<Book>> GetPurchasedBooksByCustomerIdAsync(Guid customerId, int pageNumber, int pageSize);
        Task<bool> CheckPurchasedBookExistByCustomerIdAsync(Guid customerId, Guid bookId);
        Task<PaginatedList<Order>> GetByCustomerIdAsync(Guid customerId, int pageNumber, int pageSize);
        Task<int> GetCountByOrderDateAsync(DateTime orderDate);
        Task<int> GetCountByPurchaseDateAsync(DateTime purchaseDate);
        Task<decimal> GetRevenueByPurchaseDateAsync(DateTime purchaseDate);
    }
}