﻿using BookStore.Domain.Commons;
using BookStore.Domain.Entities;

namespace BookStore.Domain.Interfaces
{
    public interface IBookRepository : IRepository<Book>
    {
        Task<PaginatedList<Book>> GetAllAsync(int pageNumber, int pageSize);
        Task<PaginatedList<Book>> FilterBooksAsync(int pageNumber, int pageSize, string? title, string? author, string? genre, DateTime? publicationDate);
    }
}
