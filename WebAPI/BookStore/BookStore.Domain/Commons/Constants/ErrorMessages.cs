﻿namespace BookStore.Domain.Commons.Constants
{
    public static class ErrorMessages
    {
        public const string NumberMustBePositive = "Number cannot be less than 0";
        public const string StringCannotBeEmpty = "String cannot be empty";
        public const string DateCannotBeLaterThanCurrentDate = "Date cannot be later than current date";
        public const string DateMustEqualCurrentDate = "Date must equal current date";
        public const string PurchaseDateCannotBeEarlierThanOrderDate = "Purchase date cannot be earlier than order date";
        public const string CannotCreateAppUser = "Cannot create user";
        public const string CannotCreateRole = "Cannot create role";
        public const string CannotAddRole = "Cannot add role";
        public const string CannotFindAppUser = "Cannot find user";
        public const string InvalidEnumRate = "Rate must be between 0 - 2";
        public const string InvalidOtpCodeLength = "Otp code cannot be more than 6 characters";
        public const string InvalidExpirationDate = "Expiration dates cannot be earlier than today";
        public const string InsufficientBalance = "Insufficient balance";
    }
}