﻿using BookStore.Domain.Commons.Constants;
using System.Text;

namespace BookStore.Domain.Entities
{
    public class OtpCode
    {
        private Guid _id = Guid.NewGuid();
        private string _email = string.Empty;
        private string _code = string.Empty;
        private DateTime _expirationDate;

        private OtpCode() { }

        public OtpCode(string email, DateTime expirationDate)
        {
            Email = email;
            ExpirationDate = expirationDate;
        }

        public Guid Id { get => _id; }

        public string Email
        {
            get
            {
                return _email;
            }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ErrorMessages.StringCannotBeEmpty);
                }

                _email = value;
            }
        }

        public string Code
        {
            get
            {
                return _code;
            }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ErrorMessages.StringCannotBeEmpty);
                }

                if (value.Length > 6)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.InvalidOtpCodeLength);
                }

                _code = value;
            }
        }

        public DateTime ExpirationDate
        {
            get
            {
                return _expirationDate;
            }
            private set
            {
                if (value < DateTime.UtcNow)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.InvalidExpirationDate);
                }

                _expirationDate = value;
            }
        }

        public void RandomCode()
        {
            const string Characters = "abcdefghijklmnopqrstuvwxyz0123456789";
            const int OtpCodeLenght = 6;

            Random rd = new Random();
            StringBuilder otpCode = new StringBuilder();
            List<int> numbers = new List<int>();

            //Generate unique number
            for (int i = 0; i < OtpCodeLenght; i++)
            {
                int number;
                do
                {
                    number = rd.Next(0, Characters.Length - 1);
                } while (numbers.Contains(number));

                numbers.Add(number);
            }

            foreach (var item in numbers)
            {
                otpCode.Append(Characters.ElementAt(item));
            }

            Code = otpCode.ToString();
        }
    }
}