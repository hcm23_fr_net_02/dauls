﻿using BookStore.Domain.Commons.Constants;

namespace BookStore.Domain.Entities
{
    public class OrderDetail
    {
        private Guid _id = Guid.NewGuid();
        private Guid _orderId;
        private Guid _bookId;
        private decimal _unitPrice;
        private int _quantity;

        private OrderDetail() { }

        public OrderDetail(Guid orderId, Guid bookId, decimal unitPrice, int quantity)
        {
            OrderId = orderId;
            BookId = bookId;
            UnitPrice = unitPrice;
            Quantity = quantity;
        }

        public Guid Id => _id;

        public Guid OrderId { get => _orderId; private set => _orderId = value; }

        public Guid BookId { get => _bookId; private set => _bookId = value; }

        public decimal UnitPrice
        {
            get { return _unitPrice; }
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.NumberMustBePositive);
                }

                _unitPrice = value;
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.NumberMustBePositive);
                }

                _quantity = value;
            }
        }

        public bool IsDelete { get; set; }

        public Order? Order { get; set; }

        public Book? Book { get; set; }
    }
}