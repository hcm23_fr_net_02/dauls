﻿using BookStore.Domain.Commons.Constants;

namespace BookStore.Domain.Entities
{
    public class Customer
    {
        private Guid _id;
        private Guid _accountBalanceId;
        private string _name = string.Empty;
        private string _email = string.Empty;
        private DateTime _createdDate;

        private Customer() { }

        public Customer(Guid id, Guid accountBalanceId, string name, DateTime createdDate, string email)
        {
            _id = id;
            AccountBalanceId = accountBalanceId;
            Name = name;
            CreatedDate = createdDate;
            Email = email;
        }

        public Guid Id => _id;

        public Guid AccountBalanceId { get => _accountBalanceId; private set => _accountBalanceId = value; }

        public string Name
        {
            get { return _name; }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ErrorMessages.StringCannotBeEmpty);
                }

                _name = value;
            }
        }

        public string Email
        {
            get { return _email; }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ErrorMessages.StringCannotBeEmpty);
                }

                _email = value;
            }
        }

        public DateTime CreatedDate
        {
            get => _createdDate;
            private set
            {
                if (value.Date != DateTime.UtcNow.Date)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.DateMustEqualCurrentDate);
                }

                _createdDate = value;
            }
        }

        public bool IsDelete { get; set; }

        public AccountBalance? AccountBalance { get; set; }

        public ICollection<FavoriteBook> FavoriteBooks { get; set; } = new List<FavoriteBook>();

        public ICollection<Review> Reviews { get; set; } = new List<Review>();

        public ICollection<Order> Orders { get; set; } = new List<Order>();
    }
}