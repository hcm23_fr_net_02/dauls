﻿using BookStore.Domain.Commons.Constants;

namespace BookStore.Domain.Entities
{
    public class Book
    {
        private Guid _id = Guid.NewGuid();
        private string _title = string.Empty;
        private string _author = string.Empty;
        private string _genre = string.Empty;
        private DateTime _publicationDate;
        private string _coverImgUrl = string.Empty;
        private int _quantity;
        private decimal _unitPrice;

        private Book() { }

        public Book(
            string title,
            string author,
            string genre,
            DateTime publicationDate,
            string coverImgUrl,
            int quantity,
            decimal unitPrice)
        {
            Title = title;
            Author = author;
            Genre = genre;
            PublicationDate = publicationDate;
            CoverImgUrl = coverImgUrl;
            Quantity = quantity;
            UnitPrice = unitPrice;
        }

        public Guid Id => _id;

        public string Title
        {
            get { return _title; }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ErrorMessages.StringCannotBeEmpty);
                }

                _title = value;
            }
        }

        public string Author
        {
            get { return _author; }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ErrorMessages.StringCannotBeEmpty);
                }

                _author = value;
            }
        }

        public string Genre
        {
            get { return _genre; }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ErrorMessages.StringCannotBeEmpty);
                }

                _genre = value;
            }
        }

        public DateTime PublicationDate
        {
            get { return _publicationDate; }
            private set
            {
                if (value.Date > DateTime.UtcNow.Date)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.DateCannotBeLaterThanCurrentDate);
                }

                _publicationDate = value;
            }
        }

        public string CoverImgUrl
        {
            get { return _coverImgUrl; }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ErrorMessages.StringCannotBeEmpty);
                }

                _coverImgUrl = value;
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.NumberMustBePositive);
                }

                _quantity = value;
            }
        }

        public decimal UnitPrice
        {
            get { return _unitPrice; }
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.NumberMustBePositive);
                }

                _unitPrice = value;
            }
        }

        public bool IsDelete { get; set; }

        public ICollection<Review> Reviews { get; set; } = new List<Review>();

        public ICollection<FavoriteBook> FavoriteBooks { get; set; } = new List<FavoriteBook>();

        public ICollection<OrderDetail> OrderDetails { get; set; } = new List<OrderDetail>();

        public void UpdateDetails(
            string title,
            string author,
            string genre,
            DateTime publicationDate,
            string coverImgUrl,
            int quantity,
            decimal unitPrice)
        {
            Title = title;
            Author = author;
            Genre = genre;
            PublicationDate = publicationDate;
            CoverImgUrl = coverImgUrl;
            Quantity = quantity;
            UnitPrice = unitPrice;
        }

        public void UpdateQuantityByOrder(int amount)
        {
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException(ErrorMessages.NumberMustBePositive);
            }

            _quantity -= amount;
        }
    }
}