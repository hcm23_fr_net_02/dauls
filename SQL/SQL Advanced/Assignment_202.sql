CREATE DATABASE Assignment_202

USE Assignment_202

CREATE TABLE [dbo].[Employee](
        [EmpNo] [int] NOT NULL,
        [EmpName] [nchar](30) COLLATE
        SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [BirthDay] [datetime] NOT NULL,
        [DeptNo] [int] NOT NULL,
        [MgrNo] [int] NOT NULL,
        [StartDate] [datetime] NOT NULL,
        [Salary] [money] NOT NULL,
        [Status] [int] NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL,
        [Level] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE Employee
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  ADD  CONSTRAINT [chk_Level] CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO

ALTER TABLE [dbo].[Employee]  ADD  CONSTRAINT [chk_Status] CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))
GO

ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30)
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] ADD CONSTRAINT chk_Email1 UNIQUE(Email)
GO

ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo
GO

ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status
GO

CREATE TABLE [dbo].[Skill](
        [SkillNo] [int] IDENTITY(1,1) NOT NULL,
        [SkillName] [nchar](30) COLLATE
         SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)
GO

CREATE TABLE [dbo].[Department](
        [DeptNo] [int] IDENTITY(1,1) NOT NULL,
        [DeptName] [nchar](30) COLLATE
        SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)
GO

CREATE TABLE [dbo].[Emp_Skill](
        [SkillNo] [int] NOT NULL,
        [EmpNo] [int] NOT NULL,
        [SkillLevel] [int] NOT NULL,
        [RegDate] [datetime] NOT NULL,
        [Description] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

-- Insert records into the Department table
INSERT INTO Department (DeptName, Note)
VALUES
    ('HR Department', 'Handles human resources and personnel matters'),
    ('Finance Department', 'Manages financial operations'),
    ('IT Department', 'Manages information technology services'),
    ('Marketing Department', 'Handles marketing and advertising'),
    ('Sales Department', 'Manages sales and customer relations');

-- Insert records into the Skill table
INSERT INTO Skill (SkillName, Note)
VALUES
    ('Programming', 'Software development skills'),
    ('Data Analysis', 'Analytical skills for data analysis'),
    ('Communication', 'Strong communication skills'),
    ('Project Management', 'Skills in managing projects'),
    ('Customer Service', 'Customer support and service skills');

-- Insert records into the Employee table
INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email)
VALUES
    (1, 'John Smith', '1990-05-15', 1, 0, '2015-01-10', 60000, 1, NULL, 5, 'john.smith@example.com'),
    (2, 'Jane Doe', '1985-09-25', 2, 0, '2016-03-20', 70000, 1, NULL, 6, 'jane.doe@example.com'),
    (3, 'Mike Johnson', '1992-11-03', 1, 1, '2017-07-05', 55000, 1, NULL, 4, 'mike.johnson@example.com'),
    (4, 'Emily Wilson', '1988-03-12', 3, 0, '2018-02-15', 65000, 1, NULL, 5, 'emily.wilson@example.com'),
    (5, 'David Brown', '1995-07-18', 4, 0, '2019-04-10', 75000, 1, NULL, 6, 'david.brown@example.com');

-- Insert records into the Emp_Skill table
INSERT INTO Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description)
VALUES
    (1, 1, 5, '2015-01-10', 'Expert programmer in C# and Java'),
    (2, 1, 4, '2015-01-10', 'Skilled in data analysis tools'),
    (3, 2, 5, '2016-03-20', 'Excellent communication skills'),
    (4, 3, 4, '2017-07-05', 'Experience in project management'),
    (5, 4, 5, '2018-02-15', 'Top-notch customer service skills');

-- Cau 3
CREATE PROCEDURE UpdateEmployeeLevel
AS
BEGIN
	UPDATE Employee
	SET Level = 2
	WHERE Employee.Level = '1' AND YEAR(GETDATE()) - YEAR(Employee.StartDate) >= 3
	DECLARE @UpdatedCount int;
	SET @UpdatedCount = @@ROWCOUNT;
	PRINT 'Updated ' + CAST(@UpdatedCount AS nvarchar) + ' records.';
END

EXEC UpdateEmployeeLevel

-- Cau 4
CREATE PROCEDURE GetEmployeeNotWorking
@EmpNo int
AS
BEGIN
IF (SELECT 1 FROM Employee Emp WHERE Emp.EmpNo = @EmpNo AND Emp.Status ='2') IS NOT NULL
	BEGIN
	SELECT a.EmpName, a.Email, b.DeptName
	FROM Employee a
	JOIN Department b ON a.DeptNo = b.DeptNo
	WHERE a.EmpNo = @EmpNo
	END
ELSE
	BEGIN
	PRINT 'This employee is still working'
	END
END

EXEC GetEmployeeNotWorking @EmpNo = 2;

-- Cau 5
CREATE TRIGGER PreventInvalidEmployeeInfor
ON Employee
FOR INSERT, UPDATE
AS
BEGIN
	IF EXISTS (SELECT 1 FROM inserted WHERE Level = 1 AND Salary > 10000000)
	BEGIN
		PRINT 'Can not input information with level = 1 and salary > 10.000.000'
		ROLLBACK TRANSACTION;
	END
END

INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Level, Email)
VALUES (6, 'Invalid Employee', '1990-01-15', 1, 0, '2015-01-10', 11000000, 1, 1, 'invalid@example.com');