 CREATE DATABASE Assignment_301
-- Create table Employee, Status = 1: are working
USE Assignment_301

CREATE TABLE [dbo].[Employee](
	[EmpNo] [int] NOT NULL
,	[EmpName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[BirthDay] [datetime] NOT NULL
,	[DeptNo] [int] NOT NULL
, 	[MgrNo] [int]
,	[StartDate] [datetime] NOT NULL
,	[Salary] [money] NOT NULL
,	[Status] [int] NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
,	[Level] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE Employee 
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Level] 
	CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO
ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Status] 
	CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))

GO
ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30) 
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] 
ADD CONSTRAINT chk_Email1 UNIQUE(Email)

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status

GO
CREATE TABLE [dbo].[Skill](
	[SkillNo] [int] IDENTITY(1,1) NOT NULL
,	[SkillName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)

GO
CREATE TABLE [dbo].[Department](
	[DeptNo] [int] IDENTITY(1,1) NOT NULL
,	[DeptName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)

GO
CREATE TABLE [dbo].[Emp_Skill](
	[SkillNo] [int] NOT NULL
,	[EmpNo] [int] NOT NULL
,	[SkillLevel] [int] NOT NULL
,	[RegDate] [datetime] NOT NULL
,	[Description] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  
ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_2] FOREIGN KEY ([EmpNo])
REFERENCES Employee([EmpNo])

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_3] FOREIGN KEY ([SkillNo])
REFERENCES Skill([SkillNo])

GO

USE Assignment_301

-- Insert data into Employee table
INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email)
VALUES
    (1, N'John Doe', '1980-01-15', 1, NULL, '2023-03-20', 60000, 1, NULL, 5, N'john@example.com'),
    (2, N'Jane Smith', '1985-05-25', 2, 1, '2023-08-10', 55000, 1, NULL, 4, N'jane@example.com'),
    (3, N'Bob Johnson', '1990-11-08', 1, 1, '2023-02-15', 70000, 2, NULL, 6, N'bob@example.com'),
    (4, N'Susan Lee', '1982-09-30', 3, 2, '2023-04-05', 62000, 1, NULL, 5, N'susan@example.com'),
    (5, N'Michael Chen', '1987-04-12', 2, 2, '2022-07-22', 58000, 2, NULL, 4, N'michael@example.com'),
    (6, N'Alice Wang', '1993-03-18', 3, NULL, '2023-01-10', 68000, 1, NULL, 5, N'alice@example.com'),
    (7, N'David Kim', '1981-07-04', 1, 1, '2023-11-30', 72000, 1, NULL, 6, N'david@example.com'),
    (8, N'Emily Davis', '1989-12-22', 2, 3, '2023-09-14', 59000, 2, NULL, 4, N'emily@example.com');

-- Insert data into Skill table
INSERT INTO Skill (SkillName, Note)
VALUES
    (N'Programming', N'General programming skills'),
    (N'Database Management', N'SQL and NoSQL databases'),
    (N'Web Development', N'HTML, CSS, JavaScript'),
    (N'Project Management', N'Agile and Scrum'),
    (N'Data Analysis', N'Data visualization and analytics'),
    (N'Networking', N'Network administration'),
    (N'Security', N'Cybersecurity'),
    (N'UI/UX Design', N'User interface and user experience design');

-- Insert data into Department table
INSERT INTO Department (DeptName, Note)
VALUES
    (N'Engineering', N'Software development and engineering department'),
    (N'Sales', N'Sales and marketing department'),
    (N'Finance', N'Financial management department');

-- Insert data into Emp_Skill table
INSERT INTO Emp_Skill (EmpNo, SkillNo, SkillLevel, RegDate, [Description])
VALUES
    (1, 1, 5, '2023-02-15', NULL),
    (2, 2, 4, '2023-09-28', NULL),
    (3, 3, 5, '2023-01-12', NULL),
    (4, 4, 4, '2022-07-01', NULL),
    (5, 5, 3, '2022-11-20', NULL),
    (6, 6, 5, '2023-02-08', NULL),
    (7, 7, 4, '2023-08-04', NULL),
    (8, 8, 3, '2023-12-19', NULL);

-- Cau 2
SELECT a.EmpName, a.Email, b.DeptName
FROM Employee a LEFT JOIN Department b
ON a.DeptNo = b.DeptNo
WHERE DATEDIFF(month, a.StartDate, GETDATE()) >= 6;

-- Cau 3
SELECT DISTINCT E.EmpName, S.SkillName
FROM Employee AS E
JOIN Emp_Skill AS ES ON E.EmpNo = ES.EmpNo
JOIN Skill AS S ON ES.SkillNo = S.SkillNo
WHERE S.SkillName IN ('C++', '.NET');

-- Cau 4
SELECT a.EmpName, b.EmpName as ManagerName, b.Email as ManagerEmail
FROM Employee a LEFT JOIN Employee b
ON a.MgrNo = b.EmpNo

-- Cau 5
SELECT a.DeptName, COUNT(EmpName) as Employee
FROM Department a JOIN Employee b
ON a.DeptNo = b.DeptNo
GROUP BY a.DeptName
HAVING COUNT(EmpName) >= 2

-- Cau 6
SELECT a.EmpName, a.Email, COUNT(*) as NumberOfSkill FROM Employee a
JOIN Emp_Skill b on a.EmpNo = b.EmpNo
GROUP BY a.EmpName, a.Email
ORDER BY COUNT(*) ASC

-- Cau 7
SELECT a.EmpName, a.Email, a.Birthday FROM Employee a
WHERE a.Status = '1' AND a.EmpNo IN
	(SELECT b.EmpNo FROM Emp_Skill b
	GROUP BY b.EmpNo
	HAVING COUNT(*) >= 2)

-- Cau 8
CREATE VIEW EmployeesAreWorkingWithSkills AS
SELECT a.EmpName, b.SkillName, c.DeptName 
FROM Employee a
JOIN Emp_Skill ES ON a.EmpNo = ES.EmpNo
JOIN Skill b ON b.SkillNo = ES.SkillNo
JOIN Department c ON c.DeptNo = a.DeptNo
WHERE a.Status = '1';

SELECT * FROM EmployeesAreWorkingWithSkills

