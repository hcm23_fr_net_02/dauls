﻿CREATE DATABASE DAULS_QLBH

USE DAULS_QLBH

CREATE TABLE KhachHang (
	MaKH int PRIMARY KEY,
	HoTen nvarchar(255),
	DiaChi nvarchar(255),
	SoDT int,
	NgSinh date,
	Doanhso money,
	NgDK date
);

CREATE TABLE NhanVien (
	MaNV int PRIMARY KEY,
	HoTen nvarchar(255),
	SoDT int,
	NgVaoLam date
);

CREATE TABLE SanPham (
	MaSP int PRIMARY KEY,
	TenSP nvarchar(255),
	DVT nvarchar(255),
	NuocSX nvarchar(255),
	Gia int
);

CREATE TABLE HoaDon (
	SoHD int PRIMARY KEY,
	NgHD date,
	MaKH int,
	MaNV int,
	TriGia int,
	FOREIGN KEY (MaKH) REFERENCES KhachHang(MaKH),
    FOREIGN KEY (MaNV) REFERENCES NhanVien(MaNV)
);

CREATE TABLE CTHD (
	SoHD int,
	MaSP int,
	SL int,
	PRIMARY KEY (SoHD, MaSP),
    FOREIGN KEY (SoHD) REFERENCES HoaDon(SoHD),
    FOREIGN KEY (MaSP) REFERENCES SanPham(MaSP)
);

INSERT INTO SanPham (MaSP, TenSP, DVT, NuocSX, Gia)
VALUES (1, 'Nuoc rua chen', 'Chai', 'VietNam', 20000),
		(2, 'Bot giat', 'Bao', 'VietNam', 30000),
		(3, 'Dien thoai', 'Cai', 'VietNam', 10000000);

INSERT INTO NhanVien (MaNV, HoTen, SoDT, NgVaoLam)
VALUES (1, 'Nguyen Van A', 123456789, '2020-01-15'),
		(2, 'Nguyen Van B', 123456789, '2021-02-15'),
		(3, 'Nguyen Van C', 123456789, '2022-03-15');

INSERT INTO KhachHang (MaKH, HoTen, DiaChi, SoDT, NgSinh, Doanhso, NgDK)
VALUES (1, 'Nguyen Van D', 'A Street', 123456789, '1999-01-10', 0, '2000-01-15'),
		(2, 'Nguyen Van E', 'B Street', 123456789, '2000-01-10', 0, '2000-05-17'),
		(3, 'Nguyen Van F', 'C Street', 123456789, '2001-05-10', 0, '2000-07-20');

INSERT INTO HoaDon (SoHD, NgHD, MaKH, MaNV, TriGia)
VALUES (1, '2023-05-23', 1, 1, 50000),
		(2, '2023-03-13', 2, 2, 40000),
		(3, '2023-05-23', 3, 3, 10050000);

INSERT INTO CTHD (SoHD, MaSP, SL)
VALUES (1, 1, 1),
		(1, 2, 1),
		(2, 1, 2),
		(3, 1, 1),
		(3, 2, 1),
		(3, 3, 1);

-- Cau 1
SELECT DISTINCT a.MaSP, a.TenSP FROM SanPham a
JOIN CTHD b ON a.MaSP = b.MaSP
JOIN HoaDon c ON b.SoHD = c.SoHD
WHERE a.NuocSX = 'VietNam' OR c.NgHD = '2023-05-23'

-- Cau 2
SELECT a.MaSP, a.TenSP FROM SanPham a
WHERE NOT EXISTS
	(SELECT b.MaSP FROM CTHD b WHERE a.MaSP = b.MaSP)

-- Cau 3
SELECT DISTINCT a.SoHD, NgHD, MaKH, MaNV FROM HoaDon a
JOIN CTHD b ON a.SoHD = b.SoHD
JOIN SanPham c ON b.MaSP = c.MaSP
WHERE c.NuocSX = 'VietNam'

